/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "octopus.h"

static void
event_cb (OctopusCrawler *crawler,
          OctopusEvent   *event,
          gpointer        user_data)
{
  g_print ("Received event: %s\n",
           octopus_event_get_printable (event));
}

int main (int argc, char **argv)
{
  GError *error = NULL;
  OctopusCrawler *crawler;
  gint i;

  if (argc < 2)
    {
      g_printerr ("At least one argument (filepath) needed!\n");
      return -1;
    }

  g_thread_init (NULL);
  g_type_init ();

  /* Create crawler */
  crawler = octopus_crawler_new ();

  /* Setup the signal handler */
  g_signal_connect (crawler,
                    "event",
                    G_CALLBACK (event_cb),
                    NULL);

  /* Add paths recursively */
  for (i=1; i<argc; i++)
    {
      GFile *dir;

      dir = g_file_new_for_commandline_arg (argv[i]);
      if (!octopus_crawler_add_path (crawler,
                                     dir,
                                     OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                     &error))
        {
          g_warning ("Error adding path synchronously: %s",
                     error ? error->message : "none");
          g_clear_error (&error);
        }
      else
        g_message ("Succesfully added path synchronously");
      g_object_unref (dir);
    }

  /* Start synchronous crawling! */
  if (!octopus_crawler_crawl (crawler, NULL, &error))
    {
      g_warning ("Crawling failed: %s",
                 error ? error->message : "none");
      g_clear_error (&error);
    }

  g_object_unref (crawler);

  g_message ("Finished.");

  return 0;
}

