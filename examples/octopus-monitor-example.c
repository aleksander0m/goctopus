/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "octopus.h"

/* The program main loop */
GMainLoop *loop;

static void
signals_handler (int signum)
{
  if (loop &&
      g_main_loop_is_running (loop))
    {
      g_main_loop_quit (loop);
    }
}

static void
signals_setup (void)
{
  signal (SIGINT, signals_handler);
  signal (SIGHUP, signals_handler);
  signal (SIGTERM, signals_handler);
}

static void
event_cb (OctopusMonitor  *monitor,
          OctopusEvent    *event,
          gpointer         user_data)
{
  g_print ("Received event: %s\n",
           octopus_event_get_printable (event));
}

static void
monitor_synchronous (OctopusMonitor *monitor,
                     const gchar    *path)
{
  GError *error = NULL;
  GFile *dir;

  dir = g_file_new_for_commandline_arg (path);
  if (!octopus_monitor_add_path (monitor,
                                 dir,
                                 OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                 NULL,
                                 &error))
    {
      g_warning ("Error adding path synchronously: %s",
                 error ? error->message : "none");
      g_error_free (error);
    }
  else
    {
      g_message ("Succesfully added path synchronously");
    }
  g_object_unref (dir);
}

int main (int argc, char **argv)
{
  OctopusMonitor *monitor;
  gint i;

  if (argc < 2)
    {
      g_printerr ("At least one argument (filepath) needed!\n");
      return -1;
    }

  g_thread_init (NULL);
  g_type_init ();

  /* Setup program signals */
  signals_setup ();

  /* Create the monitor (real-time event notification) */
  monitor = octopus_monitor_new_real_time (TRUE, 5);

  /* Setup the signal handler */
  g_signal_connect (monitor,
                    "event",
                    G_CALLBACK (event_cb),
                    NULL);

  for (i=1; i<argc; i++)
    {
      monitor_synchronous (monitor, argv[i]);
    }

  /* Go loop, go */
  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);

  g_object_unref (monitor);

  g_message ("Finished.");

  return 0;
}

