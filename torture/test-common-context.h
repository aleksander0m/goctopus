/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef _TEST_COMMON_CONTEXT_H_
#define _TEST_COMMON_CONTEXT_H_

#include <glib.h>
#include <gio/gio.h>
#include <octopus.h>

/*
 * Test directory structure:
 *  - Directory A
 *     -- Directory AA
 *         --- Directory AAA
 *              --- Directory AAAA
 *              --- Directory AAAB
 *         --- Directory AAB
 *     -- Directory AB
 *         --- Directory ABA
 *         --- Directory ABB
 *  - Directory B
 *     -- Directory BA
 *         --- Directory BAA
 *         --- Directory BAB
 *     -- Directory BB
 *         --- Directory BBA
 *         --- Directory BBB
 */
typedef enum {
  TEST_DIRECTORY_A = 0,
  TEST_DIRECTORY_AA,
  TEST_DIRECTORY_AAA,
  TEST_DIRECTORY_AAAA,
  TEST_DIRECTORY_AAAB,
  TEST_DIRECTORY_AAB,
  TEST_DIRECTORY_AB,
  TEST_DIRECTORY_ABA,
  TEST_DIRECTORY_ABB,
  TEST_DIRECTORY_B,
  TEST_DIRECTORY_BA,
  TEST_DIRECTORY_BAA,
  TEST_DIRECTORY_BAB,
  TEST_DIRECTORY_BB,
  TEST_DIRECTORY_BBA,
  TEST_DIRECTORY_BBB,
  TEST_DIRECTORY_LAST
} TestDirectory;

/* Fixture struct */
typedef struct {
  /* Base tmp dir to work in */
  GFile *base;
  /* Array with all existing test directories */
  GFile *test_dir[TEST_DIRECTORY_LAST];
  /* A directory which doesn't exist */
  GFile *unexisting_dir;
  /* A main loop */
  GMainLoop *loop;
} TestCommonContext;

/* Utilities to setup the tests */
void test_common_context_setup    (TestCommonContext *fixture,
                                   gconstpointer      data);
void test_common_context_teardown (TestCommonContext *fixture,
                                   gconstpointer      data);
void test_common_run_loop         (TestCommonContext *fixture,
                                   guint              duration);
#define octopus_test_add_func(path,fun)         \
  g_test_add_func(path,                         \
                  fun)
#define octopus_test_add(path,fun)              \
  g_test_add (path,                             \
              TestCommonContext,                \
              NULL,                             \
              test_common_context_setup,        \
              fun,                              \
              test_common_context_teardown)

/* Utilities to manage a list of real-time events */
void     test_common_event_list_clean   (GList           **list);
gboolean test_common_event_list_find    (GList             *list,
                                         GFile             *event_source_file,
                                         GFile             *event_destination_file,
                                         OctopusEventType   event_type);
void     test_common_event_list_connect (OctopusMonitor    *monitor,
                                         GList            **list);

/* Utilities to manipulate files */
gboolean test_common_file_create (GFile       *file,
                                  const gchar *contents);
gboolean test_common_file_update (GFile       *file,
                                  const gchar *contents);
gboolean test_common_file_delete (GFile       *file);
gboolean test_common_file_rename (GFile       *file_src,
                                  GFile       *file_dest);

/* Utilities to manipulate directories */
gboolean test_common_directory_create (GFile *directory);
gboolean test_common_directory_delete (GFile *directory);
gboolean test_common_directory_rename (GFile *directory_src,
                                       GFile *directory_dest);

/* I'm lazy to get_uri() each time I want to report an error... */
#define test_common_error(file, error, format, ...) \
  do {                                              \
    gchar *uri = g_file_get_uri (file);             \
    g_debug ("%s [file: %s'] [error: %s]",          \
             format,                                \
             ##__VA_ARGS__,                         \
             uri,                                   \
             error ? error->message : "none");      \
    g_free (uri);                                   \
  } while (0)

#endif /* _TEST_COMMON_CONTEXT_H_ */
