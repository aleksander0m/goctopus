/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>
#include <test-common-context.h>

/* If A is not configured,
 *  -AA, AAA, AAB, AB, ABA and ABB are NOT crawled
 */
static void
test_crawler_tree_001 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is ignored:
 *  -AA, AAA, AAB, AB, ABA and ABB are NOT crawled
 */
static void
test_crawler_tree_002 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is crawled (NOT recursively):
 *  -AA and AB are crawled
 *  -AAA, AAB, ABA and ABB are NOT crawled
 */
static void
test_crawler_tree_003 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is recursively crawled:
 *  -AA, AB, AAA, AAB, ABA and ABB are crawled
 */
static void
test_crawler_tree_004 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, TRUE);

  g_object_unref (crawler);
}

/* If A is ignored and AA is ignored
 *  -AAA and AAB are NOT crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_005 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is ignored and AA is crawled (not recursively),
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_006 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}


/* If A is ignored and AA is recursively crawled,
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are crawled
 */
static void
test_crawler_tree_007 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (crawler);
}

/* If A is crawled (not recursively) and AA is ignored
 *  -AAA and AAB are NOT crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_008 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is crawled (not recursively) and AA is crawled (not recursively),
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_009 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is crawled (not recursively) and AA is recursively crawled,
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are crawled
 */
static void
test_crawler_tree_010 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (crawler);
}

/* If A is crawled recursively and AA is ignored
 *  -AAA and AAB are NOT crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_011 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}

/* If A is crawled recursively and AA is crawled (not recursively),
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are NOT crawled
 */
static void
test_crawler_tree_012 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (crawler);
}


/* If A is crawled recursively and AA is recursively crawled,
 *  -AAA and AAB are crawled
 *  -AAAA and AAAB are crawled
 */
static void
test_crawler_tree_013 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_CRAWL_RECURSIVELY,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_crawler_is_file_crawled (crawler, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (crawler);
}

void
test_crawler_tree_tests (void)
{
  octopus_test_add ("/octopus/crawler/tree/001", test_crawler_tree_001);
  octopus_test_add ("/octopus/crawler/tree/002", test_crawler_tree_002);
  octopus_test_add ("/octopus/crawler/tree/003", test_crawler_tree_003);
  octopus_test_add ("/octopus/crawler/tree/004", test_crawler_tree_004);
  octopus_test_add ("/octopus/crawler/tree/005", test_crawler_tree_005);
  octopus_test_add ("/octopus/crawler/tree/006", test_crawler_tree_006);
  octopus_test_add ("/octopus/crawler/tree/007", test_crawler_tree_007);
  octopus_test_add ("/octopus/crawler/tree/008", test_crawler_tree_008);
  octopus_test_add ("/octopus/crawler/tree/009", test_crawler_tree_009);
  octopus_test_add ("/octopus/crawler/tree/010", test_crawler_tree_010);
  octopus_test_add ("/octopus/crawler/tree/011", test_crawler_tree_011);
  octopus_test_add ("/octopus/crawler/tree/012", test_crawler_tree_012);
  octopus_test_add ("/octopus/crawler/tree/013", test_crawler_tree_013);
}
