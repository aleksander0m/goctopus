/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>

#include <test-common-context.h>

/* Create a new enabled monitor */
static void
test_monitor_new_enabled_001 (void)
{
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, TRUE);
  g_object_unref (monitor);
}

/* Create a new enabled monitor, then disable it */
static void
test_monitor_new_enabled_002 (void)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_disable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, FALSE);

  g_object_unref (monitor);
}

/* Create a new enabled monitor, then re-enable it */
static void
test_monitor_new_enabled_003 (void)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_enable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, TRUE);

  g_object_unref (monitor);
}

/* Create a new disabled monitor */
static void
test_monitor_new_disabled_001 (void)
{
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (FALSE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, FALSE);
  g_object_unref (monitor);
}

/* Create a new disabled monitor, then enable it */
static void
test_monitor_new_disabled_002 (void)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (FALSE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_enable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, TRUE);

  g_object_unref (monitor);
}

/* Create a new disabled monitor, then re-disable it */
static void
test_monitor_new_disabled_003 (void)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (FALSE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));


  g_assert_cmpint (octopus_monitor_disable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_is_enabled (monitor), ==, FALSE);

  g_object_unref (monitor);
}

void
test_monitor_generic_tests (void)
{
  octopus_test_add_func ("/octopus/monitor/new/enabled/001", test_monitor_new_enabled_001);
  octopus_test_add_func ("/octopus/monitor/new/enabled/002", test_monitor_new_enabled_002);
  octopus_test_add_func ("/octopus/monitor/new/enabled/003", test_monitor_new_enabled_003);
  octopus_test_add_func ("/octopus/monitor/new/disabled/001", test_monitor_new_disabled_001);
  octopus_test_add_func ("/octopus/monitor/new/disabled/002", test_monitor_new_disabled_002);
  octopus_test_add_func ("/octopus/monitor/new/disabled/003", test_monitor_new_disabled_003);
}
