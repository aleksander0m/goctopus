/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>

#include <test-common-context.h>

#define LOOP_WAIT_TIME 2

/* Create new disabled monitor with on-demand event support */
static void
test_monitor_event_merge_on_demand_001 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new_on_demand (FALSE, TRUE);

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get events */
  it = octopus_monitor_get_events (monitor);
  /* iterator should NOT be null */
  g_assert (it);
  /* But should not have a single event to iterate */
  g_assert (!octopus_event_iterator_next (it));
  octopus_event_iterator_destroy (it);

  /* And enable */
  g_assert_cmpint (octopus_monitor_enable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);

  /* Get events */
  it = octopus_monitor_get_events (monitor);
  /* iterator should NOT be null */
  g_assert (it);
  /* And it should have several events to iterate. At least one. */
  g_assert (octopus_event_iterator_next (it));
  octopus_event_iterator_destroy (it);

  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add ignored path: no events should be reported */
static void
test_monitor_event_merge_on_demand_002 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get events */
  it = octopus_monitor_get_events (monitor);
  /* iterator should NOT be null */
  g_assert (it);
  /* But should not have a single event to iterate */
  g_assert (!octopus_event_iterator_next (it));
  octopus_event_iterator_destroy (it);

  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * We should have one FOUND event for each file/directory inside the root
 * directory. */
static void
test_monitor_event_merge_on_demand_003 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get events */
  it = octopus_monitor_get_events (monitor);
  /* iterator should NOT be null */
  g_assert (it);

  /* Then we should have 2 FOUND events, for A, AA and AB. But we cannot assume
   * the order of the events here... So, first check that the remaining number
   * of events is 3. */
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  /* No more events... */
  g_assert (!octopus_event_iterator_next (it));

  /* Then, check that we have events for the files */
  octopus_event_iterator_rewind (it);
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));

  octopus_event_iterator_destroy (it);

  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * We should have one FOUND event for each subdirectory inside the hierarchy
 * given by the configured path */
static void
test_monitor_event_merge_on_demand_004 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get events */
  it = octopus_monitor_get_events (monitor);
  /* iterator should NOT be null */
  g_assert (it);

  /* Then we should have 8 FOUND events */
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_next (it));
  /* No more events... */
  g_assert (!octopus_event_iterator_next (it));

  /* Then, check that we have events for the files */
  octopus_event_iterator_rewind (it);
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AAA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AAAA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AAAB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AAB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_ABA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_ABB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));

  octopus_event_iterator_destroy (it);

  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to ignore:
 * Create a file in the monitored directory, and check no CREATED event
 * is issued. */
static void
test_monitor_event_merge_on_demand_005 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");

  /* Create a file inside the monitored structure */
  g_assert (test_common_file_create (test_file, "whatever"));
  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Create a file in the monitored directory, and look for a CREATED event */
static void
test_monitor_event_merge_on_demand_006 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");

  /* Create a file inside the monitored structure */
  g_assert (test_common_file_create (test_file, "whatever"));
  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Create a file in the subdirs of the monitored directory, and make sure
 * there is no CREATED event */
static void
test_monitor_event_merge_on_demand_007 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");

  /* Create a file inside the monitored structure */
  g_assert (test_common_file_create (test_file, "whatever"));
  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * Create a file in the hierarchy, and look for a CREATED event */
static void
test_monitor_event_merge_on_demand_008 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");

  /* Create a file inside the monitored structure */
  g_assert (test_common_file_create (test_file, "whatever"));
  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to ignore:
 * Update the file from the hierarchy, and check there is no UPDATED event */
static void
test_monitor_event_merge_on_demand_009 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "newtext"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Update the file in the directory, and check there is an UPDATED event */
static void
test_monitor_event_merge_on_demand_010 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "newtext"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Update the file in a subdirectory, and check there is no UPDATED event */
static void
test_monitor_event_merge_on_demand_011 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "newtext"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * Update the file from the hierarchy, and look for an UPDATED event */
static void
test_monitor_event_merge_on_demand_012 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "newtext"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to ignore:
 * Delete the file from the hierarchy, and check there is no DELETED event */
static void
test_monitor_event_merge_on_demand_013 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Delete the file from the directory, and check there is a DELETED event */
static void
test_monitor_event_merge_on_demand_014 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Delete the file from the subdirectory, and check there is no DELETED event */
static void
test_monitor_event_merge_on_demand_015 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * Delete the file from the hierarchy, and look for a DELETED event */
static void
test_monitor_event_merge_on_demand_016 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to ignore:
 * Move the file to another monitored path, and check there is no MOVED,
 * DELETED or CREATED events */
static void
test_monitor_event_merge_on_demand_017 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_src,
                                          test_file_dest,
                                          OCTOPUS_EVENT_TYPE_MOVED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_src,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_dest,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Move the file to the same monitored path, and check there is a MOVED event, and
 * no DELETED or CREATED events */
static void
test_monitor_event_merge_on_demand_018 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_src,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_dest,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * Move the file to another monitored path, and look for a MOVED event */
static void
test_monitor_event_merge_on_demand_019 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_src,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_dest,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to ignore:
 * Move the file to another path, and check there is no DELETED event */
static void
test_monitor_event_merge_on_demand_020 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory (AAA) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_src,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (not recursively):
 * Move the file to another path, and check there is a DELETED event */
static void
test_monitor_event_merge_on_demand_021 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory (B) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file, then create new enabled monitor with on-demand event support,
 * and add path to monitor (recursively):
 * Move the file to a non-monitored path, and look for a DELETED event */
static void
test_monitor_event_merge_on_demand_022 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a non-monitored directory (B) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file in a non-monitored path, then create new enabled monitor
 * with on-demand event support, and add path to ignore:
 * Move the file to the path, and check there is no CREATED event */
static void
test_monitor_event_merge_on_demand_023 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory (B) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (!octopus_event_iterator_find (it,
                                          test_file_dest,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file in a non-monitored path, then create new enabled monitor
 * with on-demand event support, and add path to monitor (not recursively):
 * Move the file to the path, and check there is a CREATED event */
static void
test_monitor_event_merge_on_demand_024 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory (B) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_dest,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* Create a test file in a non-monitored path, then create new enabled monitor
 * with on-demand event support, and add path to monitor (recursively):
 * Move the file to a monitored path, and look for a CREATED event */
static void
test_monitor_event_merge_on_demand_025 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Get all found events and remove them */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to a monitored directory (B) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_dest,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* ------------------ Now, real merge events ----------------------- */

/* FOUND(A) + FOUND(A) = N/A */
static void
test_monitor_event_merge_on_demand_026 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* FOUND(A) + CREATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_027 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}


/* FOUND(A) + UPDATE(A) = FOUND(A) */
static void
test_monitor_event_merge_on_demand_028 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);
  /* Update the file */
  g_assert (test_common_file_update (test_file, "egun on guztioi!"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a FOUND event for the file, but no UPDATE */
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* FOUND(A) + DELETE(A) = DELETE(A) */
static void
test_monitor_event_merge_on_demand_029 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a DELETE event for the file, but no FOUND */
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_FOUND));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* FOUND(A) + MOVE(A->B) = FOUND(A) + MOVE(A->B) */
static void
test_monitor_event_merge_on_demand_030 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Move the file to another monitored directory (AAA) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* CREATE(A) + FOUND(A) = N/A */
static void
test_monitor_event_merge_on_demand_031 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* CREATE(A) + CREATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_032 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}


/* CREATE(A) + UPDATE(A) = CREATE(A) */
static void
test_monitor_event_merge_on_demand_033 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  /* Update the file */
  g_assert (test_common_file_update (test_file, "egun on guztioi!"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a CREATE event for the file, but no UPDATE */
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* CREATE(A) + DELETE(A) = NOTHING */
static void
test_monitor_event_merge_on_demand_034 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* There shouldn't be ANY event here */
  g_assert (!octopus_event_iterator_next (it));

  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* CREATE(A) + MOVE(A->B) = CREATE(A) + MOVE(A->B) */
static void
test_monitor_event_merge_on_demand_035 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  /* Move the file to another monitored directory (AAA) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* UPDATE(A) + FOUND(A) = N/A */
static void
test_monitor_event_merge_on_demand_036 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* UPDATE(A) + CREATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_037 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}


/* UPDATE(A) + UPDATE(A) = UPDATE(A) */
static void
test_monitor_event_merge_on_demand_038 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "egun on guztioi!"));

  /* Update the file */
  g_assert (test_common_file_update (test_file, "ondoloin!"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a single UPDATE event */
  g_assert (octopus_event_iterator_next (it));
  g_assert (!octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* UPDATE(A) + DELETE(A) = DELETE(A) */
static void
test_monitor_event_merge_on_demand_039 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file, "egun on guztioi!"));

  /* Delete the file */
  g_assert (test_common_file_delete (test_file));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a DELETE event for the file, but no UPDATE */
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_DELETED));
  g_assert (!octopus_event_iterator_find (it,
                                          test_file,
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_object_unref (test_file);
  g_object_unref (monitor);
}

/* UPDATE(A) + MOVE(A->B) = UPDATE(A) + MOVE(A->B) */
static void
test_monitor_event_merge_on_demand_040 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Update the file */
  g_assert (test_common_file_update (test_file_src, "egun on guztioi!"));

  /* Move the file to another monitored directory (AAA) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_UPDATED));
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}

/* DELETE(A) + FOUND(A) = N/A */
static void
test_monitor_event_merge_on_demand_041 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* DELETE(A) + CREATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_042 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file;

  /* Create a test file */
  test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "file.txt");
  g_assert (test_common_file_create (test_file, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Delete and create the file again */
  g_assert (test_common_file_delete (test_file));
  g_assert (test_common_file_create (test_file, "new text"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events */
  it = octopus_monitor_get_events (monitor);

  /* Make sure there's a single UPDATE event */
  g_assert (octopus_event_iterator_next (it));
  g_assert (!octopus_event_iterator_next (it));
  g_assert (octopus_event_iterator_find (it,
                                         test_file,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_UPDATED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file));
  g_object_unref (test_file);
  g_object_unref (monitor);
}


/* DELETE(A) + UPDATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_043 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* DELETE(A) + DELETE(A) = N/A */
static void
test_monitor_event_merge_on_demand_044 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* DELETE(A) + MOVE(A->B) = MOVE(A->B)
 * This is a tricky one, as GLib will only notify a DELETE before the MOVE
 * for directory moves, if the directory had contents, and there was an event
 * on some file inside the directory...
 */
static void
test_monitor_event_merge_on_demand_045 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *dest;
  GFile *stupid_file;

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Create a file inside AA */
  stupid_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AA], "stupid.txt");
  g_assert (test_common_file_create (stupid_file, "prff"));

  /* Remove any possible FOUND or CREATED event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Moving AA->AZ */
  dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "Z");
  g_assert (test_common_file_rename (fixture->test_dir[TEST_DIRECTORY_AA], dest));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events, there should be a single MOVE event, and no DELETE event */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         fixture->test_dir[TEST_DIRECTORY_AA],
                                         dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  g_assert (!octopus_event_iterator_find (it,
                                          fixture->test_dir[TEST_DIRECTORY_AA],
                                          NULL,
                                          OCTOPUS_EVENT_TYPE_DELETED));
  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_rename (dest, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (test_common_file_delete (stupid_file));
  g_object_unref (dest);
  g_object_unref (stupid_file);
  g_object_unref (monitor);
}

/* MOVE(A->B) + FOUND(A) = N/A */
static void
test_monitor_event_merge_on_demand_046 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* MOVE(A->B) + CREATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_047 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  OctopusEventIterator *it;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *test_file_src;
  GFile *test_file_dest;

  /* Create a test file */
  test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A],
                                    "file.txt");
  g_assert (test_common_file_create (test_file_src, "whatever"));

  monitor = octopus_monitor_new_on_demand (TRUE, TRUE);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Remove any possible FOUND event */
  it = octopus_monitor_get_events (monitor);
  octopus_event_iterator_destroy (it);

  /* Move the file to another monitored directory (AAA) */
  test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA],
                                     "renamed.txt");
  g_assert (test_common_file_rename (test_file_src, test_file_dest));

  /* Create src again */
  g_assert (test_common_file_create (test_file_src, "whatever"));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get all events, MOVE and CREATED */
  it = octopus_monitor_get_events (monitor);
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         test_file_dest,
                                         OCTOPUS_EVENT_TYPE_MOVED));
  g_assert (octopus_event_iterator_find (it,
                                         test_file_src,
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_CREATED));

  octopus_event_iterator_destroy (it);

  /* Clean exit */
  g_assert (test_common_file_delete (test_file_dest));
  g_assert (test_common_file_delete (test_file_src));
  g_object_unref (test_file_src);
  g_object_unref (test_file_dest);
  g_object_unref (monitor);
}


/* MOVE(A->B) + UPDATE(A) = N/A */
static void
test_monitor_event_merge_on_demand_048 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* MOVE(A->B) + DELETE(A) = N/A */
static void
test_monitor_event_merge_on_demand_049 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

/* MOVE(A->B) + MOVE(A->B) = N/A */
static void
test_monitor_event_merge_on_demand_050 (TestCommonContext *fixture,
                                        gconstpointer      data)
{
  g_assert (TRUE);
}

void
test_monitor_event_merge_on_demand_tests (void)
{
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/001", test_monitor_event_merge_on_demand_001);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/002", test_monitor_event_merge_on_demand_002);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/003", test_monitor_event_merge_on_demand_003);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/004", test_monitor_event_merge_on_demand_004);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/005", test_monitor_event_merge_on_demand_005);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/006", test_monitor_event_merge_on_demand_006);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/007", test_monitor_event_merge_on_demand_007);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/008", test_monitor_event_merge_on_demand_008);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/009", test_monitor_event_merge_on_demand_009);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/010", test_monitor_event_merge_on_demand_010);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/011", test_monitor_event_merge_on_demand_011);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/012", test_monitor_event_merge_on_demand_012);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/013", test_monitor_event_merge_on_demand_013);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/014", test_monitor_event_merge_on_demand_014);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/015", test_monitor_event_merge_on_demand_015);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/016", test_monitor_event_merge_on_demand_016);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/017", test_monitor_event_merge_on_demand_017);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/018", test_monitor_event_merge_on_demand_018);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/019", test_monitor_event_merge_on_demand_019);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/020", test_monitor_event_merge_on_demand_020);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/021", test_monitor_event_merge_on_demand_021);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/022", test_monitor_event_merge_on_demand_022);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/023", test_monitor_event_merge_on_demand_023);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/024", test_monitor_event_merge_on_demand_024);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/025", test_monitor_event_merge_on_demand_025);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/026", test_monitor_event_merge_on_demand_026);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/027", test_monitor_event_merge_on_demand_027);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/028", test_monitor_event_merge_on_demand_028);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/029", test_monitor_event_merge_on_demand_029);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/030", test_monitor_event_merge_on_demand_030);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/031", test_monitor_event_merge_on_demand_031);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/032", test_monitor_event_merge_on_demand_032);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/033", test_monitor_event_merge_on_demand_033);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/034", test_monitor_event_merge_on_demand_034);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/035", test_monitor_event_merge_on_demand_035);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/036", test_monitor_event_merge_on_demand_036);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/037", test_monitor_event_merge_on_demand_037);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/038", test_monitor_event_merge_on_demand_038);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/039", test_monitor_event_merge_on_demand_039);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/040", test_monitor_event_merge_on_demand_040);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/041", test_monitor_event_merge_on_demand_041);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/042", test_monitor_event_merge_on_demand_042);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/043", test_monitor_event_merge_on_demand_043);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/044", test_monitor_event_merge_on_demand_044);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/045", test_monitor_event_merge_on_demand_045);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/046", test_monitor_event_merge_on_demand_046);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/047", test_monitor_event_merge_on_demand_047);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/048", test_monitor_event_merge_on_demand_048);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/049", test_monitor_event_merge_on_demand_049);
  octopus_test_add ("/octopus/monitor/event/merge/on-demand/050", test_monitor_event_merge_on_demand_050);
}
