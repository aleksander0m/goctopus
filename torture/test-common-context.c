/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <test-common-context.h>

static const gchar *test_directories_subpaths [TEST_DIRECTORY_LAST] = {
  "A",
  "A" G_DIR_SEPARATOR_S "A",
  "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "A",
  "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "A",
  "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "B",
  "A" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "B",
  "A" G_DIR_SEPARATOR_S "B",
  "A" G_DIR_SEPARATOR_S "B" G_DIR_SEPARATOR_S "A",
  "A" G_DIR_SEPARATOR_S "B" G_DIR_SEPARATOR_S "B",
  "B",
  "B" G_DIR_SEPARATOR_S "A",
  "B" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "A",
  "B" G_DIR_SEPARATOR_S "A" G_DIR_SEPARATOR_S "B",
  "B" G_DIR_SEPARATOR_S "B",
  "B" G_DIR_SEPARATOR_S "B" G_DIR_SEPARATOR_S "A",
  "B" G_DIR_SEPARATOR_S "B" G_DIR_SEPARATOR_S "B",
};

void
test_common_context_setup (TestCommonContext *fixture,
                           gconstpointer      data)
{
  guint i;
  gchar *unexisting_dir;
  gchar *base_dir;
  gchar *base_tmp_dir;

  /* Setup unexisting dir */
  unexisting_dir = g_build_path (G_DIR_SEPARATOR_S,
                                 g_get_tmp_dir (),
                                 "ay",
                                 "piticli",
                                 "bonito",
                                 NULL);
  fixture->unexisting_dir = g_file_new_for_path (unexisting_dir);
  g_free (unexisting_dir);

  /* Setup base tmp dir */
  base_dir = g_strdup_printf ("octopus-test-%d", getpid());
  base_tmp_dir = g_build_path (G_DIR_SEPARATOR_S,
                               g_get_tmp_dir (),
                               base_dir,
                               NULL);
  fixture->base = g_file_new_for_path (base_tmp_dir);

  /* Try to create the directory */
  g_assert (g_file_make_directory (fixture->base, NULL, NULL));

  /* Initialize aux directories */
  for (i = 0; i<TEST_DIRECTORY_LAST; i++)
    {
      gchar *path;

      path = g_build_path (G_DIR_SEPARATOR_S,
                           base_tmp_dir,
                           test_directories_subpaths[i],
                           NULL);
      fixture->test_dir[i] = g_file_new_for_path (path);
      g_assert (g_file_make_directory (fixture->test_dir[i], NULL, NULL));
      g_free (path);
    }

  /* Create loop */
  fixture->loop = g_main_loop_new (NULL, FALSE);

  g_free (base_dir);
  g_free (base_tmp_dir);
}

void
test_common_context_teardown (TestCommonContext *fixture,
                              gconstpointer      data)
{
  gint i;

  /* Unref loop */
  if (fixture->loop)
    {
      g_main_loop_unref (fixture->loop);
    }

  /* Deinit aux directories, from last to first */
  for (i = TEST_DIRECTORY_LAST-1; i >= 0; i--)
    {
      if (fixture->test_dir[i])
        {
          g_file_delete (fixture->test_dir[i], NULL, NULL);
          g_object_unref (fixture->test_dir[i]);
        }
    }

  /* Deinit base tmp dir */
  if (fixture->base)
    {
      g_file_delete (fixture->base, NULL, NULL);
      g_object_unref (fixture->base);
    }

  /* Deinit unexisting dir */
  if (fixture->unexisting_dir)
    {
      g_object_unref (fixture->unexisting_dir);
    }
}

static gboolean
stop_loop (gpointer user_data)
{
  TestCommonContext *fixture = user_data;

  g_main_loop_quit (fixture->loop);
  return FALSE;
}

void
test_common_run_loop (TestCommonContext *fixture,
                      guint              duration)
{
  g_timeout_add_seconds (duration, stop_loop, fixture);
  g_main_loop_run (fixture->loop);
}

void
test_common_event_list_clean (GList **list)
{
  GList *it;

  for (it = *list; it; it = g_list_next (it))
    {
      g_object_unref (it->data);
    }
  *list = NULL;
}

gboolean
test_common_event_list_find (GList            *list,
                             GFile            *event_source_file,
                             GFile            *event_destination_file,
                             OctopusEventType  event_type)
{
  gboolean found = FALSE;
  GList *it;

  for (it = list; !found && it; it = g_list_next (it))
    {
      OctopusEvent *event = it->data;
      GFile *source_file;

      source_file = octopus_event_get_source_file (event);
      if (octopus_event_get_event_type (event) == event_type &&
          g_file_equal (source_file, event_source_file))
        {
          if (event_destination_file &&
              event_type == OCTOPUS_EVENT_TYPE_MOVED)
            {
              GFile *destination_file;

              destination_file = octopus_event_moved_get_destination_file (OCTOPUS_EVENT_MOVED (event));
              found = g_file_equal (event_destination_file, destination_file);
              g_object_unref (destination_file);
            }
          else
            found = TRUE;
        }

      g_object_unref (source_file);
    }
  return found;
}

static void
test_common_event_cb (OctopusMonitor *monitor,
                      OctopusEvent   *event,
                      gpointer        user_data)
{
  GList **list = user_data;

  /* Append event */
  *list = g_list_append (*list, g_object_ref (event));
}

void
test_common_event_list_connect (OctopusMonitor  *monitor,
                                GList          **list)
{
  /* Connect to the real-time event notifications */
  g_signal_connect (monitor,
                    "event",
                    G_CALLBACK (test_common_event_cb),
                    list);
}

gboolean
test_common_file_create (GFile       *file,
                         const gchar *contents)
{
  GFileOutputStream *os = NULL;
  GError *error = NULL;
  gboolean ret_code = FALSE;

  if ((os = g_file_create (file, G_FILE_CREATE_NONE, NULL, &error)) == NULL)
    {
      test_common_error (file, error, "Couldn't create file");
    }
  else if (!g_output_stream_write_all (G_OUTPUT_STREAM (os),
                                       contents,
                                       strlen (contents),
                                       NULL,
                                       NULL,
                                       &error))
    {
      test_common_error (file, error, "Couldn't write to output stream");
    }
  else
    {
      ret_code = TRUE;
    }

  if (error)
    g_error_free (error);
  if (os)
    {
      g_output_stream_close (G_OUTPUT_STREAM (os), NULL, NULL);
      g_object_unref (os);
    }

  return ret_code;
}

gboolean
test_common_file_update (GFile       *file,
                         const gchar *contents)
{
  GOutputStream *os = NULL;
  GFileIOStream *ios = NULL;
  GError *error = NULL;
  gboolean ret_code = FALSE;

  if ((ios = g_file_open_readwrite (file, NULL, &error)) == NULL)
    {
      test_common_error (file, error, "Couldn't open file");
    }
  else if ((os = g_io_stream_get_output_stream (G_IO_STREAM (ios))) == NULL)
    {
      test_common_error (file, error, "Couldn't get output stream");
    }
  else if (!g_output_stream_write_all (G_OUTPUT_STREAM (os),
                                       contents,
                                       strlen (contents),
                                       NULL,
                                       NULL,
                                       &error))
    {
      test_common_error (file, error, "Couldn't write to output stream");
    }
  else
    {
      ret_code = TRUE;
    }

  if (error)
    g_error_free (error);
  if (ios)
    {
      g_io_stream_close (G_IO_STREAM (ios), NULL, NULL);
      g_object_unref (ios);
    }

  return ret_code;
}

gboolean
test_common_file_delete (GFile *file)
{
  GError *error = NULL;
  gboolean ret_code = FALSE;

  if ((ret_code = g_file_delete (file, NULL, &error)) == FALSE)
    {
      test_common_error (file, error, "Couldn't delete");
    }

  return ret_code;
}

gboolean
test_common_file_rename (GFile *file_src,
                         GFile *file_dest)
{
  GError *error = NULL;
  gboolean ret_code = FALSE;

  if ((ret_code = g_file_move (file_src,
                               file_dest,
                               G_FILE_COPY_OVERWRITE,
                               NULL,
                               NULL,
                               NULL,
                               &error)) == FALSE)
    {
      test_common_error (file_src, error, "Couldn't rename");
    }

  return ret_code;
}

gboolean
test_common_directory_create (GFile *directory)
{
  GError *error = NULL;
  gboolean ret_code = FALSE;

  if ((ret_code = g_file_make_directory (directory, NULL, &error)) == FALSE)
    {
      test_common_error (directory, error, "Couldn't create directory");
    }

  return ret_code;
}

gboolean
test_common_directory_delete (GFile *directory)
{
  return test_common_file_delete (directory);
}

gboolean
test_common_directory_rename (GFile *directory_src,
                              GFile *directory_dest)
{
  return test_common_file_rename (directory_src, directory_dest);
}
