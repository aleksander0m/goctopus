/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>
#include <test-common-context.h>

/*
 * If no paths configured, iterator is returned but it's empty.
 */
static void
test_crawler_path_iterator_001 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusCrawler *crawler;
  OctopusPathIterator *it;
  const OctopusPath *path;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Get paths iterator */
  it = octopus_crawler_get_paths (crawler);
  g_assert (it);

  /* Try to get first path, should fail */
  path = octopus_path_iterator_next (it);
  g_assert (!path);

  /* Cleanup iterator */
  octopus_path_iterator_destroy (it);

  g_object_unref (crawler);
}


/*
 * Add one path and make sure it's the proper one
 */
static void
test_crawler_path_iterator_002 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;
  OctopusPathIterator *it;
  const OctopusPath *path;
  GFile *path_directory;
  const gchar *path_directory_uri;
  OctopusAction path_action;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get paths iterator */
  it = octopus_crawler_get_paths (crawler);
  g_assert (it);

  /* Try to get first path, should be ok */
  path = octopus_path_iterator_next (it);
  g_assert (path);
  g_assert (OCTOPUS_IS_PATH (path));

  /* Read path properties */
  g_object_get (G_OBJECT (path),
                "path-directory", &path_directory,
                "path-directory-uri", &path_directory_uri,
                "path-action", &path_action,
                NULL);

  /* And compare them with expected */
  g_assert_cmpint (g_file_equal (path_directory, fixture->test_dir[TEST_DIRECTORY_A]), ==, TRUE);
  g_assert (path_directory_uri);
  g_assert_cmpint (path_action, ==, OCTOPUS_ACTION_IGNORE);
  g_object_unref (path_directory);

  /* Try to get next path, should fail */
  path = octopus_path_iterator_next (it);
  g_assert (!path);

  /* Cleanup iterator */
  octopus_path_iterator_destroy (it);

  g_object_unref (crawler);
}

/*
 * Add one path, make sure it's the proper one, then rewind and make sure
 * the path is still the proper one.
 */
static void
test_crawler_path_iterator_003 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;
  OctopusPathIterator *it;
  const OctopusPath *path;
  GFile *path_directory_1;
  GFile *path_directory_2;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get paths iterator */
  it = octopus_crawler_get_paths (crawler);
  g_assert (it);

  /* Try to get first path, should be ok */
  path = octopus_path_iterator_next (it);
  g_assert (path);
  g_assert (OCTOPUS_IS_PATH (path));

  /* Read path properties */
  g_object_get (G_OBJECT (path),
                "path-directory", &path_directory_1,
                NULL);

  /* And compare them with expected */
  g_assert_cmpint (g_file_equal (path_directory_1, fixture->test_dir[TEST_DIRECTORY_A]), ==, TRUE);

  /* Try to get next path, should fail */
  path = octopus_path_iterator_next (it);
  g_assert (!path);

  /* Rewind */
  octopus_path_iterator_rewind (it);

  /* Try to get first path, should be ok */
  path = octopus_path_iterator_next (it);
  g_assert (path);
  g_assert (OCTOPUS_IS_PATH (path));

  /* Read path properties */
  g_object_get (G_OBJECT (path),
                "path-directory", &path_directory_2,
                NULL);

  /* And compare them with expected */
  g_assert_cmpint (g_file_equal (path_directory_1, path_directory_2), ==, TRUE);

  /* Try to get next path, should fail */
  path = octopus_path_iterator_next (it);
  g_assert (!path);

  /* Cleanup iterator */
  octopus_path_iterator_destroy (it);

  g_object_unref (path_directory_1);
  g_object_unref (path_directory_2);
  g_object_unref (crawler);
}

/*
 * Add several paths, make sure all of them are shown by the iterator.
 */
static void
test_crawler_path_iterator_004 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  GError *error = NULL;
  OctopusCrawler *crawler;
  OctopusPathIterator *it;
  const OctopusPath *path;
  GFile *path_directory_1;
  GFile *path_directory_2;

  crawler = octopus_crawler_new ();
  g_assert (crawler);
  g_assert (OCTOPUS_IS_CRAWLER (crawler));

  /* Add path 1 */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Add path 2 */
  g_assert_cmpint (octopus_crawler_add_path (crawler,
                                             fixture->test_dir[TEST_DIRECTORY_B],
                                             OCTOPUS_ACTION_CRAWL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get paths iterator */
  it = octopus_crawler_get_paths (crawler);
  g_assert (it);

  /* Try to get first path, should be ok */
  path = octopus_path_iterator_next (it);
  g_assert (path);
  g_assert (OCTOPUS_IS_PATH (path));

  /* Read path properties */
  g_object_get (G_OBJECT (path),
                "path-directory", &path_directory_1,
                NULL);

  /* Try to get next path, should be ok */
  path = octopus_path_iterator_next (it);
  g_assert (path);
  g_assert (OCTOPUS_IS_PATH (path));

  /* Read path properties */
  g_object_get (G_OBJECT (path),
                "path-directory", &path_directory_2,
                NULL);

  /* Both paths should be different */
  g_assert_cmpint (g_file_equal (path_directory_1, path_directory_2), ==, FALSE);

  /* Paths should be equal to the ones configured. Note that order shouldn't
   * matter */
  if (g_file_equal (path_directory_1, fixture->test_dir[TEST_DIRECTORY_A]))
    g_assert_cmpint (g_file_equal (path_directory_2, fixture->test_dir[TEST_DIRECTORY_B]), ==, TRUE);
  else if (g_file_equal (path_directory_1, fixture->test_dir[TEST_DIRECTORY_B]))
    g_assert_cmpint (g_file_equal (path_directory_2, fixture->test_dir[TEST_DIRECTORY_A]), ==, TRUE);
  else
    g_assert_not_reached ();

  /* Try to get next path, should fail */
  path = octopus_path_iterator_next (it);
  g_assert (!path);

  /* Cleanup iterator */
  octopus_path_iterator_destroy (it);

  g_object_unref (path_directory_1);
  g_object_unref (path_directory_2);
  g_object_unref (crawler);
}

void
test_crawler_path_iterator_tests (void)
{
  octopus_test_add ("/octopus/crawler/path/iterator/001", test_crawler_path_iterator_001);
  octopus_test_add ("/octopus/crawler/path/iterator/002", test_crawler_path_iterator_002);
  octopus_test_add ("/octopus/crawler/path/iterator/003", test_crawler_path_iterator_003);
  octopus_test_add ("/octopus/crawler/path/iterator/004", test_crawler_path_iterator_004);
}
