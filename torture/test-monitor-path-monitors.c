/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>
#include <test-common-context.h>

#define LOOP_WAIT_TIME 2

/* If A is ignored,
 *  -A, AA, AB, AAA, AAB, ABA and ABB do not have monitors
 */
static void
test_monitor_path_monitors_001 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A and all the inner ones shouldn't have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (NOT recursively):
 *  -A has monitor
 *  -AA, AB, AAA, AAB, ABA and ABB do not have monitors
 */
static void
test_monitor_path_monitors_002 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A directory should have a monitor */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));

  /* All the inner ones shouldn't */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively):
 *  -A, AA, AB, AAA, AAB, ABA and ABB have monitors
 */
static void
test_monitor_path_monitors_003 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A and all inner ones should have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is ignored:
 *  -A, AA, AB, AAA, AAB, ABA and ABB don't have monitors
 */
static void
test_monitor_path_monitors_004 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A and all inner ones shouldn't have monitors */
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}


/* If A is ignored and AA is monitored (not recursively):
 *  -AA has monitor
 *  -A, AB, ABA, ABB, AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_005 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* AA has monitor in AA path, not in A path*/
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (recursively):
 *  -AA, AAA, AAB  have monitors
 *  -A, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_006 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* AA, AAA, AAB have monitors in the AA path, but not in the A path */
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is ignored:
 *  -A has monitor.
 *  -AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_007 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A has monitor in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (not recursively):
 *  -A and AA have monitors.
 *  -AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_008 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A has monitor in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));

  /* AA has monitor in AA path, but not in A path */
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (recursively):
 *  -A, AA, AAA, AAB have monitors.
 *  -AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_009 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A has monitor in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));

  /* AA, AAA, AAB have monitors in AA path, but not in A path */
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is ignored:
 *  -A, AB, ABA, ABB have monitors.
 *  -AA, AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_010 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A, AB, ABA, ABB have monitors in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (not recursively):
 *  -A, AA, AB, ABA, ABB have monitors.
 *  -AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_011 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A, AB, ABA, ABB have monitors in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  /* AA has monitor in AA path, but not in A path */
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (recursively):
 *  -A, AA, AB, ABA, ABB, AAA, AAB have monitors.
 */
static void
test_monitor_path_monitors_012 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path_A;
  OctopusPath *path_AA;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Get path object */
  path_A = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path_A);
  path_AA = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path_AA);

  /* A, AB, ABA, ABB have monitors in A path, but not in AA path */
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_ABB]));
  g_assert (!octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_ABB]));

  /* AA, AAA, AAB have monitors in AA path, but not in A path */
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path_AA, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path_A, fixture->test_dir[TEST_DIRECTORY_AAB]));

  g_object_unref (path_A);
  g_object_unref (path_AA);
  g_object_unref (monitor);
}

/* If A is ignored and AA is ignored, then A is removed from monitor
 *  -A, AA, AB, AAA, AAB, ABA and ABB don't have monitors
 */
static void
test_monitor_path_monitors_013 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* A and all inner ones shouldn't have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is ignored, then AA is removed from monitor
 *  -A, AA, AB, AAA, AAB, ABA and ABB don't have monitors
 */
static void
test_monitor_path_monitors_014 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A and all inner ones shouldn't have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (not recursively), then A is removed
 * from monitor.
 *  -AA has monitor
 *  -A, AB, ABA, ABB, AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_015 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA has monitor in AA path, not in A path */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (not recursively), then AA is removed
 * from monitor.
 *  -A, AA, AB, ABA, ABB, AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_016 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (recursively), then A is removed from
 * monitor.
 *  -AA, AAA, AAB have monitors
 *  -A, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_017 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA, AAA, AAB have monitors in the AA path, but not in the A path */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (recursively), then AA is removed
 * from monitor.
 *  -A, AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_018 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is ignored, then A is removed
 * from monitor.
 *  -A, AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_019 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is ignored, then AA is removed
 * from monitor.
 *  -A has monitor.
 *  -AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_020 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A should have monitor */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (not recursively),
 *  then A is removed from monitor.
 *  -AA has monitor.
 *  -A, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_021 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA has monitor in AA path, but not in A path */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (not recursively),
 *  then AA is removed from monitor.
 *  -A has monitor.
 *  -AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_022 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A has monitor in A path */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (recursively),
 *  then A is removed from monitor.
 *  -AA, AAA, AAB have monitors.
 *  -A, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_023 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA, AAA, AAB have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (recursively),
 *  then AA is removed from monitor.
 *  -A has monitor.
 *  -AA, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_024 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* A has monitor */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is ignored, then A is removed
 * from monitor.
 *  -A, AB, ABA, ABB, AA, AAA, AAB don't have monitors
 */
static void
test_monitor_path_monitors_025 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is ignored, then AA is removed
 * from monitor.
 *  -A, AB, ABA, ABB, AA, AAA, AAB all have monitors
 */
static void
test_monitor_path_monitors_026 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* All of them should have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (not recursively), then
 *  A is removed from monitor.
 *  -AA has monitor.
 *  -A, AAA, AAB, AB, ABA, ABB don't have monitors
 */
static void
test_monitor_path_monitors_027 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA has monitor */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));

  /* All the other ones shouldn't have monitors anywhere */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (not recursively), then
 *  AA is removed from monitor.
 *  -A, AA, AAA, AAB, AB, ABA, ABB all have monitors
 */
static void
test_monitor_path_monitors_028 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* All have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (recursively),
 * then A is removed from monitor.
 *  -AA, AAA, AAB have monitors.
 *  -A, AB, ABA, ABB don't have monitors.
 */
static void
test_monitor_path_monitors_029 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_A],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get A path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (!path);

  /* Get AA path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (path);

  /* AA, AAA, AAB have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));

  /* A, AB, ABA, ABB don't have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* If A is monitored (recursively) and AA is monitored (recursively),
 * then AA is removed from monitor.
 *  -A, AA, AAA, AAB, AB, ABA, ABB all have monitors
 */
static void
test_monitor_path_monitors_030 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add paths */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_remove_path (monitor,
                                                fixture->test_dir[TEST_DIRECTORY_AA],
                                                &error), ==, TRUE);
  g_assert (!error);

  /* Get AA path object, shouldn't exist */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_AA]);
  g_assert (!path);

  /* Get A path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* All have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AAB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_AB]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABA]));
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_ABB]));

  g_object_unref (path);
  g_object_unref (monitor);
}

/* A is ignored, then create AZ directory.
 *  -A, AZ do not have monitors
 */
static void
test_monitor_path_monitors_031 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, directory));

  /* Clean exit */
  g_assert (test_common_directory_delete (directory));
  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

/* Create AZ directory, if A is ignored, then AZ directory is removed.
 *  -A, AZ do not have monitors
 */
static void
test_monitor_path_monitors_032 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Remove directory */
  g_assert (test_common_directory_delete (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (!octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, directory));

  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

/* A is monitored, then create AZ directory.
 *  -A has monitor
 *  -AZ does not have monitor
 */
static void
test_monitor_path_monitors_033 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, directory));

  /* Clean exit */
  g_assert (test_common_directory_delete (directory));
  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

/* Create AZ directory, if A is monitored, then AZ directory is removed.
 *  -A has monitor.
 *  -AZ does not have monitors
 */
static void
test_monitor_path_monitors_034 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Remove directory */
  g_assert (test_common_directory_delete (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, directory));

  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

/* A is monitored recursively, then create AZ directory.
 *  -A and AZ have monitors
 */
static void
test_monitor_path_monitors_035 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (octopus_path_has_monitor (path, directory));

  /* Clean exit */
  g_assert (test_common_directory_delete (directory));
  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

/* Create AZ directory, if A is monitored recursively, then AZ directory is
 * removed.
 *  -A has monitor.
 *  -AZ does not have monitors
 */
static void
test_monitor_path_monitors_036 (TestCommonContext *fixture,
                                gconstpointer      data)
{
  OctopusMonitor *monitor;
  OctopusPath *path;
  GError *error = NULL;
  GFile *directory;

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  /* Remove directory */
  g_assert (test_common_directory_delete (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Get path object */
  path = octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]);
  g_assert (path);

  /* Check if they have monitors */
  g_assert (octopus_path_has_monitor (path, fixture->test_dir[TEST_DIRECTORY_A]));
  g_assert (!octopus_path_has_monitor (path, directory));

  g_object_unref (directory);
  g_object_unref (path);
  g_object_unref (monitor);
}

void
test_monitor_path_monitors_tests (void)
{
  octopus_test_add ("/octopus/monitor/path/monitors/001", test_monitor_path_monitors_001);
  octopus_test_add ("/octopus/monitor/path/monitors/002", test_monitor_path_monitors_002);
  octopus_test_add ("/octopus/monitor/path/monitors/003", test_monitor_path_monitors_003);
  octopus_test_add ("/octopus/monitor/path/monitors/004", test_monitor_path_monitors_004);
  octopus_test_add ("/octopus/monitor/path/monitors/005", test_monitor_path_monitors_005);
  octopus_test_add ("/octopus/monitor/path/monitors/006", test_monitor_path_monitors_006);
  octopus_test_add ("/octopus/monitor/path/monitors/007", test_monitor_path_monitors_007);
  octopus_test_add ("/octopus/monitor/path/monitors/008", test_monitor_path_monitors_008);
  octopus_test_add ("/octopus/monitor/path/monitors/009", test_monitor_path_monitors_009);
  octopus_test_add ("/octopus/monitor/path/monitors/010", test_monitor_path_monitors_010);
  octopus_test_add ("/octopus/monitor/path/monitors/011", test_monitor_path_monitors_011);
  octopus_test_add ("/octopus/monitor/path/monitors/012", test_monitor_path_monitors_012);
  octopus_test_add ("/octopus/monitor/path/monitors/013", test_monitor_path_monitors_013);
  octopus_test_add ("/octopus/monitor/path/monitors/014", test_monitor_path_monitors_014);
  octopus_test_add ("/octopus/monitor/path/monitors/015", test_monitor_path_monitors_015);
  octopus_test_add ("/octopus/monitor/path/monitors/016", test_monitor_path_monitors_016);
  octopus_test_add ("/octopus/monitor/path/monitors/017", test_monitor_path_monitors_017);
  octopus_test_add ("/octopus/monitor/path/monitors/018", test_monitor_path_monitors_018);
  octopus_test_add ("/octopus/monitor/path/monitors/019", test_monitor_path_monitors_019);
  octopus_test_add ("/octopus/monitor/path/monitors/020", test_monitor_path_monitors_020);
  octopus_test_add ("/octopus/monitor/path/monitors/021", test_monitor_path_monitors_021);
  octopus_test_add ("/octopus/monitor/path/monitors/022", test_monitor_path_monitors_022);
  octopus_test_add ("/octopus/monitor/path/monitors/023", test_monitor_path_monitors_023);
  octopus_test_add ("/octopus/monitor/path/monitors/024", test_monitor_path_monitors_024);
  octopus_test_add ("/octopus/monitor/path/monitors/025", test_monitor_path_monitors_025);
  octopus_test_add ("/octopus/monitor/path/monitors/026", test_monitor_path_monitors_026);
  octopus_test_add ("/octopus/monitor/path/monitors/027", test_monitor_path_monitors_027);
  octopus_test_add ("/octopus/monitor/path/monitors/028", test_monitor_path_monitors_028);
  octopus_test_add ("/octopus/monitor/path/monitors/029", test_monitor_path_monitors_029);
  octopus_test_add ("/octopus/monitor/path/monitors/030", test_monitor_path_monitors_030);
  octopus_test_add ("/octopus/monitor/path/monitors/031", test_monitor_path_monitors_031);
  octopus_test_add ("/octopus/monitor/path/monitors/032", test_monitor_path_monitors_032);
  octopus_test_add ("/octopus/monitor/path/monitors/033", test_monitor_path_monitors_033);
  octopus_test_add ("/octopus/monitor/path/monitors/034", test_monitor_path_monitors_034);
  octopus_test_add ("/octopus/monitor/path/monitors/035", test_monitor_path_monitors_035);
  octopus_test_add ("/octopus/monitor/path/monitors/036", test_monitor_path_monitors_036);
}
