/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>

#include <test-common-context.h>

#define LOOP_WAIT_TIME 2

/* Create new disabled monitor, add new EXISTING path synchronously, then enable */
static void
test_monitor_status_001 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (FALSE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* And enable */
  g_assert (octopus_monitor_enable (monitor, NULL, &error));
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (monitor);
}

/* Create new enabled monitor, add new EXISTING path synchronously */
static void
test_monitor_status_002 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (monitor);
}

/* Create new disabled monitor, add new UNEXISTING path synchronously, then enable.
 * During ENABLING, it must report an error */
static void
test_monitor_status_003 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (FALSE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->unexisting_dir,
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* And enable. ERROR must be reported */
  g_assert (!octopus_monitor_enable (monitor, NULL, &error));
  g_assert (error);
  g_clear_error (&error);

  /* INCONSISTENT state must be reported */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_INCONSISTENT);

  /* Now, fix the inconsistent state by removing the path */
  g_assert (octopus_monitor_remove_path (monitor, fixture->unexisting_dir, &error));
  g_assert (!error);

  /* We should still be DISABLED, as the previous enable() failed. */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* Try to enable again */
  g_assert (octopus_monitor_enable (monitor, NULL, &error));
  g_assert (!error);

  /* ENABLED state must be reported */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (monitor);
}

/* Create new enabled monitor, add new UNEXISTING path synchronously.
 * During adding the new path, it must report an error */
static void
test_monitor_status_004 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Add path */
  g_assert (!octopus_monitor_add_path (monitor,
                                       fixture->unexisting_dir,
                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                       NULL,
                                       &error));
  g_assert (error);
  g_error_free (error);

  /* Failed adding path, but we were able to recover in this case, so ENABLED */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (monitor);
}

/* Create new enabled monitor, add new existing path synchronously, but it was already
 * cancelled, so should fail. */
static void
test_monitor_status_005 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GCancellable *cancellable;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Create cancellable */
  cancellable = g_cancellable_new ();

  /* Set it cancelled already */
  g_cancellable_cancel (cancellable);

  /* Add path, should take long to crawl it */
  g_assert (!octopus_monitor_add_path (monitor,
                                       fixture->test_dir[TEST_DIRECTORY_A],
                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                       cancellable,
                                       &error));
  g_assert (error);
  g_error_free (error);

  /* Failed adding path, but we were able to recover in this case, so ENABLED */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* We should not have the path configured */
  g_assert (!octopus_monitor_get_path_at (monitor, fixture->test_dir[TEST_DIRECTORY_A]));

  g_object_unref (cancellable);
  g_object_unref (monitor);
}

static gpointer
cancel_in_100_ms (gpointer data)
{
  g_usleep (100*1000);
  g_cancellable_cancel (G_CANCELLABLE (data));
  g_object_unref (data);
  return NULL;
}

/* Create new enabled monitor, add new existing path synchronously, but it was already
 * cancelled, so should fail. */
static void
test_monitor_status_006 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GFile *root;
  GError *error = NULL;
  OctopusMonitor *monitor;
  GCancellable *cancellable;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Create cancellable and root path to monitor */
  cancellable = g_cancellable_new ();
  root = g_file_new_for_path ("/usr");

  g_assert (g_thread_create (cancel_in_100_ms,
                             g_object_ref (cancellable),
                             FALSE,
                             &error));
  g_assert (!error);

  /* Add path, should take long to crawl it */
  g_assert (!octopus_monitor_add_path (monitor,
                                       root,
                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                       cancellable,
                                       &error));
  g_assert (error);
  g_error_free (error);

  /* Failed adding path, but we were able to recover in this case, so ENABLED */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* We should not have the path configured */
  g_assert (!octopus_monitor_get_path_at (monitor, root));

  g_object_unref (cancellable);
  g_object_unref (monitor);
  g_object_unref (root);
}

/* Create new enabled monitor, add new EXISTING path synchronously.
 * Then, remove that path. Status should be inconsistent. */
static void
test_monitor_status_007 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *directory;

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      directory,
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Remove directory */
  g_assert (test_common_directory_delete (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* Now, inconsistent, as there is a OctopusPath but the real path doesn't exist. */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_INCONSISTENT);

  /* Now, fix the inconsistent state by removing the path */
  g_assert (octopus_monitor_remove_path (monitor, directory, &error));
  g_assert (!error);

  /* ENABLED state must be reported */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (directory);
  g_object_unref (monitor);
}

/* Create new enabled monitor, add new EXISTING path synchronously and
 * recursively. Then, remove a subdir. Status should be ENABLED. */
static void
test_monitor_status_008 (TestCommonContext *fixture,
                         gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GFile *directory;

  /* Create test directory */
  directory = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], "AZ");
  g_assert (test_common_directory_create (directory));

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  /* Remove directory */
  g_assert (test_common_directory_delete (directory));

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* ENABLED state must be reported */
  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_ENABLED);

  g_object_unref (directory);
  g_object_unref (monitor);
}

void
test_monitor_status_tests (void)
{
  octopus_test_add ("/octopus/monitor/status/001", test_monitor_status_001);
  octopus_test_add ("/octopus/monitor/status/002", test_monitor_status_002);
  octopus_test_add ("/octopus/monitor/status/003", test_monitor_status_003);
  octopus_test_add ("/octopus/monitor/status/004", test_monitor_status_004);
  octopus_test_add ("/octopus/monitor/status/005", test_monitor_status_005);
  octopus_test_add ("/octopus/monitor/status/006", test_monitor_status_006);
  octopus_test_add ("/octopus/monitor/status/007", test_monitor_status_007);
  octopus_test_add ("/octopus/monitor/status/008", test_monitor_status_008);
}
