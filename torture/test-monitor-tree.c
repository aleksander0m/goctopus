/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>
#include <test-common-context.h>

/* If A is not configured,
 *  -AA, AAA, AAB, AB, ABA and ABB are NOT monitored
 */
static void
test_monitor_tree_001 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is ignored:
 *  -AA, AAA, AAB, AB, ABA and ABB are NOT monitored
 */
static void
test_monitor_tree_002 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is monitored (NOT recursively):
 *  -AA and AB are monitored
 *  -AAA, AAB, ABA and ABB are NOT monitored
 */
static void
test_monitor_tree_003 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is recursively monitored:
 *  -AA, AB, AAA, AAB, ABA and ABB are monitored
 */
static void
test_monitor_tree_004 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_ABB]), ==, TRUE);

  g_object_unref (monitor);
}

/* If A is ignored and AA is ignored
 *  -AAA and AAB are NOT monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_005 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is ignored and AA is monitored (not recursively),
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_006 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}


/* If A is ignored and AA is recursively monitored,
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are monitored
 */
static void
test_monitor_tree_007 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is ignored
 *  -AAA and AAB are NOT monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_008 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is monitored (not recursively),
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_009 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is monitored (not recursively) and AA is recursively monitored,
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are monitored
 */
static void
test_monitor_tree_010 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (monitor);
}

/* If A is monitored recursively and AA is ignored
 *  -AAA and AAB are NOT monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_011 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_IGNORE,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}

/* If A is monitored recursively and AA is monitored (not recursively),
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are NOT monitored
 */
static void
test_monitor_tree_012 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, FALSE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, FALSE);

  g_object_unref (monitor);
}


/* If A is monitored recursively and AA is recursively monitored,
 *  -AAA and AAB are monitored
 *  -AAAA and AAAB are monitored
 */
static void
test_monitor_tree_013 (TestCommonContext *fixture,
                       gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  monitor = octopus_monitor_new (TRUE);
  g_assert (monitor);
  g_assert (OCTOPUS_IS_MONITOR (monitor));

  /* Add path */
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_A],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);
  g_assert_cmpint (octopus_monitor_add_path (monitor,
                                             fixture->test_dir[TEST_DIRECTORY_AA],
                                             OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                             NULL,
                                             &error), ==, TRUE);
  g_assert (!error);

  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAB]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAA]), ==, TRUE);
  g_assert_cmpint (octopus_monitor_is_file_monitored (monitor, fixture->test_dir[TEST_DIRECTORY_AAAB]), ==, TRUE);

  g_object_unref (monitor);
}

void
test_monitor_tree_tests (void)
{
  octopus_test_add ("/octopus/monitor/tree/001", test_monitor_tree_001);
  octopus_test_add ("/octopus/monitor/tree/002", test_monitor_tree_002);
  octopus_test_add ("/octopus/monitor/tree/003", test_monitor_tree_003);
  octopus_test_add ("/octopus/monitor/tree/004", test_monitor_tree_004);
  octopus_test_add ("/octopus/monitor/tree/005", test_monitor_tree_005);
  octopus_test_add ("/octopus/monitor/tree/006", test_monitor_tree_006);
  octopus_test_add ("/octopus/monitor/tree/007", test_monitor_tree_007);
  octopus_test_add ("/octopus/monitor/tree/008", test_monitor_tree_008);
  octopus_test_add ("/octopus/monitor/tree/009", test_monitor_tree_009);
  octopus_test_add ("/octopus/monitor/tree/010", test_monitor_tree_010);
  octopus_test_add ("/octopus/monitor/tree/011", test_monitor_tree_011);
  octopus_test_add ("/octopus/monitor/tree/012", test_monitor_tree_012);
  octopus_test_add ("/octopus/monitor/tree/013", test_monitor_tree_013);
}
