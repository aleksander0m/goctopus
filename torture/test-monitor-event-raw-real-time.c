/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <octopus.h>

#include <test-common-context.h>

#define LOOP_WAIT_TIME 2

/* Create new disabled monitor with real-time event support */
static void
test_monitor_event_raw_real_time_001 (TestCommonContext *fixture,
                                      gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GList *list = NULL;

  monitor = octopus_monitor_new_real_time (FALSE, FALSE);

  /* Connect monitor to store events in the list */
  test_common_event_list_connect (monitor, &list);

  g_assert_cmpint (octopus_monitor_get_status (monitor), ==, OCTOPUS_STATUS_DISABLED);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR_RECURSIVELY,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* There shouldn't be any event in the list! */
  g_assert (!list);

  /* And enable */
  g_assert_cmpint (octopus_monitor_enable (monitor, NULL, &error), ==, TRUE);
  g_assert (!error);

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* We should have events here */
  g_assert (list);

  /* Clean exit */
  test_common_event_list_clean (&list);
  g_object_unref (monitor);
}

/* Create new enabled monitor with real-time event support,
 * and add ignored path: no events should be reported */
static void
test_monitor_event_raw_real_time_002 (TestCommonContext *fixture,
                                      gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GList *list = NULL;

  monitor = octopus_monitor_new_real_time (TRUE, FALSE);

  /* Connect monitor to store events in the list */
  test_common_event_list_connect (monitor, &list);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_IGNORE,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* There shouldn't be any event in the list! */
  g_assert (!list);

  /* Clean exit */
  test_common_event_list_clean (&list);
  g_object_unref (monitor);
}

/* Create new enabled monitor with real-time event support,
 * and add path to monitor (not recursively):
 * We should have one FOUND event for each file/directory inside the root
 * directory. */
static void
test_monitor_event_raw_real_time_003 (TestCommonContext *fixture,
                                      gconstpointer      data)
{
  GError *error = NULL;
  OctopusMonitor *monitor;
  GList *list = NULL;

  monitor = octopus_monitor_new_real_time (TRUE, FALSE);

  /* Connect monitor to store events in the list */
  test_common_event_list_connect (monitor, &list);

  /* Add path */
  g_assert (octopus_monitor_add_path (monitor,
                                      fixture->test_dir[TEST_DIRECTORY_A],
                                      OCTOPUS_ACTION_MONITOR,
                                      NULL,
                                      &error));
  g_assert (!error);

  /* Run loop */
  test_common_run_loop (fixture, LOOP_WAIT_TIME);

  /* We should have events here */
  g_assert (list);

  /* Then we should have 2 FOUND events, for AA and AB. But we cannot assume
   * the order of the events here... So, first check that the remaining number
   * of events is 2. */
  g_assert_cmpint (g_list_length (list), ==, 2);

  /* Then, check that we have events for the files */
  g_assert (test_common_event_list_find (list,
                                         fixture->test_dir[TEST_DIRECTORY_AA],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));
  g_assert (test_common_event_list_find (list,
                                         fixture->test_dir[TEST_DIRECTORY_AB],
                                         NULL,
                                         OCTOPUS_EVENT_TYPE_FOUND));

  test_common_event_list_clean (&list);

  g_object_unref (monitor);
}

/* /\* Create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * We should have one FOUND event for each subdirectory inside the hierarchy */
/*  * given by the configured path *\/ */
/* static void */
/* test_monitor_event_raw_real_time_004 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   /\* iterator should NOT be null *\/ */
/*   g_assert (it); */

/*   /\* Then we should have 8 FOUND events *\/ */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   g_assert (octopus_event_iterator_next (it)); */
/*   /\* No more events... *\/ */
/*   g_assert (!octopus_event_iterator_next (it)); */

/*   /\* Then, check that we have events for the files *\/ */
/*   octopus_event_iterator_rewind (it); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AA], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AAAA], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AAAB], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AAB], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_AB], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_ABA], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          fixture->test_dir[TEST_DIRECTORY_ABB], */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_FOUND)); */

/*   octopus_event_iterator_destroy (it); */

/*   g_object_unref (monitor); */
/* } */

/* /\* Create new enabled monitor with real-time event support, */
/*  * and add path to ignore: */
/*  * Create a file in the monitored directory, and check no CREATED event */
/*  * is issued. *\/ */
/* static void */
/* test_monitor_event_raw_real_time_005 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */

/*   /\* Create a file inside the monitored structure *\/ */
/*   g_assert (test_common_file_create (test_file, "whatever")); */
/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Create a file in the monitored directory, and look for a CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_006 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */

/*   /\* Create a file inside the monitored structure *\/ */
/*   g_assert (test_common_file_create (test_file, "whatever")); */
/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Create a file in the subdirs of the monitored directory, and make sure */
/*  * there is no CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_007 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */

/*   /\* Create a file inside the monitored structure *\/ */
/*   g_assert (test_common_file_create (test_file, "whatever")); */
/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * Create a file in the hierarchy, and look for a CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_008 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */

/*   /\* Create a file inside the monitored structure *\/ */
/*   g_assert (test_common_file_create (test_file, "whatever")); */
/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to ignore: */
/*  * Update the file from the hierarchy, and check there is no UPDATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_009 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Update the file *\/ */
/*   g_assert (test_common_file_update (test_file, "newtext")); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_UPDATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Update the file in the directory, and check there is an UPDATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_010 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Update the file *\/ */
/*   g_assert (test_common_file_update (test_file, "newtext")); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_UPDATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Update the file in a subdirectory, and check there is no UPDATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_011 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Update the file *\/ */
/*   g_assert (test_common_file_update (test_file, "newtext")); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_UPDATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * Update the file from the hierarchy, and look for an UPDATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_012 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Update the file *\/ */
/*   g_assert (test_common_file_update (test_file, "newtext")); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_UPDATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file)); */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to ignore: */
/*  * Delete the file from the hierarchy, and check there is no DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_013 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Delete the file *\/ */
/*   g_assert (test_common_file_delete (test_file)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Delete the file from the directory, and check there is a DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_014 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Delete the file *\/ */
/*   g_assert (test_common_file_delete (test_file)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Delete the file from the subdirectory, and check there is no DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_015 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Delete the file *\/ */
/*   g_assert (test_common_file_delete (test_file)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * Delete the file from the hierarchy, and look for a DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_016 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file; */

/*   /\* Create a test file *\/ */
/*   test_file = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                 "file.txt"); */
/*   g_assert (test_common_file_create (test_file, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Delete the file *\/ */
/*   g_assert (test_common_file_delete (test_file)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_object_unref (test_file); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to ignore: */
/*  * Move the file to another monitored path, and check there is no MOVED, */
/*  * DELETED or CREATED events *\/ */
/* static void */
/* test_monitor_event_raw_real_time_017 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_src, */
/*                                           test_file_dest, */
/*                                           OCTOPUS_EVENT_TYPE_MOVED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_src, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_dest, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Move the file to the same monitored path, and check there is a MOVED event, and */
/*  * no DELETED or CREATED events *\/ */
/* static void */
/* test_monitor_event_raw_real_time_018 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_src, */
/*                                          test_file_dest, */
/*                                          OCTOPUS_EVENT_TYPE_MOVED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_src, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_dest, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * Move the file to another monitored path, and look for a MOVED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_019 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_src, */
/*                                          test_file_dest, */
/*                                          OCTOPUS_EVENT_TYPE_MOVED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_src, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_dest, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to ignore: */
/*  * Move the file to another path, and check there is no DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_020 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_src, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (not recursively): */
/*  * Move the file to another path, and check there is a DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_021 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_src, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file, then create new enabled monitor with real-time event support, */
/*  * and add path to monitor (recursively): */
/*  * Move the file to a non-monitored path, and look for a DELETED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_022 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AAA], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_src, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_DELETED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file in a non-monitored path, then create new enabled monitor */
/*  * with real-time event support, and add path to ignore: */
/*  * Move the file to the path, and check there is no CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_023 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_IGNORE, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (!test_common_event_list_connect (list, */
/*                                           test_file_dest, */
/*                                           NULL, */
/*                                           OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file in a non-monitored path, then create new enabled monitor */
/*  * with real-time event support, and add path to monitor (not recursively): */
/*  * Move the file to the path, and check there is a CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_024 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_A], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_dest, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

/* /\* Create a test file in a non-monitored path, then create new enabled monitor */
/*  * with real-time event support, and add path to monitor (recursively): */
/*  * Move the file to a monitored path, and look for a CREATED event *\/ */
/* static void */
/* test_monitor_event_raw_real_time_025 (TestCommonContext *fixture, */
/*                                       gconstpointer      data) */
/* { */
/*   OctopusEventIterator *it; */
/*   GError *error = NULL; */
/*   OctopusMonitor *monitor; */
/*   const OctopusEvent *event; */
/*   GFile *test_file_src; */
/*   GFile *test_file_dest; */

/*   /\* Create a test file *\/ */
/*   test_file_src = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_B], */
/*                                     "file.txt"); */
/*   g_assert (test_common_file_create (test_file_src, "whatever")); */

/*   monitor = octopus_monitor_new_real_time (TRUE, FALSE); */

/*   /\* Add path *\/ */
/*   g_assert (octopus_monitor_add_path (monitor, */
/*                                       fixture->test_dir[TEST_DIRECTORY_A], */
/*                                       OCTOPUS_ACTION_MONITOR_RECURSIVELY, */
/*                                       NULL, */
/*                                       &error)); */
/*   g_assert (!error); */

/*   /\* Get all found events and remove them *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Move the file to a monitored directory (B) *\/ */
/*   test_file_dest = g_file_get_child (fixture->test_dir[TEST_DIRECTORY_AA], */
/*                                      "renamed.txt"); */
/*   g_assert (test_common_file_rename (test_file_src, test_file_dest)); */

/*   /\* Run loop *\/ */
/*   test_common_run_loop (fixture, LOOP_WAIT_TIME); */

/*   /\* Get all events *\/ */
/*   it = octopus_monitor_get_events (monitor); */
/*   g_assert (test_common_event_list_connect (list, */
/*                                          test_file_dest, */
/*                                          NULL, */
/*                                          OCTOPUS_EVENT_TYPE_CREATED)); */
/*   octopus_event_iterator_destroy (it); */

/*   /\* Clean exit *\/ */
/*   g_assert (test_common_file_delete (test_file_dest)); */
/*   g_object_unref (test_file_src); */
/*   g_object_unref (test_file_dest); */
/*   g_object_unref (monitor); */
/* } */

void
test_monitor_event_raw_real_time_tests (void)
{
  octopus_test_add ("/octopus/monitor/event/raw/real-time/001", test_monitor_event_raw_real_time_001);
  octopus_test_add ("/octopus/monitor/event/raw/real-time/002", test_monitor_event_raw_real_time_002);
  octopus_test_add ("/octopus/monitor/event/raw/real-time/003", test_monitor_event_raw_real_time_003);
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/004", test_monitor_event_raw_real_time_004); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/005", test_monitor_event_raw_real_time_005); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/006", test_monitor_event_raw_real_time_006); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/007", test_monitor_event_raw_real_time_007); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/008", test_monitor_event_raw_real_time_008); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/009", test_monitor_event_raw_real_time_009); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/010", test_monitor_event_raw_real_time_010); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/011", test_monitor_event_raw_real_time_011); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/012", test_monitor_event_raw_real_time_012); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/013", test_monitor_event_raw_real_time_013); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/014", test_monitor_event_raw_real_time_014); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/015", test_monitor_event_raw_real_time_015); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/016", test_monitor_event_raw_real_time_016); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/017", test_monitor_event_raw_real_time_017); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/018", test_monitor_event_raw_real_time_018); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/019", test_monitor_event_raw_real_time_019); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/010", test_monitor_event_raw_real_time_010); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/021", test_monitor_event_raw_real_time_021); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/022", test_monitor_event_raw_real_time_022); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/023", test_monitor_event_raw_real_time_023); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/024", test_monitor_event_raw_real_time_024); */
  /* octopus_test_add ("/octopus/monitor/event/raw/real-time/025", test_monitor_event_raw_real_time_025); */
}
