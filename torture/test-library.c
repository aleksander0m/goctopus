/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>

#include <glib.h>

#include <octopus.h>
#include <test-common-context.h>

extern void test_crawler_generic_tests (void);
extern void test_crawler_tree_tests (void);
extern void test_crawler_path_iterator_tests (void);

extern void test_monitor_generic_tests (void);
extern void test_monitor_status_tests (void);
extern void test_monitor_tree_tests (void);
extern void test_monitor_path_iterator_tests (void);
extern void test_monitor_path_monitors_tests (void);
extern void test_monitor_event_raw_on_demand_tests (void);
extern void test_monitor_event_merge_on_demand_tests (void);
extern void test_monitor_event_raw_real_time_tests (void);

gint main (gint argc, gchar **argv)
{
  g_thread_init (NULL);
  g_type_init ();
  g_test_init (&argc, &argv, NULL);

  g_test_message ("Testing The Octopus Library");

  /*---- Crawler tests ----*/

  /* Basic API tests */
  test_crawler_generic_tests ();
  /* Tests for the internal tree of actions */
  test_crawler_tree_tests ();
  /* Tests for the internal tree of actions */
  test_crawler_tree_tests ();
  /* Tests for the paths and path iterator */
  test_crawler_path_iterator_tests ();

  /*---- Monitor tests ----*/

  /* Basic API tests */
  test_monitor_generic_tests ();
  /* Status tests */
  test_monitor_status_tests ();
  /* Tests for the internal tree of actions */
  test_monitor_tree_tests ();
  /* Tests for the paths and path iterator */
  test_monitor_path_iterator_tests ();
  /* Tests for the paths and monitors */
  test_monitor_path_monitors_tests ();
  /* Tests for on-demand RAW event iteration mode */
  test_monitor_event_raw_on_demand_tests ();
  /* Tests for on-demand MERGE event iteration mode */
  test_monitor_event_merge_on_demand_tests ();
  /* Tests for real-time RAW event notification mode */
  test_monitor_event_raw_real_time_tests ();

  return g_test_run ();
}
