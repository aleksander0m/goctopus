/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event-found.h"


#define OCTOPUS_EVENT_FOUND_GET_PRIVATE(obj)                            \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_EVENT_FOUND, OctopusEventFoundPrivate))

/* Private data of the OctopusEventFound */
struct _OctopusEventFoundPrivate {
  GFileInfo *source_file_info;
};

static void   octopus_event_found_dispose      (GObject *object);
static void   octopus_event_found_get_property (GObject      *object,
                                                guint         prop_id,
                                                GValue       *value,
                                                GParamSpec   *pspec);

static gchar                   *octopus_event_found_create_printable (const OctopusEvent *event);
static OctopusEventMergeResult  octopus_event_found_merge            (OctopusEvent  *event,
                                                                      OctopusEvent  *next,
                                                                      OctopusEvent **merged);

G_DEFINE_TYPE(OctopusEventFound, octopus_event_found, OCTOPUS_TYPE_EVENT)

enum {
  PROP_0,
  PROP_SOURCE_FILE_INFO
};

static void
octopus_event_found_class_init (OctopusEventFoundClass *klass)
{
  OctopusEventClass *parent_class;
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = octopus_event_found_dispose;
  object_class->get_property = octopus_event_found_get_property;

  parent_class = OCTOPUS_EVENT_CLASS (klass);
  parent_class->create_printable = octopus_event_found_create_printable;
  parent_class->merge = octopus_event_found_merge;

  g_type_class_add_private (object_class, sizeof (OctopusEventFoundPrivate));

  g_object_class_install_property (object_class,
                                   PROP_SOURCE_FILE_INFO,
                                   g_param_spec_object ("event-source-file-info",
                                                        "Event Source File Info",
                                                        "Information of the source file in the event",
                                                        G_TYPE_FILE_INFO,
                                                        G_PARAM_READABLE));
}

static void
octopus_event_found_init (OctopusEventFound *object)
{
  /* Setup private pointer */
  object->private = OCTOPUS_EVENT_FOUND_GET_PRIVATE (object);
}

static void
octopus_event_found_dispose (GObject *object)
{
  OctopusEventFound *event = OCTOPUS_EVENT_FOUND (object);

  if (event->private->source_file_info)
    {
      g_object_unref (event->private->source_file_info);
      event->private->source_file_info = NULL;
    }

  G_OBJECT_CLASS (octopus_event_found_parent_class)->dispose (object);
}

static void
octopus_event_found_get_property (GObject      *object,
                                  guint         prop_id,
                                  GValue       *value,
                                  GParamSpec   *pspec)
{
  OctopusEventFound *event = OCTOPUS_EVENT_FOUND (object);

  switch (prop_id)
    {
    case PROP_SOURCE_FILE_INFO:
      if (event->private->source_file_info)
        g_value_set_object (value, event->private->source_file_info);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static gchar *
octopus_event_found_create_printable (const OctopusEvent *event)
{
  gchar *source_uri;
  gchar *printable;
  GFile *source_file;

  source_file = octopus_event_get_source_file (event);
  source_uri = g_file_get_uri (source_file);

  printable = g_strdup_printf ("[ FOUND ] '%s'", source_uri);

  g_free (source_uri);
  g_object_unref (source_file);
  return printable;
}

static OctopusEventMergeResult
octopus_event_found_merge (OctopusEvent  *event,
                           OctopusEvent  *next,
                           OctopusEvent **merged)
{
  OctopusEventType next_type;

  next_type = octopus_event_get_event_type (next);
  switch (next_type)
    {
    case OCTOPUS_EVENT_TYPE_UPDATED:
      {
        OctopusEventFound *event_found;
        GFile *source_file;

        event_found = OCTOPUS_EVENT_FOUND (event);
        source_file = octopus_event_get_source_file (event);

        /* FOUND(A)  + UPDATE(A)  = FOUND(A) */
        *merged = g_object_ref (event);

        /* After merging with an update, we clear the file info,
         * as it is no longer valid */
        if (event_found->private->source_file_info)
          {
            g_object_unref (event_found->private->source_file_info);
            event_found->private->source_file_info = NULL;
          }

        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_DELETED:
      {
        /* FOUND(A)  + DELETE(A)  = DELETE(A) */
        *merged = g_object_ref (next);
        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_MOVED:
    case OCTOPUS_EVENT_TYPE_ERROR:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE;
      }
    default:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED;
      }
    }
}

/**
 * octopus_event_found_get_file_info:
 * @event: a #OctopusEventFound
 *
 * Returns the file information of the found file.
 *
 * Returns: a new reference to a #GFileInfo, or #NULL if the original info
 * retrieved during crawling is no longer valid.
 * Call g_object_unref() on it when no longer used.
 */
GFileInfo *
octopus_event_found_get_file_info (OctopusEventFound *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT_FOUND (event), FALSE);

  return (event->private->source_file_info ?
          g_object_ref (event->private->source_file_info) :
          NULL);
}

OctopusEvent *
_octopus_event_found_new (GFile     *source_file,
                          GFileInfo *source_file_info)
{
  OctopusEventFound *event;

  event = g_object_new (OCTOPUS_TYPE_EVENT_FOUND, NULL);
  _octopus_event_initialize (OCTOPUS_EVENT (event), source_file, OCTOPUS_EVENT_TYPE_FOUND);
  event->private->source_file_info = g_object_ref (source_file_info);
  return OCTOPUS_EVENT (event);
}
