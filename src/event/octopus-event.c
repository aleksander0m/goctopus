/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event.h"

#include <octopus-common-utils.h>

#define OCTOPUS_EVENT_GET_PRIVATE(obj)                                  \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_EVENT, OctopusEventPrivate))

/* Private data of the OctopusEvent */
struct _OctopusEventPrivate {
  /* Specific event */
  OctopusEventType type;
  /* File/Directory affected by the event */
  GFile *source_file;
  /* When exactly the event was created */
  GTimeVal epoch;
  /* Printable event string, if ever requested */
  gchar *printable;
};

enum {
  PROP_0,
  PROP_TYPE,
  PROP_SOURCE_FILE
};

static void octopus_event_finalize     (GObject      *object);
static void octopus_event_dispose      (GObject      *object);
static void octopus_event_get_property (GObject      *object,
                                        guint         prop_id,
                                        GValue       *value,
                                        GParamSpec   *pspec);

G_DEFINE_TYPE(OctopusEvent, octopus_event, G_TYPE_OBJECT)


static void
octopus_event_class_init (OctopusEventClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = octopus_event_finalize;
  object_class->dispose = octopus_event_dispose;

  object_class->get_property = octopus_event_get_property;

  klass->create_printable = NULL;

  g_type_class_add_private (object_class, sizeof (OctopusEventPrivate));

  g_object_class_install_property (object_class,
                                   PROP_TYPE,
                                   g_param_spec_enum ("event-type",
                                                      "Event Type",
                                                      "Type of event",
                                                      octopus_event_type_get_type (),
                                                      OCTOPUS_EVENT_TYPE_UNKNOWN,
                                                      G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_SOURCE_FILE,
                                   g_param_spec_object ("event-source-file",
                                                        "Event Source File",
                                                        "Source file for the event",
                                                        G_TYPE_FILE,
                                                        G_PARAM_READABLE));
}

static void
octopus_event_init (OctopusEvent *object)
{
  /* Setup private pointer */
  object->private = OCTOPUS_EVENT_GET_PRIVATE (object);

  /* Setup time of the event */
  g_get_current_time (&(object->private->epoch));
}

static void
octopus_event_finalize (GObject *object)
{
  OctopusEvent *event = OCTOPUS_EVENT (object);

  if (event->private->printable)
    {
      g_free (event->private->printable);
    }

  G_OBJECT_CLASS (octopus_event_parent_class)->finalize (object);
}

static void
octopus_event_dispose (GObject *object)
{
  OctopusEvent *event = OCTOPUS_EVENT (object);

  if (event->private->source_file)
    {
      g_object_unref (event->private->source_file);
      event->private->source_file = NULL;
    }

  G_OBJECT_CLASS (octopus_event_parent_class)->dispose (object);
}

void
_octopus_event_initialize (OctopusEvent     *event,
                           GFile            *source_file,
                           OctopusEventType  type)
{
  event->private->source_file = g_object_ref (source_file);
  event->private->type = type;
}

static void
octopus_event_get_property (GObject      *object,
                            guint         prop_id,
                            GValue       *value,
                            GParamSpec   *pspec)
{
  OctopusEvent *node = OCTOPUS_EVENT (object);

  switch (prop_id)
    {
    case PROP_TYPE:
      g_value_set_enum (value, node->private->type);
      break;
    case PROP_SOURCE_FILE:
      g_value_set_object (value, node->private->source_file);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

/**
 * octopus_event_get_source_file:
 * @event: a #OctopusEvent
 *
 * Returns the #GFile where the event happened.
 *
 * Returns: a new reference to a #GFile. Call g_object_unref() on it when
 * no longer used.
 */
GFile *
octopus_event_get_source_file (const OctopusEvent *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT (event), NULL);

  return g_object_ref (event->private->source_file);
}

/**
 * octopus_event_get_event_type:
 * @event: a #OctopusEvent
 *
 * Returns the type of the #OctopusEvent
 *
 * Returns: a #OctopusEventType.
 */
OctopusEventType
octopus_event_get_event_type (const OctopusEvent *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT (event),
                        OCTOPUS_EVENT_TYPE_UNKNOWN);

  return event->private->type;
}

/**
 * octopus_event_get_epoch:
 * @event: a #OctopusEvent
 * @epoch: a pointer to a #GTimeVal to store the event time.
 *
 * Retrieve the exact time when the event was detected by the #OctopusMonitor.
 */
void
octopus_event_get_epoch (const OctopusEvent *event,
                         GTimeVal           *epoch)
{
  g_return_if_fail (event);
  g_return_if_fail (epoch);

  *epoch = event->private->epoch;
}

/**
 * octopus_event_get_printable:
 * @event: a #OctopusEvent
 *
 * Returns a string showing the contents of the event.
 * Useful for debugging purposes only.
 *
 * Returns: a constant string which should not be freed by the caller.
 */
const gchar *
octopus_event_get_printable (const OctopusEvent *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT (event), NULL);

  if ((!event->private->printable) &&
      (OCTOPUS_EVENT_GET_CLASS (event)->create_printable))
    {
      event->private->printable = OCTOPUS_EVENT_GET_CLASS (event)->create_printable (event);
    }

  return event->private->printable;
}

OctopusEventMergeResult
_octopus_event_merge (OctopusEvent  *event,
                      OctopusEvent  *next,
                      OctopusEvent **merged)
{
  OctopusEventMergeResult merge_result;

  merge_result = OCTOPUS_EVENT_GET_CLASS (event)->merge (event, next, merged);

  if (merge_result == OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED)
    {
      octopus_critical ("UNEXPECTED Event merge detected: (Event1: '%s'; Event2: '%s')",
			octopus_event_get_printable (event),
			octopus_event_get_printable (next));
    }
  else if (merge_result == OCTOPUS_EVENT_MERGE_RESULT_MERGED && *merged)
    {
      /* Set epoch as time of merge */
      g_get_current_time (&((*merged)->private->epoch));
    }
  return merge_result;
}
