/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_MOVED_H__
#define __OCTOPUS_EVENT_MOVED_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "octopus-event.h"

/**
 * SECTION:octopus-event-moved
 * @short_description: Octopus Events to notify moved items
 * @title: OctopusEventMoved
 * @stability: Unstable
 * @include: octopus.h
 *
 * Reporting moved items as Octopus Events.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_EVENT_MOVED            (octopus_event_moved_get_type ())
#define OCTOPUS_EVENT_MOVED(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT_MOVED, OctopusEventMoved))
#define OCTOPUS_EVENT_MOVED_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT_MOVED, OctopusEventMovedClass))
#define OCTOPUS_IS_EVENT_MOVED(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT_MOVED))
#define OCTOPUS_IS_EVENT_MOVED_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT_MOVED))
#define OCTOPUS_EVENT_MOVED_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT_MOVED, OctopusEventMovedClass))

typedef struct _OctopusEventMoved         OctopusEventMoved;
typedef struct _OctopusEventMovedClass    OctopusEventMovedClass;
typedef struct _OctopusEventMovedPrivate  OctopusEventMovedPrivate;

/**
 * OctopusEventMoved:
 *
 * A #OctopusEvent which represents a moved item.
 */
struct _OctopusEventMoved {
  /*< private >*/
  OctopusEvent              parent;
  OctopusEventMovedPrivate *private;
};

struct _OctopusEventMovedClass {
  OctopusEventClass parent;
};

GType         octopus_event_moved_get_type             (void);

GFile        *octopus_event_moved_get_destination_file (OctopusEventMoved *event);

/* END PUBLIC */

OctopusEvent *_octopus_event_moved_new                 (GFile *source_file,
                                                        GFile *destination_file);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_MOVED_H__ */
