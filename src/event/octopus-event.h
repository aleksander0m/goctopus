/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_H__
#define __OCTOPUS_EVENT_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <octopus-enum-types.h>

/**
 * SECTION:octopus-event
 * @short_description: Base class for all Octopus Events
 * @title: OctopusEvent
 * @stability: Unstable
 * @include: octopus.h
 *
 * This section provides the common API of all Octopus Events. All event types
 * share some properties that can be accessed through the same methods, despite
 * the true type of the event.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

/* Private, not documented */
typedef enum {
  OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED,
  OCTOPUS_EVENT_MERGE_RESULT_MERGED,
  OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE,
  OCTOPUS_EVENT_MERGE_RESULT_LAST
} OctopusEventMergeResult;

#define OCTOPUS_TYPE_EVENT            (octopus_event_get_type ())
#define OCTOPUS_EVENT(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT, OctopusEvent))
#define OCTOPUS_EVENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT, OctopusEventClass))
#define OCTOPUS_IS_EVENT(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT))
#define OCTOPUS_IS_EVENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT))
#define OCTOPUS_EVENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT, OctopusEventClass))

typedef struct _OctopusEvent         OctopusEvent;
typedef struct _OctopusEventClass    OctopusEventClass;
typedef struct _OctopusEventPrivate  OctopusEventPrivate;

/**
 * OctopusEvent:
 *
 * The base type of all Octopus Events.
 */
struct _OctopusEvent {
  /*< private >*/
  GObject              parent;
  OctopusEventPrivate *private;
};

struct _OctopusEventClass {
  GObjectClass parent;

  /* Virtual method to create the printable representation of the event */
  gchar * (* create_printable) ( const OctopusEvent *event);

  /* Virtual method to merge events */
  OctopusEventMergeResult (* merge) (OctopusEvent  *event,
                                     OctopusEvent  *next,
                                     OctopusEvent **merged);
};

GType                    octopus_event_get_type        (void);

GFile                   *octopus_event_get_source_file (const OctopusEvent *event);
OctopusEventType         octopus_event_get_event_type  (const OctopusEvent *event);
void                     octopus_event_get_epoch       (const OctopusEvent *event,
                                                        GTimeVal           *epoch);
const gchar             *octopus_event_get_printable   (const OctopusEvent *event);

/* END PUBLIC */

void                     _octopus_event_initialize     (OctopusEvent     *event,
                                                        GFile            *source_file,
                                                        OctopusEventType  type);
OctopusEventMergeResult  _octopus_event_merge          (OctopusEvent  *event,
                                                        OctopusEvent  *next,
                                                        OctopusEvent **merged);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_H__ */
