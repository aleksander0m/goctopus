/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_UPDATED_H__
#define __OCTOPUS_EVENT_UPDATED_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "octopus-event.h"

/**
 * SECTION:octopus-event-updated
 * @short_description: Octopus Events to notify updated items
 * @title: OctopusEventUpdated
 * @stability: Unstable
 * @include: octopus.h
 *
 * Reporting updated items as Octopus Events.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_EVENT_UPDATED            (octopus_event_updated_get_type ())
#define OCTOPUS_EVENT_UPDATED(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT_UPDATED, OctopusEventUpdated))
#define OCTOPUS_EVENT_UPDATED_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT_UPDATED, OctopusEventUpdatedClass))
#define OCTOPUS_IS_EVENT_UPDATED(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT_UPDATED))
#define OCTOPUS_IS_EVENT_UPDATED_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT_UPDATED))
#define OCTOPUS_EVENT_UPDATED_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT_UPDATED, OctopusEventUpdatedClass))

typedef struct _OctopusEventUpdated         OctopusEventUpdated;
typedef struct _OctopusEventUpdatedClass    OctopusEventUpdatedClass;
typedef struct _OctopusEventUpdatedPrivate  OctopusEventUpdatedPrivate;

/**
 * OctopusEventUpdated:
 *
 * A #OctopusEvent which represents an updated item.
 */
struct _OctopusEventUpdated {
  /*< private >*/
  OctopusEvent                parent;
  OctopusEventUpdatedPrivate *private;
};

struct _OctopusEventUpdatedClass {
  OctopusEventClass parent;
};

GType         octopus_event_updated_get_type               (void);

gboolean      octopus_event_updated_get_contents_updated   (OctopusEventUpdated *event);
gboolean      octopus_event_updated_get_attributes_updated (OctopusEventUpdated *event);

/* END PUBLIC */

OctopusEvent *_octopus_event_updated_new                   (GFile    *source_file,
                                                            gboolean  contents_updated,
                                                            gboolean  attributes_updated);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_UPDATED_H__ */
