/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event-moved.h"

#define OCTOPUS_EVENT_MOVED_GET_PRIVATE(obj)                            \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_EVENT_MOVED, OctopusEventMovedPrivate))

/* Private data of the OctopusEventMoved */
struct _OctopusEventMovedPrivate {
  GFile *destination_file;
};

static void   octopus_event_moved_dispose      (GObject *object);
static void   octopus_event_moved_get_property (GObject      *object,
                                                guint         prop_id,
                                                GValue       *value,
                                                GParamSpec   *pspec);

static gchar                   *octopus_event_moved_create_printable (const OctopusEvent *event);
static OctopusEventMergeResult  octopus_event_moved_merge            (OctopusEvent  *event,
                                                                      OctopusEvent  *next,
                                                                      OctopusEvent **merged);

G_DEFINE_TYPE(OctopusEventMoved, octopus_event_moved, OCTOPUS_TYPE_EVENT)

enum {
  PROP_0,
  PROP_DESTINATION_FILE
};

static void
octopus_event_moved_class_init (OctopusEventMovedClass *klass)
{
  OctopusEventClass *parent_class;
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = octopus_event_moved_dispose;
  object_class->get_property = octopus_event_moved_get_property;

  parent_class = OCTOPUS_EVENT_CLASS (klass);
  parent_class->create_printable = octopus_event_moved_create_printable;
  parent_class->merge = octopus_event_moved_merge;

  g_type_class_add_private (object_class, sizeof (OctopusEventMovedPrivate));

  g_object_class_install_property (object_class,
                                   PROP_DESTINATION_FILE,
                                   g_param_spec_object ("event-destination-file",
                                                        "Event Destination File",
                                                        "Destination file for the event",
                                                        G_TYPE_FILE,
                                                        G_PARAM_READABLE));
}

static void
octopus_event_moved_init (OctopusEventMoved *object)
{
  /* Setup private pointer */
  object->private = OCTOPUS_EVENT_MOVED_GET_PRIVATE (object);
}

static void
octopus_event_moved_dispose (GObject *object)
{
  OctopusEventMoved *event = OCTOPUS_EVENT_MOVED (object);

  if (event->private->destination_file)
    {
      g_object_unref (event->private->destination_file);
      event->private->destination_file = NULL;
    }

  G_OBJECT_CLASS (octopus_event_moved_parent_class)->dispose (object);
}

static void
octopus_event_moved_get_property (GObject      *object,
                                  guint         prop_id,
                                  GValue       *value,
                                  GParamSpec   *pspec)
{
  OctopusEventMoved *event = OCTOPUS_EVENT_MOVED (object);

  switch (prop_id)
    {
    case PROP_DESTINATION_FILE:
      g_value_set_object (value, event->private->destination_file);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static gchar *
octopus_event_moved_create_printable (const OctopusEvent *event)
{
  OctopusEventMoved *event_moved = OCTOPUS_EVENT_MOVED (event);
  gchar *destination_uri;
  gchar *source_uri;
  gchar *printable;
  GFile *source_file;

  source_file = octopus_event_get_source_file (event);
  source_uri = g_file_get_uri (source_file);
  destination_uri = g_file_get_uri (event_moved->private->destination_file);

  printable = g_strdup_printf ("[ MOVED ] '%s'->'%s'",
                               source_uri,
                               destination_uri);

  g_free (destination_uri);
  g_free (source_uri);
  g_object_unref (source_file);
  return printable;
}

static OctopusEventMergeResult
octopus_event_moved_merge (OctopusEvent  *event,
                           OctopusEvent  *next,
                           OctopusEvent **merged)
{
  OctopusEventType next_type;

  next_type = octopus_event_get_event_type (next);
  switch (next_type)
    {
    case OCTOPUS_EVENT_TYPE_UPDATED:
    case OCTOPUS_EVENT_TYPE_MOVED:
    case OCTOPUS_EVENT_TYPE_ERROR:
    case OCTOPUS_EVENT_TYPE_CREATED:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE;
      }
    default:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED;
      }
    }
}

/**
 * octopus_event_moved_get_destination_file:
 * @event: a #OctopusEventMoved
 *
 * Returns the destination #GFile, where the item was moved to.
 *
 * Returns: a new reference to a #GFile. Call g_object_unref() on it when
 * no longer used.
 */
GFile *
octopus_event_moved_get_destination_file (OctopusEventMoved *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT_MOVED (event), NULL);

  return g_object_ref (event->private->destination_file);
}

OctopusEvent *
_octopus_event_moved_new (GFile *source_file,
                          GFile *destination_file)
{
  OctopusEventMoved *event;

  event = g_object_new (OCTOPUS_TYPE_EVENT_MOVED, NULL);
  _octopus_event_initialize (OCTOPUS_EVENT (event), source_file, OCTOPUS_EVENT_TYPE_MOVED);
  event->private->destination_file = g_object_ref (destination_file);
  return OCTOPUS_EVENT (event);
}
