/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event-deleted.h"
#include "octopus-event-updated.h"

static gchar                   *octopus_event_deleted_create_printable (const OctopusEvent *event);
static OctopusEventMergeResult  octopus_event_deleted_merge            (OctopusEvent  *event,
                                                                        OctopusEvent  *next,
                                                                        OctopusEvent **merged);

G_DEFINE_TYPE(OctopusEventDeleted, octopus_event_deleted, OCTOPUS_TYPE_EVENT)

static void
octopus_event_deleted_class_init (OctopusEventDeletedClass *klass)
{
  OctopusEventClass *parent_class;
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = OCTOPUS_EVENT_CLASS (klass);
  parent_class->create_printable = octopus_event_deleted_create_printable;
  parent_class->merge = octopus_event_deleted_merge;
}

static void
octopus_event_deleted_init (OctopusEventDeleted *object)
{

}

static gchar *
octopus_event_deleted_create_printable (const OctopusEvent *event)
{
  gchar *source_uri;
  gchar *printable;
  GFile *source_file;

  source_file = octopus_event_get_source_file (event);
  source_uri = g_file_get_uri (source_file);

  printable = g_strdup_printf ("[ DELETED ] '%s'", source_uri);

  g_free (source_uri);
  g_object_unref (source_file);
  return printable;
}

static OctopusEventMergeResult
octopus_event_deleted_merge (OctopusEvent  *event,
                             OctopusEvent  *next,
                             OctopusEvent **merged)
{
  OctopusEventType next_type;

  next_type = octopus_event_get_event_type (next);
  switch (next_type)
    {
    case OCTOPUS_EVENT_TYPE_CREATED:
      {
        GFile *file;

        file = octopus_event_get_source_file (event);
        /* DELETE(A)  + CREATE(A)  = UPDATE(A) */
        *merged = _octopus_event_updated_new (file, TRUE, TRUE);
        g_object_unref (file);
        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_MOVED:
      {
        /* DELETE(A) + MOVE(A->B) = MOVE(A->B) */
        *merged = g_object_ref (next);
        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_ERROR:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE;
      }
    default:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED;
      }
    }
}

OctopusEvent *
_octopus_event_deleted_new (GFile *source_file)
{
  OctopusEvent *event;

  event = g_object_new (OCTOPUS_TYPE_EVENT_DELETED, NULL);
  _octopus_event_initialize (event, source_file, OCTOPUS_EVENT_TYPE_DELETED);
  return event;
}
