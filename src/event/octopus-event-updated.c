/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event-updated.h"

#define OCTOPUS_EVENT_UPDATED_GET_PRIVATE(obj)                          \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_EVENT_UPDATED, OctopusEventUpdatedPrivate))

/* Private data of the OctopusEventUpdated */
struct _OctopusEventUpdatedPrivate {
  /* Flag to specify if contents were updated */
  gboolean contents_updated;
  /* Flag to specify if attributes were updated */
  gboolean attributes_updated;
};

static void   octopus_event_updated_get_property (GObject      *object,
                                                  guint         prop_id,
                                                  GValue       *value,
                                                  GParamSpec   *pspec);

static gchar                   *octopus_event_updated_create_printable (const OctopusEvent *event);
static OctopusEventMergeResult  octopus_event_updated_merge            (OctopusEvent  *event,
                                                                        OctopusEvent  *next,
                                                                        OctopusEvent **merged);

G_DEFINE_TYPE(OctopusEventUpdated, octopus_event_updated, OCTOPUS_TYPE_EVENT)

enum {
  PROP_0,
  PROP_CONTENTS_UPDATED,
  PROP_ATTRIBUTES_UPDATED
};

static void
octopus_event_updated_class_init (OctopusEventUpdatedClass *klass)
{
  OctopusEventClass *parent_class;
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->get_property = octopus_event_updated_get_property;

  parent_class = OCTOPUS_EVENT_CLASS (klass);
  parent_class->create_printable = octopus_event_updated_create_printable;
  parent_class->merge = octopus_event_updated_merge;

  g_type_class_add_private (object_class, sizeof (OctopusEventUpdatedPrivate));

  g_object_class_install_property (object_class,
                                   PROP_CONTENTS_UPDATED,
                                   g_param_spec_boolean ("event-contents-updated",
                                                         "Event Contents Updated",
                                                         "Whether contents were updated in the event",
                                                         FALSE,
                                                         G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_ATTRIBUTES_UPDATED,
                                   g_param_spec_boolean ("event-attributes-updated",
                                                         "Event Attributes Updated",
                                                         "Whether attributes were updated in the event",
                                                         FALSE,
                                                         G_PARAM_READABLE));
}

static void
octopus_event_updated_init (OctopusEventUpdated *object)
{
  /* Setup private pointer */
  object->private = OCTOPUS_EVENT_UPDATED_GET_PRIVATE (object);
}

static void
octopus_event_updated_get_property (GObject      *object,
                                    guint         prop_id,
                                    GValue       *value,
                                    GParamSpec   *pspec)
{
  OctopusEventUpdated *event = OCTOPUS_EVENT_UPDATED (object);

  switch (prop_id)
    {
    case PROP_CONTENTS_UPDATED:
      g_value_set_boolean (value, event->private->contents_updated);
      break;
    case PROP_ATTRIBUTES_UPDATED:
      g_value_set_boolean (value, event->private->attributes_updated);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static gchar *
octopus_event_updated_create_printable (const OctopusEvent *event)
{
  OctopusEventUpdated *event_updated = OCTOPUS_EVENT_UPDATED (event);
  gchar *source_uri;
  gchar *printable;
  GFile *source_file;

  source_file = octopus_event_get_source_file (event);
  source_uri = g_file_get_uri (source_file);

  printable = g_strdup_printf ("[ UPDATED ] '%s' (contents: %s, attributes: %s)",
                               source_uri,
                               event_updated->private->contents_updated ? "yes" : "no",
                               event_updated->private->attributes_updated ? "yes" : "no");

  g_free (source_uri);
  g_object_unref (source_file);
  return printable;
}

static OctopusEventMergeResult
octopus_event_updated_merge (OctopusEvent  *event,
                             OctopusEvent  *next,
                             OctopusEvent **merged)
{
  OctopusEventType next_type;

  next_type = octopus_event_get_event_type (next);
  switch (next_type)
    {
    case OCTOPUS_EVENT_TYPE_UPDATED:
      {
        /* UPDATE(A)  + UPDATE(A)  = UPDATE(A) */
        *merged = g_object_ref (event);
        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_DELETED:
      {
        /* UPDATE(A)  + DELETE(A)  = DELETE(A) */
        *merged = g_object_ref (next);
        return OCTOPUS_EVENT_MERGE_RESULT_MERGED;
      }
    case OCTOPUS_EVENT_TYPE_MOVED:
    case OCTOPUS_EVENT_TYPE_ERROR:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE;
      }
    default:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED;
      }
    }
}

/**
 * octopus_event_updated_get_contents_updated:
 * @event: a #OctopusEventUpdated
 *
 * Checks whether the item update event refers to contents being updated.
 *
 * Returns: #TRUE if contents were notified to be updated, #FALSE otherwise.
 */
gboolean
octopus_event_updated_get_contents_updated (OctopusEventUpdated *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT_UPDATED (event), FALSE);

  return event->private->contents_updated;
}

/**
 * octopus_event_updated_get_attributes_updated:
 * @event: a #OctopusEventUpdated
 *
 * Checks whether the item update event refers to attributes being updated.
 *
 * Returns: #TRUE if attributes were notified to be updated, #FALSE otherwise.
 */
gboolean
octopus_event_updated_get_attributes_updated (OctopusEventUpdated *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT_UPDATED (event), FALSE);

  return event->private->attributes_updated;
}

OctopusEvent *
_octopus_event_updated_new (GFile    *source_file,
                            gboolean  contents_updated,
                            gboolean  attributes_updated)
{
  OctopusEventUpdated *event;

  event = g_object_new (OCTOPUS_TYPE_EVENT_UPDATED, NULL);
  _octopus_event_initialize (OCTOPUS_EVENT (event), source_file, OCTOPUS_EVENT_TYPE_UPDATED);
  event->private->contents_updated = contents_updated;
  event->private->attributes_updated = attributes_updated;
  return OCTOPUS_EVENT (event);
}
