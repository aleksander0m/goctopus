/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_FOUND_H__
#define __OCTOPUS_EVENT_FOUND_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "octopus-event.h"

/**
 * SECTION:octopus-event-found
 * @short_description: Octopus Events to notify items found during crawling
 * @title: OctopusEventFound
 * @stability: Unstable
 * @include: octopus.h
 *
 * Reporting found items as Octopus Events.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_EVENT_FOUND            (octopus_event_found_get_type ())
#define OCTOPUS_EVENT_FOUND(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT_FOUND, OctopusEventFound))
#define OCTOPUS_EVENT_FOUND_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT_FOUND, OctopusEventFoundClass))
#define OCTOPUS_IS_EVENT_FOUND(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT_FOUND))
#define OCTOPUS_IS_EVENT_FOUND_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT_FOUND))
#define OCTOPUS_EVENT_FOUND_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT_FOUND, OctopusEventFoundClass))

typedef struct _OctopusEventFound         OctopusEventFound;
typedef struct _OctopusEventFoundClass    OctopusEventFoundClass;
typedef struct _OctopusEventFoundPrivate  OctopusEventFoundPrivate;

/**
 * OctopusEventFound:
 *
 * A #OctopusEvent which represents a found item.
 */
struct _OctopusEventFound {
  /*< private >*/
  OctopusEvent              parent;
  OctopusEventFoundPrivate *private;
};

struct _OctopusEventFoundClass {
  OctopusEventClass parent;
};

GType         octopus_event_found_get_type      (void);

GFileInfo    *octopus_event_found_get_file_info (OctopusEventFound *event);

/* END PUBLIC */

OctopusEvent *_octopus_event_found_new          (GFile     *source_file,
                                                 GFileInfo *source_file_info);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_FOUND_H__ */
