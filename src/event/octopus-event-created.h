/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_CREATED_H__
#define __OCTOPUS_EVENT_CREATED_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "octopus-event.h"

G_BEGIN_DECLS

/* BEGIN PUBLIC */

/**
 * SECTION:octopus-event-created
 * @short_description: Octopus Events to notify newly created items
 * @title: OctopusEventCreated
 * @stability: Unstable
 * @include: octopus.h
 *
 * Reporting newly created items as Octopus Events.
 */

#define OCTOPUS_TYPE_EVENT_CREATED            (octopus_event_created_get_type ())
#define OCTOPUS_EVENT_CREATED(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT_CREATED, OctopusEventCreated))
#define OCTOPUS_EVENT_CREATED_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT_CREATED, OctopusEventCreatedClass))
#define OCTOPUS_IS_EVENT_CREATED(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT_CREATED))
#define OCTOPUS_IS_EVENT_CREATED_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT_CREATED))
#define OCTOPUS_EVENT_CREATED_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT_CREATED, OctopusEventCreatedClass))

typedef struct _OctopusEventCreated         OctopusEventCreated;
typedef struct _OctopusEventCreatedClass    OctopusEventCreatedClass;
typedef struct _OctopusEventCreatedPrivate  OctopusEventCreatedPrivate;

/**
 * OctopusEventCreated:
 *
 * The <structname>OctopusEventCreated</structname> represents an event in a
 * new created item.
 */
struct _OctopusEventCreated {
  /*< private >*/
  OctopusEvent                parent;
  OctopusEventCreatedPrivate *private;
};

struct _OctopusEventCreatedClass {
  OctopusEventClass parent;
};

GType         octopus_event_created_get_type (void);

/* END PUBLIC */

OctopusEvent *_octopus_event_created_new     (GFile *source_file);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_CREATED_H__ */
