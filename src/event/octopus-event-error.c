/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "octopus-event-error.h"

#define OCTOPUS_EVENT_ERROR_GET_PRIVATE(obj)                            \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_EVENT_ERROR, OctopusEventErrorPrivate))

/* Private data of the OctopusEventError */
struct _OctopusEventErrorPrivate {
  /* The error */
  GError *error;
};

static void   octopus_event_error_finalize         (GObject *object);

static gchar                   *octopus_event_error_create_printable (const OctopusEvent *event);
static OctopusEventMergeResult  octopus_event_error_merge            (OctopusEvent  *event,
                                                                      OctopusEvent  *next,
                                                                      OctopusEvent **merged);

G_DEFINE_TYPE(OctopusEventError, octopus_event_error, OCTOPUS_TYPE_EVENT)

static void
octopus_event_error_class_init (OctopusEventErrorClass *klass)
{
  OctopusEventClass *parent_class;
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = octopus_event_error_finalize;

  parent_class = OCTOPUS_EVENT_CLASS (klass);
  parent_class->create_printable = octopus_event_error_create_printable;
  parent_class->merge = octopus_event_error_merge;

  g_type_class_add_private (object_class, sizeof (OctopusEventErrorPrivate));
}

static void
octopus_event_error_init (OctopusEventError *object)
{
  /* Setup private pointer */
  object->private = OCTOPUS_EVENT_ERROR_GET_PRIVATE (object);

  object->private->error = NULL;
}

static void
octopus_event_error_finalize (GObject *object)
{
  OctopusEventError *event =  OCTOPUS_EVENT_ERROR (object);

  if (event->private->error)
    g_error_free (event->private->error);

  G_OBJECT_CLASS (octopus_event_error_parent_class)->finalize (object);
}

static gchar *
octopus_event_error_create_printable (const OctopusEvent *event)
{
  OctopusEventError *event_error = OCTOPUS_EVENT_ERROR (event);
  gchar *source_uri;
  gchar *printable;
  GFile *source_file;

  source_file = octopus_event_get_source_file (event);
  source_uri = g_file_get_uri (source_file);

  printable = g_strdup_printf ("[ ERROR ] '%s': %s",
                               source_uri,
                               event_error->private->error->message);

  g_free (source_uri);
  g_object_unref (source_file);
  return printable;
}

static OctopusEventMergeResult
octopus_event_error_merge (OctopusEvent  *event,
                           OctopusEvent  *next,
                           OctopusEvent **merged)
{
  OctopusEventType next_type;

  next_type = octopus_event_get_event_type (next);
  switch (next_type)
    {
    case OCTOPUS_EVENT_TYPE_FOUND:
    case OCTOPUS_EVENT_TYPE_CREATED:
    case OCTOPUS_EVENT_TYPE_UPDATED:
    case OCTOPUS_EVENT_TYPE_DELETED:
    case OCTOPUS_EVENT_TYPE_MOVED:
    case OCTOPUS_EVENT_TYPE_ERROR:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNMERGEABLE;
      }
    default:
      {
        return OCTOPUS_EVENT_MERGE_RESULT_UNEXPECTED;
      }
    }
}

OctopusEvent *
_octopus_event_error_new (GFile       *source_file,
                          GQuark       error_domain,
                          gint         error_code,
                          const gchar *format,
                          ...)
{
  va_list args;
  OctopusEventError *event;

  g_return_val_if_fail (G_IS_FILE (source_file), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  /* Initialize variable length list */
  va_start (args, format);

  event = g_object_new (OCTOPUS_TYPE_EVENT_ERROR, NULL);
  _octopus_event_initialize (OCTOPUS_EVENT (event), source_file, OCTOPUS_EVENT_TYPE_ERROR);

  /* Create error */
  event->private->error = g_error_new_valist (error_domain,
                                              error_code,
                                              format,
                                              args);

  va_end (args);

  return OCTOPUS_EVENT (event);
}

OctopusEvent *
_octopus_event_error_new_from_error (GFile        *source_file,
                                     const GError *error)
{
  OctopusEventError *event;

  g_return_val_if_fail (G_IS_FILE (source_file), NULL);
  g_return_val_if_fail (error != NULL, NULL);

  event = g_object_new (OCTOPUS_TYPE_EVENT_ERROR, NULL);
  _octopus_event_initialize (OCTOPUS_EVENT (event), source_file, OCTOPUS_EVENT_TYPE_ERROR);

  /* Create error */
  event->private->error = g_error_copy (error);

  return OCTOPUS_EVENT (event);
}

/**
 * octopus_event_error_propagate:
 * @event: a #OctopusEventError
 * @error: place where the #GError will be set
 *
 * Propagates the internally stored error into a new #GError
 */
void
octopus_event_error_propagate (OctopusEventError  *event,
                               GError            **error)
{
  g_return_if_fail (OCTOPUS_IS_EVENT_ERROR (event));

  if (error)
    {
      *error = g_error_copy (event->private->error);
    }
}

/**
 * octopus_event_error_get_message:
 * @event: a #OctopusEventError
 *
 * Returns the error message associated to this #OctopusEventError
 *
 * Returns: a constant string which should not be freed by the caller.
 */
const gchar *
octopus_event_error_get_message (OctopusEventError *event)
{
  g_return_val_if_fail (OCTOPUS_IS_EVENT_ERROR (event), NULL);

  return event->private->error->message;
}
