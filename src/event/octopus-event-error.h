/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_ERROR_H__
#define __OCTOPUS_EVENT_ERROR_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "octopus-event.h"

/**
 * SECTION:octopus-event-error
 * @short_description: Octopus Events for errors
 * @title: OctopusEventError
 * @stability: Unstable
 * @include: octopus.h
 *
 * Error reporting as Octopus Events
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_EVENT_ERROR            (octopus_event_error_get_type ())
#define OCTOPUS_EVENT_ERROR(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_EVENT_ERROR, OctopusEventError))
#define OCTOPUS_EVENT_ERROR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_EVENT_ERROR, OctopusEventErrorClass))
#define OCTOPUS_IS_EVENT_ERROR(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_EVENT_ERROR))
#define OCTOPUS_IS_EVENT_ERROR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_EVENT_ERROR))
#define OCTOPUS_EVENT_ERROR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_EVENT_ERROR, OctopusEventErrorClass))

typedef struct _OctopusEventError         OctopusEventError;
typedef struct _OctopusEventErrorClass    OctopusEventErrorClass;
typedef struct _OctopusEventErrorPrivate  OctopusEventErrorPrivate;

/**
 * OctopusEventError:
 *
 * A #OctopusEvent which represents an error
 */
struct _OctopusEventError {
  /*< private >*/
  OctopusEvent              parent;
  OctopusEventErrorPrivate *private;
};

struct _OctopusEventErrorClass {
  OctopusEventClass parent;
};

GType         octopus_event_error_get_type        (void);

const gchar  *octopus_event_error_get_message     (OctopusEventError *event);

void          octopus_event_error_propagate       (OctopusEventError  *event,
                                                   GError            **error);

/* END PUBLIC */

OctopusEvent *_octopus_event_error_new            (GFile       *source_file,
                                                   GQuark       error_domain,
                                                   gint         error_code,
                                                   const gchar *format,
                                                   ...);

OctopusEvent *_octopus_event_error_new_from_error (GFile        *source_file,
                                                   const GError *error);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_ERROR_H__ */
