/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <octopus-marshal.h>
#include <octopus-enum-types.h>
#include <octopus-common-utils.h>

#include <octopus-path.h>

#include "octopus-crawler.h"

#define OCTOPUS_CRAWLER_GET_PRIVATE(obj)                                \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_CRAWLER, OctopusCrawlerPrivate))

/* Private data of the OctopusCrawler */
struct _OctopusCrawlerPrivate {
  /* Already crawling? */
  gboolean crawling;
  /* The tree where all configured paths are stored */
  OctopusPath *tree;
};

enum {
  EVENT,
  LAST_SIGNAL
};

static void     octopus_crawler_dispose      (GObject        *object);

G_DEFINE_TYPE(OctopusCrawler, octopus_crawler, G_TYPE_OBJECT)

static guint signals[LAST_SIGNAL] = { 0 };

static void
octopus_crawler_class_init (OctopusCrawlerClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = octopus_crawler_dispose;

  g_type_class_add_private (object_class, sizeof (OctopusCrawlerPrivate));

  /**
   * OctopusCrawler::event:
   * @crawler: The object which emitted the event
   * @event: The #OctopusEvent
   *
   * Notifies #OctopusEventFound or #OctopusEventError events during crawling.
   */
  signals[EVENT] = g_signal_new ("event",
                                 G_TYPE_FROM_CLASS (klass),
                                 G_SIGNAL_RUN_LAST,
                                 0,
                                 NULL, NULL,
                                 _octopus_marshal_VOID__OBJECT,
                                 G_TYPE_NONE,
                                 1,
                                 OCTOPUS_TYPE_EVENT);
}

static void
octopus_crawler_init (OctopusCrawler *object)
{
  /* Setup private data pointer */
  object->private = OCTOPUS_CRAWLER_GET_PRIVATE (object);
  /* Create empty tree */
  object->private->tree = _octopus_path_tree_new ();
  object->private->crawling = FALSE;
}

static void
octopus_crawler_dispose (GObject *object)
{
  OctopusCrawler *crawler = OCTOPUS_CRAWLER (object);

  /* Destroy the tree */
  g_object_unref (crawler->private->tree);

  G_OBJECT_CLASS (octopus_crawler_parent_class)->dispose (object);
}

/**
 * octopus_crawler_new:
 *
 * Creates a new #OctopusCrawler.
 *
 * Returns: a new #OctopusCrawler, which should be disposed with
 * g_object_unref() when no longer used.
 */
OctopusCrawler *
octopus_crawler_new (void)
{
  return g_object_new (OCTOPUS_TYPE_CRAWLER, NULL);
}

static void
octopus_crawler_path_event_cb (OctopusPath  *path,
                               OctopusEvent *event,
                               gpointer      user_data)
{
  g_signal_emit (OCTOPUS_CRAWLER (user_data), signals[EVENT], 0, event);
}

/**
 * octopus_crawler_add_path:
 * @crawler: a #OctopusCrawler
 * @directory: a #GFile
 * @action: the #OctopusAction to be configured
 * @error: a #GError for error reporting
 *
 * Configures @directory in @crawler with the given @action.
 * The only allowed values for @action are: #OCTOPUS_ACTION_IGNORE,
 * #OCTOPUS_ACTION_CRAWL and #OCTOPUS_ACTION_CRAWL_RECURSIVELY.
 *
 * Returns: #TRUE if the successful, #FALSE otherwise.
 */
gboolean
octopus_crawler_add_path (OctopusCrawler  *crawler,
                          GFile           *directory,
                          OctopusAction    action,
                          GError         **error)
{
  OctopusPath *path;

  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);
  g_return_val_if_fail (action == OCTOPUS_ACTION_IGNORE ||
                        action == OCTOPUS_ACTION_CRAWL ||
                        action == OCTOPUS_ACTION_CRAWL_RECURSIVELY, FALSE);

  /* Cant add paths while crawling */
  if (crawler->private->crawling)
    {
      g_set_error (error,
                   g_quark_from_static_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_WRONG_STATUS,
                   "Cannot add new path in crawler if already started");
      return FALSE;
    }

  octopus_file_debug (directory, "Adding new path in crawler...");

  /* Create new path object */
  path = _octopus_path_new (directory,
                            action,
                            octopus_crawler_path_event_cb,
                            crawler);

  /* Add the item in the tree */
  if (!_octopus_path_tree_add (crawler->private->tree,
                               path,
                               NULL,
                               error))
    {
      /* Adding the path here should NEVER fail. This is not like the
       * monitor case, as we're not crawling the paths here. */
      g_warn_if_reached ();

      octopus_crawler_remove_path (crawler, directory, NULL);
      g_object_unref (path);
      return FALSE;
    }

  g_object_unref (path);
  return TRUE;
}

/**
 * octopus_crawler_remove_path:
 * @crawler: a #OctopusCrawler
 * @directory: a #GFile
 * @error: a #GError for error reporting
 *
 * Removes the path given by @directory from @crawler.
 *
 * Returns: #TRUE if the path was correctly removed. Otherwise, #FALSE is
 * returned and @error is set accordingly.
 */
gboolean
octopus_crawler_remove_path (OctopusCrawler  *crawler,
                             GFile           *directory,
                             GError         **error)
{
  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);

  /* Cant remove paths while crawling */
  if (crawler->private->crawling)
    {
      g_set_error (error,
                   g_quark_from_static_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_WRONG_STATUS,
                   "Cannot remove paths in crawler if already started");
      return FALSE;
    }

  octopus_file_debug (directory, "Removing path from crawler...");

  return _octopus_path_tree_remove (crawler->private->tree,
                                    directory,
                                    error);
}

/**
 * octopus_crawler_get_path_at:
 * @crawler: a #OctopusCrawler
 * @directory: a #GFile
 *
 * Checks whether the given @directory is configured in @crawler.
 *
 * Returns: a new reference to the #OctopusPath corresponding to the
 * @directory path, if found. #NULL otherwise.
 */
OctopusPath *
octopus_crawler_get_path_at (OctopusCrawler *crawler,
                             GFile          *directory)
{
  OctopusPath *self;

  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);

  if (_octopus_path_tree_find (crawler->private->tree,
                               directory,
                               NULL,
                               &self))
    {
      return g_object_ref (self);
    }
  return NULL;
}

/**
 * octopus_crawler_is_file_crawled:
 * @crawler: a #OctopusCrawler
 * @file: a #GFile
 *
 * Checks whether the given @file is currently configured to be crawled
 * by the @crawler.
 *
 * Returns: #TRUE if the @file is configured to be crawled, #FALSE otherwise.
 */
gboolean
octopus_crawler_is_file_crawled (OctopusCrawler *crawler,
                                 GFile          *file)
{
  OctopusPath *parent;
  GFile *parent_directory;

  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);

  /* Find inner most parent */
  _octopus_path_tree_find (crawler->private->tree,
                           file,
                           &parent,
                           NULL);

  parent_directory = (parent != crawler->private->tree ?
                      octopus_path_get_directory (parent) :
                      NULL);

  if (parent_directory)
    {
      /* If file/dir is inside a non-recursively crawled parent, and
       * the parent is not exactly the first parent, then NOPE */
      switch (octopus_path_get_action (parent))
        {
        case OCTOPUS_ACTION_CRAWL:
          return g_file_has_parent (file, parent_directory);
        case OCTOPUS_ACTION_CRAWL_RECURSIVELY:
          return TRUE;
        default:
          return FALSE;
        }
      g_object_unref (parent_directory);
    }
  return FALSE;
}

/**
 * octopus_crawler_get_paths:
 * @crawler: a #OctopusCrawler
 *
 * Returns an object which allows iterating over all the paths configured in
 * @crawler.
 *
 * Returns: a #OctopusPathIterator, which should be disposed with
 * octopus_path_iterator_destroy() when no longer used.
 */
OctopusPathIterator *
octopus_crawler_get_paths (OctopusCrawler  *crawler)
{
  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), NULL);

  return _octopus_path_iterator_new_from_tree (crawler->private->tree);
}

/**
 * octopus_crawler_crawl:
 * @crawler: a #OctopusCrawler
 * @cancellable: a #GCancellable, or #NULL
 * @error: a #GError for error reporting
 *
 * Starts the #OctopusCrawler, if not already started.
 *
 * Returns: #TRUE if all the configured paths are properly
 * crawled. If an error occurs, #FALSE is returned and the @error is set
 * accordingly.
 */
gboolean
octopus_crawler_crawl (OctopusCrawler  *crawler,
                       GCancellable    *cancellable,
                       GError         **error)
{
  gboolean ret;

  g_return_val_if_fail (OCTOPUS_IS_CRAWLER (crawler), FALSE);

  /* Start sync crawling... */
  crawler->private->crawling = TRUE;
  /* Crawling goes on when enabling the path */
  ret = _octopus_path_enable (crawler->private->tree,
                              TRUE,
                              cancellable,
                              error);
  /* After enabling it, we disable it so that we can call
   * crawl() again. This action is not cancellable. */
  _octopus_path_disable (crawler->private->tree, NULL, NULL);
  /* Finished crawling... */
  crawler->private->crawling = FALSE;

  return ret;
}
