/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_CRAWLER_H__
#define __OCTOPUS_CRAWLER_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

/* Octopus headers */
#include <octopus-common.h>
#include <octopus-path-iterator.h>
#include <octopus-event-iterator.h>

/**
 * SECTION:octopus-crawler
 * @short_description: The Octopus Crawler main interface
 * @title: OctopusCrawler
 * @stability: Unstable
 * @include: octopus.h
 *
 * The #OctopusCrawler is an object which allows crawling directories.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_CRAWLER            (octopus_crawler_get_type ())
#define OCTOPUS_CRAWLER(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_CRAWLER, OctopusCrawler))
#define OCTOPUS_CRAWLER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_CRAWLER, OctopusCrawlerClass))
#define OCTOPUS_IS_CRAWLER(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_CRAWLER))
#define OCTOPUS_IS_CRAWLER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_CRAWLER))
#define OCTOPUS_CRAWLER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_CRAWLER, OctopusCrawlerClass))

typedef struct _OctopusCrawler         OctopusCrawler;
typedef struct _OctopusCrawlerClass    OctopusCrawlerClass;
typedef struct _OctopusCrawlerPrivate  OctopusCrawlerPrivate;

/**
 * OctopusCrawler:
 *
 * The Octopus Crawler object
 */
struct _OctopusCrawler {
  /*< private >*/
  GObject                parent;
  OctopusCrawlerPrivate *private;
};

struct _OctopusCrawlerClass {
  GObjectClass parent;
};

GType           octopus_crawler_get_type (void);

/* Crawler creation */
OctopusCrawler *octopus_crawler_new         (void);

/* Start crawling */
gboolean        octopus_crawler_crawl       (OctopusCrawler  *crawler,
                                             GCancellable    *cancellable,
                                             GError         **error);

/* Adding/removing Paths */
gboolean        octopus_crawler_add_path    (OctopusCrawler  *crawler,
                                             GFile           *directory,
                                             OctopusAction    action,
                                             GError         **error);
gboolean        octopus_crawler_remove_path (OctopusCrawler  *crawler,
                                             GFile           *directory,
                                             GError         **error);

/* Path iteration and other utilities */
gboolean             octopus_crawler_is_file_crawled   (OctopusCrawler *crawler,
                                                        GFile          *file);
OctopusPath         *octopus_crawler_get_path_at       (OctopusCrawler *crawler,
                                                        GFile          *directory);
OctopusPathIterator *octopus_crawler_get_paths         (OctopusCrawler  *crawler);

/* END PUBLIC */

G_END_DECLS

#endif /* __OCTOPUS_CRAWLER_H__ */
