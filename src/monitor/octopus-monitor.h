/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_MONITOR_H__
#define __OCTOPUS_MONITOR_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

/* Octopus headers */
#include <octopus-common.h>
#include <octopus-path-iterator.h>
#include <octopus-event-iterator.h>

/**
 * SECTION:octopus-monitor
 * @short_description: The Octopus Monitor main interface
 * @title: OctopusMonitor
 * @stability: Unstable
 * @include: octopus.h
 *
 * The #OctopusMonitor is an object which allows crawling and monitoring
 * directories.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_MONITOR            (octopus_monitor_get_type ())
#define OCTOPUS_MONITOR(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_MONITOR, OctopusMonitor))
#define OCTOPUS_MONITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_MONITOR, OctopusMonitorClass))
#define OCTOPUS_IS_MONITOR(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_MONITOR))
#define OCTOPUS_IS_MONITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_MONITOR))
#define OCTOPUS_MONITOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_MONITOR, OctopusMonitorClass))

typedef struct _OctopusMonitor         OctopusMonitor;
typedef struct _OctopusMonitorClass    OctopusMonitorClass;
typedef struct _OctopusMonitorPrivate  OctopusMonitorPrivate;

/**
 * OctopusMonitor:
 *
 * The Octopus Monitor object
 */
struct _OctopusMonitor {
  /*< private >*/
  GObject                parent;
  OctopusMonitorPrivate *private;
};

struct _OctopusMonitorClass {
  GObjectClass parent;
};

GType           octopus_monitor_get_type (void);

/* Monitor creation */
OctopusMonitor *octopus_monitor_new           (gboolean enabled);
OctopusMonitor *octopus_monitor_new_real_time (gboolean enabled,
                                               guint    events_merge_s);
OctopusMonitor *octopus_monitor_new_on_demand (gboolean enabled,
                                               gboolean events_merge);
OctopusMonitor *octopus_monitor_new_full      (gboolean enabled,
                                               gboolean events_on_demand,
                                               gboolean events_on_demand_merge,
                                               gboolean events_real_time,
                                               guint    events_real_time_merge_s);

/* Monitor enabling/disabling */
gboolean        octopus_monitor_enable     (OctopusMonitor  *monitor,
                                            GCancellable    *cancellable,
                                            GError         **error);
gboolean        octopus_monitor_disable    (OctopusMonitor  *monitor,
                                            GCancellable    *cancellable,
                                            GError         **error);
gboolean        octopus_monitor_is_enabled (OctopusMonitor *monitor);

/* Events on-demand?*/
gboolean              octopus_monitor_enable_events_on_demand  (OctopusMonitor *monitor,
                                                                gboolean        events_merge);
gboolean              octopus_monitor_disable_events_on_demand (OctopusMonitor *monitor);

OctopusEventIterator *octopus_monitor_get_events               (OctopusMonitor *monitor);

/* Events real-time?*/
gboolean        octopus_monitor_enable_events_real_time  (OctopusMonitor *monitor,
                                                          guint           events_merge_s);
gboolean        octopus_monitor_disable_events_real_time (OctopusMonitor *monitor);

/* Status */
OctopusStatus   octopus_monitor_get_status (OctopusMonitor  *monitor);

/* Adding/removing Paths */
gboolean        octopus_monitor_add_path    (OctopusMonitor  *monitor,
                                             GFile           *directory,
                                             OctopusAction    action,
                                             GCancellable    *cancellable,
                                             GError         **error);
gboolean        octopus_monitor_remove_path (OctopusMonitor  *monitor,
                                             GFile           *directory,
                                             GError         **error);

/* Path iteration and other utilities */
gboolean             octopus_monitor_is_file_monitored (OctopusMonitor *monitor,
                                                        GFile          *file);

OctopusPath         *octopus_monitor_get_path_at       (OctopusMonitor *monitor,
                                                        GFile          *directory);

OctopusPathIterator *octopus_monitor_get_paths         (OctopusMonitor  *monitor);

/* END PUBLIC */

G_END_DECLS

#endif /* __OCTOPUS_MONITOR_H__ */
