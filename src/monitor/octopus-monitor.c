/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <octopus-marshal.h>
#include <octopus-enum-types.h>
#include <octopus-common-utils.h>

#include <octopus-path.h>

#include <octopus-event-moved.h>
#include <octopus-event-queue.h>

#include "octopus-monitor.h"

#define MAX_EVENTS_REAL_TIME_MERGE_S     86400
#define DEFAULT_EVENTS_REAL_TIME_MERGE_S 5

#define OCTOPUS_MONITOR_GET_PRIVATE(obj)                                \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_MONITOR, OctopusMonitorPrivate))

/* Private data of the OctopusMonitor */
struct _OctopusMonitorPrivate {
  /* Flag to set the monitor as enabled or disabled */
  gboolean enabled;

  /* ON-DEMAND Operation support */
  /* Flag to enable on-demand event iteration */
  gboolean events_on_demand;
  /* Flag to enable event merging in on-demand */
  gboolean events_on_demand_merge;
  /* The stored queue of events */
  OctopusEventQueue *events_on_demand_queue;

  /* REAL-TIME Operation support */
  /* Flag to enable real-time event notification */
  gboolean events_real_time;
  /* Max time to merge two events in real-time. If 0, disabled */
  guint events_real_time_merge_s;
  /* The stored queue of events */
  OctopusEventQueue *events_real_time_queue;
  /* ID of the scheduled timeout event */
  guint events_real_time_timeout_id;

  /* The tree where all configured paths are stored */
  OctopusPath *tree;
};

enum {
  PROP_0,
  PROP_ENABLED,
  PROP_EVENTS_ON_DEMAND,
  PROP_EVENTS_ON_DEMAND_MERGE,
  PROP_EVENTS_REAL_TIME,
  PROP_EVENTS_REAL_TIME_MERGE_S,
  PROP_STATUS
};

enum {
  EVENT,
  LAST_SIGNAL
};

static void     octopus_monitor_dispose      (GObject        *object);
static void     octopus_monitor_finalize     (GObject        *object);
static void     octopus_monitor_get_property (GObject        *object,
                                              guint           prop_id,
                                              GValue         *value,
                                              GParamSpec     *pspec);

static gboolean octopus_monitor_set_enabled  (OctopusMonitor  *monitor,
                                              gboolean         enabled,
                                              GCancellable    *cancellable,
                                              GError         **error);

static gboolean octopus_monitor_set_events_on_demand     (OctopusMonitor *monitor,
                                                          gboolean        enable,
                                                          gboolean        events_merge);
static gboolean octopus_monitor_set_events_real_time     (OctopusMonitor *monitor,
                                                          gboolean        enable,
                                                          guint           events_merge_s);
static gboolean events_real_time_merge_cb                (gpointer user_data);


G_DEFINE_TYPE(OctopusMonitor, octopus_monitor, G_TYPE_OBJECT)

static guint signals[LAST_SIGNAL] = { 0 };

static void
octopus_monitor_class_init (OctopusMonitorClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = octopus_monitor_dispose;
  object_class->finalize = octopus_monitor_finalize;

  object_class->get_property = octopus_monitor_get_property;

  g_type_class_add_private (object_class, sizeof (OctopusMonitorPrivate));

  g_object_class_install_property (object_class,
                                   PROP_ENABLED,
                                   g_param_spec_boolean ("enabled",
                                                         "Enabled",
                                                         "Desired status of the monitor",
                                                         FALSE,
                                                         G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_EVENTS_ON_DEMAND,
                                   g_param_spec_boolean ("events-on-demand",
                                                         "Events On-Demand",
                                                         "Specifies if on-demand event iteration is enabled",
                                                         FALSE,
                                                         G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_EVENTS_ON_DEMAND_MERGE,
                                   g_param_spec_boolean ("events-merge-on-demand",
                                                         "Events Merge On-Demand",
                                                         "Specifies if merging is enabled in on-demand event iteration",
                                                         FALSE,
                                                         G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_EVENTS_REAL_TIME,
                                   g_param_spec_boolean ("events-real-time",
                                                         "Events Real-Time",
                                                         "Specifies if real-time event notification is enabled",
                                                         FALSE,
                                                         G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_EVENTS_REAL_TIME_MERGE_S,
                                   g_param_spec_int ("events-merge-real-time-s",
                                                     "Events Merge timeout in Real-Time (s)",
                                                     "Time in seconds between events to be merged, in Real-Time",
                                                     0,
                                                     MAX_EVENTS_REAL_TIME_MERGE_S,
                                                     0,
                                                     G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_STATUS,
                                   g_param_spec_enum ("status",
                                                      "Status",
                                                      "Current status of the monitor",
                                                      octopus_status_get_type(),
                                                      OCTOPUS_STATUS_NONE,
                                                      G_PARAM_READABLE));

  /**
   * OctopusMonitor::event:
   * @monitor: The object which emitted the event
   * @event: The #OctopusEvent
   *
   * Notifies of new #OctopusEvent happening if real-time notifications are enabled.
   */
  signals[EVENT] = g_signal_new ("event",
                                 G_TYPE_FROM_CLASS (klass),
                                 G_SIGNAL_RUN_LAST,
                                 0,
                                 NULL, NULL,
                                 _octopus_marshal_VOID__OBJECT,
                                 G_TYPE_NONE,
                                 1,
                                 OCTOPUS_TYPE_EVENT);
}

static void
octopus_monitor_init (OctopusMonitor *object)
{
  /* Setup private data pointer */
  object->private = OCTOPUS_MONITOR_GET_PRIVATE (object);

  /* Initial status */
  object->private->enabled = FALSE;

  object->private->events_on_demand = FALSE;
  object->private->events_on_demand_merge = FALSE;
  object->private->events_on_demand_queue = NULL;

  object->private->events_real_time = FALSE;
  object->private->events_real_time_merge_s = 0;

  /* Create empty tree */
  object->private->tree = _octopus_path_tree_new ();
}

static void
octopus_monitor_dispose (GObject *object)
{
  OctopusMonitor *monitor = OCTOPUS_MONITOR (object);

  /* Destroy the tree */
  g_object_unref (monitor->private->tree);

  G_OBJECT_CLASS (octopus_monitor_parent_class)->dispose (object);
}

static void
octopus_monitor_finalize (GObject *object)
{
  OctopusMonitor *monitor = OCTOPUS_MONITOR (object);

  if (monitor->private->events_on_demand_queue)
    _octopus_event_queue_destroy (monitor->private->events_on_demand_queue);
  if (monitor->private->events_real_time_queue)
    _octopus_event_queue_destroy (monitor->private->events_real_time_queue);

  G_OBJECT_CLASS (octopus_monitor_parent_class)->finalize (object);
}

static void
octopus_monitor_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  OctopusMonitor *monitor = OCTOPUS_MONITOR (object);

  switch (prop_id)
    {
    case PROP_ENABLED:
      g_value_set_boolean (value, monitor->private->enabled);
      break;
    case PROP_EVENTS_ON_DEMAND:
      g_value_set_boolean (value, monitor->private->events_on_demand);
      break;
    case PROP_EVENTS_ON_DEMAND_MERGE:
      g_value_set_boolean (value, monitor->private->events_on_demand_merge);
      break;
    case PROP_EVENTS_REAL_TIME:
      g_value_set_boolean (value, monitor->private->events_real_time);
      break;
    case PROP_EVENTS_REAL_TIME_MERGE_S:
      g_value_set_uint (value, monitor->private->events_real_time_merge_s);
      break;
    case PROP_STATUS:
      g_value_set_enum (value, _octopus_path_tree_get_status (monitor->private->tree));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

/**
 * octopus_monitor_new:
 * @enabled: #TRUE if the monitor should start enabled, #FALSE otherwise.
 *
 * Creates a new #OctopusMonitor, not enabled neither as real-time nor as
 * on-demand.
 *
 * Returns: a new #OctopusMonitor, or #NULL if file monitoring is not supported.
 * Dispose it with g_object_unref() when no longer used.
 */
OctopusMonitor *
octopus_monitor_new (gboolean enabled)
{
  return octopus_monitor_new_full (enabled,  /* enabled */
                                   FALSE,    /* events_on_demand */
                                   FALSE,    /* events_on_demand_merge */
                                   FALSE,    /* events_real_time */
                                   0);       /* events_real_time_merge_s */
}

/**
 * octopus_monitor_new_real_time:
 * @enabled: #TRUE if the monitor should start enabled, #FALSE otherwise.
 * @events_merge_s: Maximum time, in seconds, to consider merging two
 * events on the same file. If 0, event merging is disabled. Maximum value is
 * 86400 (1 day).
 *
 * Creates a new #OctopusMonitor enabled to emit Real-Time notifications.
 *
 * Returns: a new #OctopusMonitor, or #NULL if file monitoring is not supported.
 * Dispose it with g_object_unref() when no longer used.
 */
OctopusMonitor *
octopus_monitor_new_real_time (gboolean enabled,
                               guint    events_merge_s)
{
  return octopus_monitor_new_full (enabled,  /* enabled */
                                   FALSE,    /* events_on_demand */
                                   FALSE,    /* events_on_demand_merge */
                                   TRUE,     /* events_real_time */
                                   events_merge_s); /* events_real_time_merge_s */
}

/**
 * octopus_monitor_new_on_demand:
 * @enabled: #TRUE if the monitor should start enabled, #FALSE otherwise.
 * @events_merge: #TRUE if events can be merged when using On-Demand
 * iterations, #FALSE otherwise.
 *
 * Creates a new #OctopusMonitor enabled to support On-Demand iterations.
 *
 * Returns: a new #OctopusMonitor, or #NULL if file monitoring is not supported.
 * Dispose it with g_object_unref() when no longer used.
 */
OctopusMonitor *
octopus_monitor_new_on_demand (gboolean enabled,
                               gboolean events_merge)
{
  return octopus_monitor_new_full (enabled,  /* enabled */
                                   TRUE,    /* events_on_demand */
                                   TRUE,    /* events_on_demand_merge */
                                   FALSE,     /* events_real_time */
                                   0); /* events_real_time_merge_s */
}

/**
 * octopus_monitor_new_full:
 * @enabled: #TRUE if the monitor should start enabled, #FALSE otherwise.
 * @events_on_demand: #TRUE if support for on-demand iteration of events should
 * be enabled, #FALSE otherwise.
 * @events_on_demand_merge: #TRUE if events can be merged when using On-Demand
 * iterations, #FALSE otherwise.
 * @events_real_time: #TRUE if support for real-time notification of events
 * should be enabled, #FALSE otherwise.
 * @events_real_time_merge_s: Maximum time, in seconds, to consider merging two
 * events on the same file when using Real-Time notifications.. If 0, event
 * merging is disabled. Maximum value is 86400 (1 day).
 *
 * Creates a new #OctopusMonitor.
 *
 * Returns: a new #OctopusMonitor, or #NULL if file monitoring is not supported.
 * Dispose it with g_object_unref() when no longer used.
 */
OctopusMonitor *
octopus_monitor_new_full (gboolean enabled,
                          gboolean events_on_demand,
                          gboolean events_on_demand_merge,
                          gboolean events_real_time,
                          guint    events_real_time_merge_s)
{
  GError *error = NULL;
  OctopusMonitor *monitor;

  /* If already initialized, this is actually kind of no-op */
  if (!_octopus_common_init (&error))
    {
      octopus_critical ("Cannot create an Octopus Monitor: %s",
                        error->message);
      g_error_free (error);
      return NULL;
    }

  monitor = g_object_new (OCTOPUS_TYPE_MONITOR, NULL);
  octopus_monitor_set_enabled (monitor, enabled, NULL, NULL);
  octopus_monitor_set_events_on_demand (monitor, events_on_demand, events_on_demand_merge);
  octopus_monitor_set_events_real_time (monitor, events_real_time, events_real_time_merge_s);
  return monitor;
}

static gboolean
octopus_monitor_set_enabled (OctopusMonitor  *monitor,
                             gboolean         enabled,
                             GCancellable    *cancellable,
                             GError         **error)
{
  octopus_debug ("Setting monitor as '%s'",
                 enabled ? "ENABLED" : "DISABLED");

  /* If already in the requested state, return */
  if (monitor->private->enabled == enabled)
    {
      return TRUE;
    }

  /* Initiate cascade enabling/disabling */
  if (enabled ?
      _octopus_path_enable (monitor->private->tree,
                            TRUE,
                            cancellable,
                            error) :
      _octopus_path_disable (monitor->private->tree,
                             cancellable,
                             error))
    {
      /* Set the new desired state */
      monitor->private->enabled = enabled;

      return TRUE;
    }
  return FALSE;
}

/**
 * octopus_monitor_enable:
 * @monitor: a #OctopusMonitor
 * @cancellable: a #GCancellable, or #NULL
 * @error: a #GError for error reporting
 *
 * Enables the #OctopusMonitor, if not already enabled. If the enabling operation
 * fails, the monitor will go into a #OCTOPUS_STATUS_INCONSISTENT state.
 * This action is synchronous, so it will block until finished. Note that if this
 * action fails, the monitor will be kept disabled until the inconsistent state
 * is fixed and it's enabled again.
 *
 * Returns: #TRUE if the monitor and all its configured paths are properly
 * enabled. If an error occurs, #FALSE is returned and the @error is set
 * accordingly.
 */
gboolean
octopus_monitor_enable (OctopusMonitor  *monitor,
                        GCancellable    *cancellable,
                        GError         **error)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_enabled (monitor, TRUE, cancellable, error);
}

/**
 * octopus_monitor_disable:
 * @monitor: a #OctopusMonitor
 * @cancellable: a #GCancellable, or #NULL
 * @error: a #GError for error reporting
 *
 * Disables the #OctopusMonitor, if not already disabled. If the disabling operation
 * fails, the monitor will go into a #OCTOPUS_STATUS_INCONSISTENT state.
 * This action is synchronous, so it will block until finished.
 *
 * Returns: #TRUE if the monitor is properly disabled. If an error occurs,
 * #FALSE is returned and the @error is set accordingly.
 */
gboolean
octopus_monitor_disable (OctopusMonitor  *monitor,
                         GCancellable    *cancellable,
                         GError         **error)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_enabled (monitor, FALSE, cancellable, error);
}

/**
 * octopus_monitor_is_enabled:
 * @monitor: a #OctopusMonitor
 *
 * Returns whether the @monitor is enabled or disabled
 *
 * Returns: #TRUE if the @monitor is enabled, #FALSE otherwise.
 */
gboolean
octopus_monitor_is_enabled (OctopusMonitor *monitor)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return monitor->private->enabled;
}

static gboolean
octopus_monitor_set_events_on_demand (OctopusMonitor *monitor,
                                      gboolean        enable,
                                      gboolean        events_merge)
{
  monitor->private->events_on_demand = enable;
  monitor->private->events_on_demand_merge = events_merge;

  if (enable)
    {
      /* Create list of events if not already done */
      if (G_LIKELY (!monitor->private->events_on_demand_queue))
        {
          monitor->private->events_on_demand_queue = _octopus_event_queue_new (events_merge);
        }
    }
  else
    {
      /* Destroy list of events */
      if (G_LIKELY (monitor->private->events_on_demand_queue))
        {
          _octopus_event_queue_destroy (monitor->private->events_on_demand_queue);
          monitor->private->events_on_demand_queue = NULL;
        }
    }

  return TRUE;
}

/**
 * octopus_monitor_enable_events_on_demand:
 * @monitor: a #OctopusMonitor
 * @events_merge: #TRUE if events can be merged when using On-Demand
 * iterations, #FALSE otherwise.
 *
 * Enables support for on-demand event iteration in @monitor.
 *
 * Returns: #TRUE if successful, #FALSE otherwise.
 */
gboolean
octopus_monitor_enable_events_on_demand (OctopusMonitor *monitor,
                                         gboolean        events_merge)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_events_on_demand (monitor, TRUE, events_merge);
}

/**
 * octopus_monitor_disable_events_on_demand:
 * @monitor: a #OctopusMonitor
 *
 * Disables support for on-demand event iteration in @monitor.
 *
 * Returns: #TRUE if successful, #FALSE otherwise.
 */
gboolean
octopus_monitor_disable_events_on_demand (OctopusMonitor *monitor)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_events_on_demand (monitor, FALSE, FALSE);
}


/**
 * octopus_monitor_get_events:
 * @monitor: a #OctopusMonitor
 *
 * Returns an object which allows iterating over all the events stored in
 * @monitor when on-demand-event iteration is enabled. After this call,
 * the @monitor will clean the internal list of events and start with an
 * empty one.
 *
 * Returns: a #OctopusEventIterator, which should be disposed with
 * octopus_event_iterator_destroy() when no longer used. #NULL is returned
 * if on-demand event iteration is not enabled.
 */
OctopusEventIterator *
octopus_monitor_get_events (OctopusMonitor *monitor)
{
  if (monitor->private->events_on_demand)
    {
      OctopusEventIterator *it;

      it = _octopus_event_iterator_new_from_queue (monitor->private->events_on_demand_queue);
      monitor->private->events_on_demand_queue =
        _octopus_event_queue_new (monitor->private->events_on_demand_merge);
      return it;
    }
  return NULL;
}

static void
notify_event_cb (OctopusEvent *event,
                 gpointer      user_data)
{
  g_signal_emit (OCTOPUS_MONITOR (user_data), signals[EVENT], 0, event);
}

static gboolean
events_real_time_merge_cb (gpointer user_data)
{
   OctopusMonitor *monitor = OCTOPUS_MONITOR (user_data);
   time_t now;
   time_t threshold;
   time_t next;

   /* Compute threshold (current time - configured value) */
   now = time (NULL);
   threshold = now - monitor->private->events_real_time_merge_s;

   /* We will notify all queued events now */
   next = _octopus_event_queue_notify_expired (monitor->private->events_real_time_queue,
                                               threshold,
                                               notify_event_cb,
                                               monitor);

   /* Reset event ID */
   monitor->private->events_real_time_timeout_id = 0;

   /* A new timeout needed? */
   if (next > 0)
     {
       /* +1 to make sure the event is expired */
       gint delay = now - next +1;

       octopus_debug ("Re-scheduling real time merge timeout in %d seconds", delay);

       if (delay > 0)
         {
           /* Reschedule the event notification */
           monitor->private->events_real_time_timeout_id =
             g_timeout_add_seconds (delay,
                                    events_real_time_merge_cb,
                                    monitor);
         }
       else
         g_warn_if_reached ();
     }

   /* Always return false here */
   return FALSE;
}

static gboolean
octopus_monitor_set_events_real_time (OctopusMonitor *monitor,
                                      gboolean        enable,
                                      guint           events_merge_s)
{
  monitor->private->events_real_time = enable;
  monitor->private->events_real_time_merge_s = events_merge_s;

  /* Create list of events if not already done */
  if (enable &&
      events_merge_s > 0 &&
      G_LIKELY (!monitor->private->events_real_time_queue))
    {
      monitor->private->events_real_time_queue = _octopus_event_queue_new (TRUE);
    }
  /* Was it already enabled, but we don't want to know event merging?; or
   * we want to disable it? */
  else if (events_merge_s == 0 ||
           !enable)
    {
      /* Remove timeout, if any */
      if (monitor->private->events_real_time_timeout_id)
        {
          g_source_remove (monitor->private->events_real_time_timeout_id);
          monitor->private->events_real_time_timeout_id = 0;
        }

      /* We will notify all queued events now, and then destroy the queue */
      if (monitor->private->events_real_time_queue)
        {
          _octopus_event_queue_notify_expired (monitor->private->events_real_time_queue,
                                               (time_t)G_MAXUINT64,
                                               notify_event_cb,
                                               monitor);
          _octopus_event_queue_destroy (monitor->private->events_real_time_queue);
          monitor->private->events_real_time_queue = NULL;
        }
    }

  return TRUE;
}

/**
 * octopus_monitor_enable_events_real_time:
 * @monitor: a #OctopusMonitor
 * @events_merge_s: Maximum time, in seconds, to consider merging two
 * events on the same file when using Real-Time notifications.. If 0, event
 * merging is disabled. Maximum value is 86400 (1 day).
 *
 * Enables real-time notification of events in @monitor.
 *
 * Returns: #TRUE if successful, #FALSE otherwise.
 */
gboolean
octopus_monitor_enable_events_real_time (OctopusMonitor *monitor,
                                         guint           events_merge_s)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_events_real_time (monitor, TRUE, events_merge_s);
}

/**
 * octopus_monitor_disable_events_real_time:
 * @monitor: a #OctopusMonitor
 *
 * Disables real-time notification of events in @monitor.
 *
 * Returns: #TRUE if successful, #FALSE otherwise.
 */
gboolean
octopus_monitor_disable_events_real_time (OctopusMonitor *monitor)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  return octopus_monitor_set_events_real_time (monitor, FALSE, FALSE);
}

/**
 * octopus_monitor_get_status:
 * @monitor: a #OctopusMonitor
 *
 * Returns the current status of @monitor
 *
 * Returns: a #OctopusStatus
 */
OctopusStatus
octopus_monitor_get_status (OctopusMonitor *monitor)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), OCTOPUS_STATUS_NONE);
  return _octopus_path_tree_get_status (monitor->private->tree);
}

static void
octopus_monitor_path_event_cb (OctopusPath  *path,
                               OctopusEvent *event,
                               gpointer      user_data)
{
  OctopusMonitor *monitor = OCTOPUS_MONITOR (user_data);

  /* Real-Time operation enabled? */
  if (monitor->private->events_real_time)
    {
      /* If merging not needed, emit it right away */
      if (!monitor->private->events_real_time_merge_s)
        g_signal_emit (monitor, signals[EVENT], 0, event);
      else
        {
          /* Add to queue */
          _octopus_event_queue_add (monitor->private->events_real_time_queue, event);

          /* Was this the first element added in the queue? */
          if (monitor->private->events_real_time_timeout_id == 0)
            {
              /* +1 to make sure the event is expired */
              octopus_debug ("Scheduling real time merge timeout in %d seconds",
                             monitor->private->events_real_time_merge_s + 1);

              /* Schedule event notification in the configured seconds */
              monitor->private->events_real_time_timeout_id =
                g_timeout_add_seconds (monitor->private->events_real_time_merge_s + 1,
                                       events_real_time_merge_cb,
                                       monitor);
            }
        }
    }

  /* On-Demand operation enabled? */
  if (monitor->private->events_on_demand)
    {
      /* Add to queue */
      _octopus_event_queue_add (monitor->private->events_on_demand_queue, event);
    }
}

/**
 * octopus_monitor_add_path:
 * @monitor: a #OctopusMonitor
 * @directory: a #GFile
 * @action: the #OctopusAction to be configured
 * @cancellable: a #GCancellable
 * @error: a #GError for error reporting
 *
 * Configures @directory in @monitor with the given @action.
 * If adding the path fails due to an internal error or if the
 * action is cancelled, the status of @monitor is recovered (never
 * goes into #OCTOPUS_STATUS_INCONSISTENT state).
 * This action is synchronous, so it will block until finished.
 * The only allowed values for @action are: #OCTOPUS_ACTION_IGNORE,
 * #OCTOPUS_ACTION_MONITOR and #OCTOPUS_ACTION_MONITOR_RECURSIVELY.
 *
 * Returns: #TRUE if the successful, #FALSE otherwise.
 */
gboolean
octopus_monitor_add_path (OctopusMonitor  *monitor,
                          GFile           *directory,
                          OctopusAction    action,
                          GCancellable    *cancellable,
                          GError         **error)
{
  OctopusPath *path;

  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);
  g_return_val_if_fail (action == OCTOPUS_ACTION_IGNORE ||
                        action == OCTOPUS_ACTION_MONITOR ||
                        action == OCTOPUS_ACTION_MONITOR_RECURSIVELY, FALSE);

  octopus_file_debug (directory, "Adding new path in monitor...");

  /* Create new path object */
  path = _octopus_path_new (directory,
                            action,
                            octopus_monitor_path_event_cb,
                            monitor);

  /* Add the item in the tree */
  if (!_octopus_path_tree_add (monitor->private->tree,
                               path,
                               cancellable,
                               error))
    {
      /* Check if we got it cancelled */
      if (g_cancellable_is_cancelled (cancellable))
        {
          gchar *uri;

          octopus_debug ("Creating new path in the monitor was cancelled...");

          uri = g_file_get_uri (directory);
          g_set_error (error,
                       g_quark_from_static_string (OCTOPUS_ERROR),
                       OCTOPUS_ERROR_CANCELLED,
                       "Adding path '%s' was cancelled",
                       uri);
          g_free (uri);
        }

      /* If enabling the path failed, we need to cleanup and report the
       * error */
      octopus_monitor_remove_path (monitor, directory, NULL);
      g_object_unref (path);
      return FALSE;
    }

  g_object_unref (path);
  return TRUE;
}

/**
 * octopus_monitor_remove_path:
 * @monitor: a #OctopusMonitor
 * @directory: a #GFile
 * @error: a #GError for error reporting
 *
 * Removes the path given by @directory from @monitor.
 *
 * Returns: #TRUE if the path was correctly removed. Otherwise, #FALSE is
 * returned and @error is set accordingly.
 */
gboolean
octopus_monitor_remove_path (OctopusMonitor  *monitor,
                             GFile           *directory,
                             GError         **error)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);

  octopus_file_debug (directory, "Removing path from monitor...");

  return _octopus_path_tree_remove (monitor->private->tree,
                                    directory,
                                    error);
}

/**
 * octopus_monitor_get_path_at:
 * @monitor: a #OctopusMonitor
 * @directory: a #GFile
 *
 * Checks whether the given @directory is configured in @monitor.
 *
 * Returns: a new reference to the #OctopusPath corresponding to the
 * @directory path, if found. #NULL otherwise.
 */
OctopusPath *
octopus_monitor_get_path_at (OctopusMonitor *monitor,
                             GFile          *directory)
{
  OctopusPath *self;

  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);

  if (_octopus_path_tree_find (monitor->private->tree,
                               directory,
                               NULL,
                               &self))
    {
      return g_object_ref (self);
    }
  return NULL;
}

/**
 * octopus_monitor_is_file_monitored:
 * @monitor: a #OctopusMonitor
 * @file: a #GFile
 *
 * Checks whether the given @file is currently configured to be monitored
 * by the @monitor. Note that this doesn't depend on the monitor being
 * enabled or disabled
 *
 * Returns: #TRUE if the @file is configured to be monitored, #FALSE otherwise.
 */
gboolean
octopus_monitor_is_file_monitored (OctopusMonitor *monitor,
                                   GFile          *file)
{
  OctopusPath *parent;
  GFile *parent_directory;

  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);

  /* Find inner most parent */
  _octopus_path_tree_find (monitor->private->tree,
                           file,
                           &parent,
                           NULL);

  parent_directory = (parent != monitor->private->tree ?
                      octopus_path_get_directory (parent) :
                      NULL);

  if (parent_directory)
    {
      /* If file/dir is inside a non-recursively monitored parent, and
       * the parent is not exactly the first parent, then NOPE */
      switch (octopus_path_get_action (parent))
        {
        case OCTOPUS_ACTION_MONITOR:
          return g_file_has_parent (file, parent_directory);
        case OCTOPUS_ACTION_MONITOR_RECURSIVELY:
          return TRUE;
        default:
          return FALSE;
        }
      g_object_unref (parent_directory);
    }
  return FALSE;
}

/**
 * octopus_monitor_get_paths:
 * @monitor: a #OctopusMonitor
 *
 * Returns an object which allows iterating over all the paths configured in
 * @monitor.
 *
 * Returns: a #OctopusPathIterator, which should be disposed with
 * octopus_path_iterator_destroy() when no longer used.
 */
OctopusPathIterator *
octopus_monitor_get_paths (OctopusMonitor  *monitor)
{
  g_return_val_if_fail (OCTOPUS_IS_MONITOR (monitor), NULL);

  return _octopus_path_iterator_new_from_tree (monitor->private->tree);
}

