/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <octopus-common.h>
#include <octopus-common-utils.h>
#include "octopus-event-queue.h"

static void
queue_destroy_foreach (gpointer data,
                       gpointer user_data)
{
  g_object_unref (data);
  ((OctopusEventQueue *)user_data)->n_events--;
}

static gboolean
ht_destroy_foreach (gpointer key,
                    gpointer value,
                    gpointer user_data)
{
  GQueue *queue = value;
  if (queue)
    {
      /* Call unref() on each queue item */
      g_queue_foreach (queue, queue_destroy_foreach, user_data);
      g_queue_free (queue);
    }
  return TRUE;
}

void
_octopus_event_queue_item_destroy (OctopusEventQueueItem *item,
                                   OctopusEventQueue     *event_queue)
{
  if (item->next_events)
    {
      g_hash_table_foreach_remove (item->next_events,
                                   ht_destroy_foreach,
                                   event_queue);
      g_hash_table_destroy (item->next_events);
    }

  if (item->originating_event)
    {
      g_object_unref (item->originating_event);
      event_queue->n_events--;
    }

  g_slice_free (OctopusEventQueueItem, item);
}

static OctopusEventQueueItem *
_octopus_event_queue_item_new (OctopusEvent *event)
{
  /* A new queue item is needed */
  OctopusEventQueueItem *item;

  item = g_slice_new0 (OctopusEventQueueItem);
  item->originating_event = event ? g_object_ref (event) : NULL;
  item->next_events = NULL;
  return item;
}

static void
event_queue_item_destroy_foreach (gpointer data,
                                  gpointer user_data)
{
  if (data)
    _octopus_event_queue_item_destroy (data, user_data);
}

void
_octopus_event_queue_destroy (OctopusEventQueue *event_queue)
{
  if (event_queue)
    {
      /* Call destroy() on each queue item */
      g_queue_foreach (event_queue->queue, event_queue_item_destroy_foreach, event_queue);
      g_queue_free (event_queue->queue);
      g_slice_free (OctopusEventQueue, event_queue);
    }
}

OctopusEventQueue *
_octopus_event_queue_new (gboolean merge_events)
{
  OctopusEventQueue *event_queue;

  event_queue = g_slice_new0 (OctopusEventQueue);
  event_queue->merge_events = merge_events;
  event_queue->queue = g_queue_new ();
  _octopus_event_queue_add (event_queue, NULL);
  return event_queue;
}

void
_octopus_event_queue_enable_events_merge (OctopusEventQueue *event_queue)
{
  event_queue->merge_events = TRUE;
}

void
_octopus_event_queue_disable_events_merge (OctopusEventQueue *event_queue)
{
  event_queue->merge_events = FALSE;
}

void
_octopus_event_queue_add (OctopusEventQueue *event_queue,
                          OctopusEvent      *event)
{
  GFile *event_file;
  OctopusEventQueueItem *last;
  GQueue *previous_events;
  OctopusEvent *last_previous_event;

  /* If no event, we need a new queue item */
  if (!event)
    {
      OctopusEventQueueItem *new_item;

      new_item = _octopus_event_queue_item_new (NULL);
      g_queue_push_tail (event_queue->queue, new_item);
      event_queue->n_events++;
      return;
    }

  /* Peek last element in the queue */
  last = g_queue_peek_tail (event_queue->queue);
  /* If no last element, we need a new queue item */
  if (!last)
    {
      OctopusEventQueueItem *new_item;

      new_item = _octopus_event_queue_item_new (event);
      g_queue_push_tail (event_queue->queue, new_item);
      event_queue->n_events++;
      return;
    }

  /* Get the file of the new event */
  event_file = octopus_event_get_source_file (event);

  /* Create HT of events if not already done */
  if (G_UNLIKELY (!last->next_events))
    {
      last->next_events = g_hash_table_new_full (g_file_hash,
                                                 (GEqualFunc) g_file_equal,
                                                 (GDestroyNotify) g_object_unref,
                                                 NULL);
      previous_events = NULL;
    }
  else
    {
      /* Try to find exactly same element in the HT of last element */
      previous_events = g_hash_table_lookup (last->next_events, event_file);
    }

  /* If no previous events, create new queue for them */
  if (!previous_events)
    {
      previous_events = g_queue_new ();
      /* If no previous event, add it */
      g_hash_table_insert (last->next_events,
                           g_object_ref (event_file),
                           previous_events);
      last_previous_event = NULL;
    }
  else
    {
      /* Pop last event */
      last_previous_event = g_queue_pop_tail (previous_events);
    }

  /* If no previous last event, just push the new one */
  if (!last_previous_event)
    {
      g_queue_push_tail (previous_events, g_object_ref (event));
      event_queue->n_events++;
    }
  else
    {
      OctopusEvent *merged = NULL;

      /* A previous event already exists, try to merge it if configured to do so */
      if (event_queue->merge_events &&
          _octopus_event_merge (last_previous_event,
                                event,
                                &merged) == OCTOPUS_EVENT_MERGE_RESULT_MERGED)
        {
          /* Events were merged, and result, if any, stored in 'merged' */
          g_object_unref (last_previous_event);
          /* Note: don't unref 'event'! */

          if (merged)
            {
              /* If merged event is a MOVED one, create a new OctopusEventQueueItem. */
              if (octopus_event_get_event_type (merged) == OCTOPUS_EVENT_TYPE_MOVED)
                {
                  OctopusEventQueueItem *new_item;

                  /* 'merged' is always a new reference */
                  new_item = _octopus_event_queue_item_new (merged);
                  g_queue_push_tail (event_queue->queue, new_item);
                }
              else
                {
                  g_queue_push_tail (previous_events, merged);
                }

              /* When merging an already existing event and the new one in a single
               * event, the total amount of events doesn't change */
            }
          else
            {
              /* else, the already existing event and the new one got merged and they
               * cancelled each other, which means that the total number of events is
               * reduced by one */
              event_queue->n_events--;
            }
        }
      else
        {
          /* Unmergeable, Unexpected or not to be merged, so push both separately */
          g_queue_push_tail (previous_events, last_previous_event);

          /* If new event is a MOVED one, create a new OctopusEventQueueItem. */
          if (octopus_event_get_event_type (event) == OCTOPUS_EVENT_TYPE_MOVED)
            {
              OctopusEventQueueItem *new_item;

              /* this will store a new reference to 'event' in 'new_item' */
              new_item = _octopus_event_queue_item_new (event);
              g_queue_push_tail (event_queue->queue, new_item);
            }
          else
            {
              /* We need to add a new reference to 'event' here to keep
               * them balanced */
              g_queue_push_tail (previous_events, g_object_ref (event));
            }

          /* Only 1 event more as the previous one was already counted */
          event_queue->n_events++;
        }
    }
  g_object_unref (event_file);
}

typedef struct {
  OctopusEventQueue *event_queue;
  time_t threshold;
  time_t next_event_timeout;
  NotifyExpiredCallback callback;
  gpointer user_data;
} ForeachNotifyExpired;

static gboolean
ht_foreach_notify_expired (gpointer key,
                           gpointer value,
                           gpointer user_data)
{
  GQueue *queue = value;
  ForeachNotifyExpired *context = user_data;
  OctopusEvent *event;

  /* Go popping events from queue */
  while ((event = g_queue_pop_head (queue)) != NULL)
    {
      GTimeVal epoch;

      octopus_event_get_epoch (event, &epoch);
      /* Expired? */
      if (epoch.tv_sec <= context->threshold)
        {
          /* Call callback for this expired event, then remove it */
          context->callback (event, context->user_data);
          g_object_unref (event);
          context->event_queue->n_events--;
        }
      else
        {
          octopus_debug ("Event not expired... (event time: %lu, threshold: %lu",
                         epoch.tv_sec, context->threshold);
          /* Put back the event in the queue and return */
          g_queue_push_head (queue, event);
          /* Update next event time if sooner than the already stored one */
          if (context->next_event_timeout == 0 ||
              epoch.tv_sec < context->next_event_timeout)
            context->next_event_timeout = epoch.tv_sec;

          /* Don't remove the queue! */
          return FALSE;
        }
    }

  /* Iterated all elements in the queue, and all notified, so
   * return TRUE so that the HT element is removed */
  g_queue_free (queue);
  return TRUE;
}

time_t
_octopus_event_queue_notify_expired (OctopusEventQueue     *event_queue,
                                     time_t                 threshold,
                                     NotifyExpiredCallback  callback,
                                     gpointer               user_data)
{
  ForeachNotifyExpired context;
  guint i;

  /* If no events, nothing to do */
  if (event_queue->n_events == 0)
    return 0;

  /* Set context */
  context.event_queue = event_queue;
  context.callback = callback;
  context.user_data = user_data;
  context.threshold = threshold;
  context.next_event_timeout = 0;

  /* Iterate over the elements of the queue */
  for (i = 0; i < event_queue->queue->length; i++)
    {
      OctopusEventQueueItem *current;

      octopus_debug ("Peeking queue item %d...", i);

      /* First, check the originating event. If this one is NOT
       * expired, stop iterating, as we know that the next
       * elements will be newer than the originating one */
      current = g_queue_peek_nth (event_queue->queue, i);
      if (current->originating_event)
        {
          GTimeVal epoch;

          octopus_event_get_epoch (current->originating_event, &epoch);
          /* Expired? */
          if (epoch.tv_sec <= threshold)
            {
              octopus_debug ("Originating event expired, notifying");
              /* Call callback for this expired event, then remove it */
              callback (current->originating_event, user_data);
              g_object_unref (current->originating_event);
              current->originating_event = NULL;
              event_queue->n_events--;
            }
          else /* Originating event not expired, stop. */
            {
              octopus_debug ("Originating event not expired (%lu, now: %lu, threshold: %lu)",
                             epoch.tv_sec, time (NULL), threshold);
              return epoch.tv_sec;
            }
        }

      /* Now, iterate the events in the HT of this queue item */
      g_hash_table_foreach_remove (current->next_events,
                                   ht_foreach_notify_expired,
                                   &context);
    }

  octopus_debug ("Remaining events in the queue: %lu, next event timeout: %lu",
                 event_queue->n_events,
                 context.next_event_timeout);

  return context.next_event_timeout;
}
