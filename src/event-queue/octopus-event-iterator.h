/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_ITERATOR_H__
#define __OCTOPUS_EVENT_ITERATOR_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

/* Octopus headers */
#include <octopus-common.h>
#include <octopus-event.h>
#include "octopus-event-queue.h"

/**
 * SECTION:octopus-event-iterator
 * @short_description: Iteration of On-Demand events
 * @title: OctopusEventIterator
 * @stability: Unstable
 * @include: octopus.h
 *
 * Support for iteration the list of on-demand events retrieved from a
 * #OctopusMonitor.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

/**
 * OctopusEventIterator:
 *
 * The Octopus Event Iterator object allows iterating over all #OctopusEvent
 * objects stored in a given #OctopusMonitor if On-Demand operation is enabled.
 */
typedef struct _OctopusEventIterator OctopusEventIterator;

const OctopusEvent *octopus_event_iterator_next    (OctopusEventIterator *iterator);
void                octopus_event_iterator_rewind  (OctopusEventIterator *iterator);
void                octopus_event_iterator_destroy (OctopusEventIterator *iterator);

const OctopusEvent *octopus_event_iterator_find    (OctopusEventIterator *iterator,
                                                    GFile                *event_source_file,
                                                    GFile                *event_destination_file,
                                                    OctopusEventType      event_type);

/* END PUBLIC */

OctopusEventIterator *_octopus_event_iterator_new_from_queue (OctopusEventQueue *event_queue);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_ITERATOR_H__ */
