/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_EVENT_QUEUE_H__
#define __OCTOPUS_EVENT_QUEUE_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <octopus-event.h>

G_BEGIN_DECLS

/* The Event Queue is composed of EventQueueItems:
 *  - The first element in the queue has always a NULL originating_event.
 *  - The next elements in the queue will store in originating_event the specific
 *     OctopusEvent which forced the creation of a new EventQueueItem. Usually,
 *     this is needed after every MOVE event, which are the ones which may
 *     change the directory structure.
 *  - The next_events hash table contain entries for each file that had an event
 *     after the originating_event of the same EventQueueItem and before the
 *     originating_event of the next EventQueueItem. The order of the events in
 *     the hash table is not important.
 */
typedef struct {
  /* The event which originated the new queue item */
  OctopusEvent *originating_event;
  /* The HT of events after the originating one, which don't
   * affect the structure of the directory trees. The data of each HT item is
   * a Queue of OctopusEvents in a given file. */
  GHashTable   *next_events;
} OctopusEventQueueItem;

typedef struct {
  /* The queue of EventQueueItems */
  GQueue   *queue;
  /* Should merge events in the queue? */
  gboolean  merge_events;
  /* Total number of events in the queue */
  gulong    n_events;
} OctopusEventQueue;

#define _octopus_event_queue_get_n_events(queue) queue->n_events

OctopusEventQueue *_octopus_event_queue_new                 (gboolean merge_events);

void              _octopus_event_queue_destroy              (OctopusEventQueue *event_queue);

void              _octopus_event_queue_enable_events_merge  (OctopusEventQueue *event_queue);

void              _octopus_event_queue_disable_events_merge (OctopusEventQueue *event_queue);

void              _octopus_event_queue_add                  (OctopusEventQueue *event_queue,
                                                             OctopusEvent      *event);

void              _octopus_event_queue_item_destroy         (OctopusEventQueueItem *item,
                                                             OctopusEventQueue     *event_queue);


typedef void      (* NotifyExpiredCallback)                 (OctopusEvent *event,
                                                             gpointer      user_data);

/* Iterates the queue of events, and calls callback for each event which
 * is expired (event time < threshold). After that, those expired events get
 * removed from the queue. Returns the time of the NEXT event expired.  */
time_t            _octopus_event_queue_notify_expired       (OctopusEventQueue     *event_queue,
                                                             time_t                 threshold,
                                                             NotifyExpiredCallback  callback,
                                                             gpointer               user_data);

G_END_DECLS

#endif /* __OCTOPUS_EVENT_QUEUE_H__ */
