/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/* Octopus headers */
#include <octopus-event-iterator.h>
#include <octopus-event-moved.h>
#include <octopus-common-utils.h>

struct _OctopusEventIterator {
  /* The whole event queue */
  OctopusEventQueue *event_queue;
  /* The current event queue item being iterated */
  OctopusEventQueueItem *current_octopus_event_queue_item;
  /* The current list of events being iterated */
  GQueue *current_file_events;

  /* As we walk the events, we keep them in this
   * list, so that we can rewind() */
  GList *already_iterated_events;
  /* If we did a rewind, this will point to the current item
   * being iterated from the already_iterated_events */
  GList *current_already_iterated_event;
};

OctopusEventIterator *
_octopus_event_iterator_new_from_queue (OctopusEventQueue *event_queue)
{
  OctopusEventIterator *iterator;

  iterator = g_slice_new0 (OctopusEventIterator);
  iterator->event_queue = event_queue;
  iterator->already_iterated_events = NULL;

  /* Set current pointing to the first element in the list */
  octopus_event_iterator_rewind (iterator);

  return iterator;
}

/**
 * octopus_event_iterator_next:
 * @iterator: a #OctopusEventIterator
 *
 * Iterates to the next available event, if any.
 *
 * Returns: a #OctopusEvent which should not be unref-ed by the caller; or #NULL
 * if no more events to iterate.
 */
const OctopusEvent *
octopus_event_iterator_next (OctopusEventIterator *iterator)
{
  OctopusEvent *next;

  g_return_val_if_fail (iterator, NULL);

  /* We may be iterating already iterated events, after a rewind */
  if (iterator->current_already_iterated_event)
    {
      /* Set next to be returned */
      next = iterator->current_already_iterated_event->data;

      /* And move iterator. Note we walk with previous as we started with last
       * element in list */
      iterator->current_already_iterated_event = g_list_previous (iterator->current_already_iterated_event);
      octopus_debug ("Returning next event (re-iterating): %p (%s)",
		     next,
		     octopus_event_get_printable (next));
      return next;
    }

  while (1)
    {
      /* If no more events to iterate, return */
      if (!iterator->event_queue)
        return NULL;

      /* If current queue item not set, try to set it, if any */
      if (!iterator->current_octopus_event_queue_item)
        {
          /* Get next item in the event queue */
          iterator->current_octopus_event_queue_item = g_queue_pop_head (iterator->event_queue->queue);
          if (!iterator->current_octopus_event_queue_item)
            {
              /* Oh, no more items? Destroy queue. */
              _octopus_event_queue_destroy (iterator->event_queue);
              iterator->event_queue = NULL;
              continue;
            }

          /* If we've got an originating event, this is the one to be
           * returned */
          if (iterator->current_octopus_event_queue_item->originating_event)
            {
              next = iterator->current_octopus_event_queue_item->originating_event;
              iterator->current_octopus_event_queue_item->originating_event = NULL;
              iterator->already_iterated_events = g_list_prepend (iterator->already_iterated_events, next);
              octopus_debug ("Returning next event (originating): %p (%s)",
                             next,
                             octopus_event_get_printable (next));
              return next;
            }
        }

      /* If current list of events is not set, try to get it from the current
       * queue item */
      if (!iterator->current_file_events)
        {
          GHashTableIter iter;
          gpointer value;

          /* We may not have a HT of events */
          if (!iterator->current_octopus_event_queue_item->next_events)
            return NULL;

          g_hash_table_iter_init (&iter, iterator->current_octopus_event_queue_item->next_events);
          if (!g_hash_table_iter_next (&iter, NULL, &value))
            {
              /* Oh, so no more lists in the HT... destroy queue item */
              _octopus_event_queue_item_destroy (iterator->current_octopus_event_queue_item,
                                                 iterator->event_queue);
              iterator->current_octopus_event_queue_item = NULL;

              /* Retry so that we advance the queue item */
              continue;
            }

          /* Got one per-file event queue, remove HT element */
          iterator->current_file_events = value;
          g_hash_table_iter_remove (&iter);
        }

      /* If we are already iterating the events of a given file... */
      next = g_queue_pop_head (iterator->current_file_events);
      if (next)
        {
          /* Notify the event from the list */
          iterator->already_iterated_events = g_list_prepend (iterator->already_iterated_events, next);
          octopus_debug ("Returning next event for file: %p (%s)",
                         next,
                         octopus_event_get_printable (next));
          return next;
        }

      /* Nothing else to iterate in this file */
      g_queue_free (iterator->current_file_events);
      iterator->current_file_events = NULL;

      /* Retry so that we advance to the next file in the HT */
    }
}

/**
 * octopus_event_iterator_rewind:
 * @iterator: a #OctopusEventIterator
 *
 * Rewinds the iterator so that it points back to the first element in the list
 * to iterate.
 */
void
octopus_event_iterator_rewind (OctopusEventIterator *iterator)
{
  g_return_if_fail (iterator);

  iterator->current_already_iterated_event = g_list_last (iterator->already_iterated_events);
}

/**
 * octopus_event_iterator_destroy:
 * @iterator: a #OctopusEventIterator
 *
 * Disposes @iterator. Once disposed, the #OctopusEvent objects returned during
 * octopus_event_iterator_next() operations should no longer be used.
 */
void
octopus_event_iterator_destroy (OctopusEventIterator *iterator)
{
  g_return_if_fail (iterator);

  if (iterator->already_iterated_events)
    {
      GList *li;
      for (li = iterator->already_iterated_events;
           li;
           li = g_list_next (li))
        {
          g_object_unref (li->data);
        }
      g_list_free (iterator->already_iterated_events);
    }

  if (iterator->current_file_events)
    {
      OctopusEvent *event;

      while ((event = g_queue_pop_head (iterator->current_file_events)) != NULL)
        {
          g_object_unref (event);
        }
      g_queue_free (iterator->current_file_events);
    }

  if (iterator->current_octopus_event_queue_item)
    _octopus_event_queue_item_destroy (iterator->current_octopus_event_queue_item,
                                       iterator->event_queue);

  if (iterator->event_queue)
    _octopus_event_queue_destroy (iterator->event_queue);

  g_slice_free (OctopusEventIterator, iterator);
}

/**
 * octopus_event_iterator_find:
 * @iterator: a #OctopusEventIterator
 * @event_source_file: a #GFile
 * @event_destination_file: a #GFile or #NULL, used only to look for
 *  #OCTOPUS_EVENT_TYPE_MOVED events.
 * @event_type: a #OctopusEventType
 *
 * Checks whether there is an event of type @event_type on file
 * @event_source_file in the list of events managed by the iterator.
 * Note that this will actually iterate over the whole list looking for the
 * event, so it may be slow. Also, note that the iterator pointer will point
 * to the given found event, if any; or to the first event of the iteration if
 * not found (as in a rewind).
 *
 * Returns: #TRUE if an event of type @event_type on file @event_source_file is
 * found by the iterator, #FALSE otherwise.
 */
const OctopusEvent *
octopus_event_iterator_find (OctopusEventIterator *iterator,
                             GFile                *event_source_file,
                             GFile                *event_destination_file,
                             OctopusEventType      event_type)
{
  const OctopusEvent *event;
  gboolean found = FALSE;

  g_return_val_if_fail (iterator, NULL);
  g_return_val_if_fail (G_IS_FILE (event_source_file), NULL);
  g_return_val_if_fail (!event_destination_file || G_IS_FILE (event_destination_file), NULL);
  g_return_val_if_fail (event_type < OCTOPUS_EVENT_TYPE_LAST, NULL);

  /* Rewind the iterator */
  octopus_event_iterator_rewind (iterator);

  while (!found &&
         (event = octopus_event_iterator_next (iterator)) != NULL)
    {
      GFile *source_file;

      source_file = octopus_event_get_source_file (event);
      if (octopus_event_get_event_type (event) == event_type &&
          g_file_equal (source_file, event_source_file))
        {
          if (event_destination_file &&
              event_type == OCTOPUS_EVENT_TYPE_MOVED)
            {
              GFile *destination_file;

              destination_file = octopus_event_moved_get_destination_file (OCTOPUS_EVENT_MOVED (event));
              found = g_file_equal (event_destination_file, destination_file);
              g_object_unref (destination_file);
            }
          else
            found = TRUE;
        }

      g_object_unref (source_file);
    }

  if (!found)
    octopus_event_iterator_rewind (iterator);

  return event;
}
