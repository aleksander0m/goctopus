/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <config.h>

#include "octopus-path.h"

#include <octopus-common.h>
#include <octopus-common-utils.h>
#include <octopus-marshal.h>
#include <octopus-enum-types.h>

#include <octopus-event-error.h>
#include <octopus-event-found.h>
#include <octopus-event-created.h>
#include <octopus-event-updated.h>
#include <octopus-event-moved.h>
#include <octopus-event-deleted.h>

#define OCTOPUS_PATH_GET_PRIVATE(obj)                                   \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), OCTOPUS_TYPE_PATH, OctopusPathPrivate))

/* Private data of the OctopusPath */
struct _OctopusPathPrivate {
  /* Configured parent directory */
  GFile         *directory;
  gchar         *directory_uri;
  /* Configured monitor action */
  OctopusAction  action;

  /* Parent of this path */
  OctopusPath   *parent;
  /* Children of this path */
  GList         *children;

  /* HT of monitors */
  GHashTable    *monitors;
  /* Current status of the path */
  OctopusStatus  status;

  /* Notification to upper objects */
  OctopusPathNotificationFunc func;
  gpointer                    user_data;
};

#define OCTOPUS_PATH_IS_TREE_ROOT(node)             \
  (node->private->directory == NULL ? TRUE : FALSE)

enum {
  PROP_0,
  PROP_DIRECTORY,
  PROP_DIRECTORY_URI,
  PROP_STATUS,
  PROP_ACTION
};

static void     octopus_path_dispose          (GObject       *object);
static void     octopus_path_finalize         (GObject       *object);
static void     octopus_path_get_property     (GObject       *object,
                                               guint          prop_id,
                                               GValue        *value,
                                               GParamSpec    *pspec);
static void     octopus_path_disable_internal (OctopusPath   *path);
static gboolean octopus_path_crawl            (OctopusPath   *path,
                                               GFile         *directory,
                                               gboolean       recurse,
                                               gboolean       silent,
                                               gboolean       recrawl,
                                               GCancellable  *cancellable,
                                               GError       **error);
static gboolean octopus_path_setup_monitor    (OctopusPath   *path,
                                               GFile         *directory,
                                               GCancellable  *cancellable,
                                               GError       **error);

/* Fixing monitors */
static void         octopus_path_create_monitors_recursively (OctopusPath *path,
                                                              GFile       *file,
                                                              gboolean     silent);
static void         octopus_path_remove_monitors_recursively (OctopusPath *path,
                                                              GFile       *file);
static void         octopus_path_move_monitors_recursively   (OctopusPath *path_source,
                                                              OctopusPath *path_destination,
                                                              GFile       *file_source,
                                                              GFile       *file_destination);

/* Path tree management */
static gboolean     octopus_path_recrawl          (OctopusPath   *path,
                                                   GCancellable  *cancellable,
                                                   GError       **error);
static OctopusPath *octopus_path_get_tree_root    (OctopusPath  *path);
static OctopusPath *octopus_path_find_child       (OctopusPath  *path,
                                                   GFile        *directory);
static OctopusPath *octopus_path_tree_find_parent (OctopusPath  *subtree,
                                                   GFile        *directory);

G_DEFINE_TYPE(OctopusPath, octopus_path, G_TYPE_OBJECT)

static void
octopus_path_class_init (OctopusPathClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = octopus_path_finalize;
  object_class->dispose = octopus_path_dispose;

  object_class->get_property = octopus_path_get_property;

  g_type_class_add_private (object_class, sizeof (OctopusPathPrivate));

  /* Read only properties */
  g_object_class_install_property (object_class,
                                   PROP_DIRECTORY,
                                   g_param_spec_object ("path-directory",
                                                        "Path Directory",
                                                        "Directory in this path",
                                                        G_TYPE_FILE,
                                                        G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_DIRECTORY_URI,
                                   g_param_spec_string ("path-directory-uri",
                                                        "Path Directory URI",
                                                        "URI of the directory in this path",
                                                        NULL,
                                                        G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_STATUS,
                                   g_param_spec_enum ("path-status",
                                                      "Path Status",
                                                      "Status of this path",
                                                      octopus_status_get_type (),
                                                      OCTOPUS_STATUS_NONE,
                                                      G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_ACTION,
                                   g_param_spec_enum ("path-action",
                                                      "Path Action",
                                                      "Action to consider in this path",
                                                      octopus_action_get_type (),
                                                      OCTOPUS_ACTION_NONE,
                                                      G_PARAM_READABLE));
}

static void
octopus_path_init (OctopusPath *object)
{
  object->private = OCTOPUS_PATH_GET_PRIVATE (object);
}

static void
octopus_path_finalize (GObject *object)
{
  OctopusPath *path = OCTOPUS_PATH (object);

  /* HT of monitors is destroyed while disabling the path */
  octopus_path_disable_internal (path);

  g_free (path->private->directory_uri);

  G_OBJECT_CLASS (octopus_path_parent_class)->finalize (object);
}

static void
octopus_path_dispose (GObject *object)
{
  OctopusPath *path = OCTOPUS_PATH (object);
  GList *li;

  if (path->private->directory)
    {
      g_object_unref (path->private->directory);
      path->private->directory = NULL;
    }

  if (path->private->children)
    {
      for (li = path->private->children;
           li;
           li = g_list_next (li))
        {
          g_object_unref (G_OBJECT (li->data));
        }
      g_list_free (path->private->children);
      path->private->children = NULL;
    }

  G_OBJECT_CLASS (octopus_path_parent_class)->dispose (object);
}

static void
octopus_path_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  OctopusPath *path = OCTOPUS_PATH (object);

  switch (prop_id)
    {
    case PROP_DIRECTORY:
      g_value_set_object (value, path->private->directory);
      break;
    case PROP_DIRECTORY_URI:
      g_value_set_static_string (value, path->private->directory_uri);
      break;
    case PROP_ACTION:
      g_value_set_enum (value, path->private->action);
      break;
    case PROP_STATUS:
      g_value_set_enum (value, path->private->status);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static OctopusPath *
octopus_path_tree_find_parent (OctopusPath *subtree,
                               GFile       *directory)
{
  GList *li;

  for (li = subtree->private->children;
       li;
       li = g_list_next (li))
    {
      OctopusPath *li_path = OCTOPUS_PATH (li->data);

      if ((g_file_has_prefix (directory,
                              li_path->private->directory)) &&
          (!g_file_equal (directory,
                          li_path->private->directory)))
        {
          OctopusPath *found;

          /* Recurse in the subtree to find the parent */
          found = octopus_path_tree_find_parent (li_path, directory);

          return found ? found : li_path;
        }
    }

  /* If not found while traversing, return */
  return NULL;
}

static OctopusPath *
octopus_path_get_tree_root (OctopusPath *path)
{
  if (path->private->parent)
    return octopus_path_get_tree_root (path->private->parent);
  return path;
}


static OctopusPath *
octopus_path_find_child (OctopusPath *path,
                         GFile       *directory)
{
  GList *li;

  for (li = path->private->children;
       li;
       li = g_list_next (li))
    {
      OctopusPath *li_path = OCTOPUS_PATH (li->data);

      if (g_file_equal (directory, li_path->private->directory))
        {
          return li_path;
        }
    }

  /* Equal not found */
  return NULL;
}

gboolean
_octopus_path_tree_find (OctopusPath  *tree,
                         GFile        *directory,
                         OctopusPath **parent,
                         OctopusPath **self)
{
  OctopusPath *found_parent;
  OctopusPath *found_self;

  /* Try to find a suitable parent */
  found_parent = octopus_path_tree_find_parent (tree, directory);
  if (!found_parent)
    {
      /* Reset to root of the tree if no suitable parent */
      found_parent = tree;
    }

  /* Set output value of parent if requested */
  if (parent)
    {
      *parent = found_parent;
    }

  /* Is there already the same directory in the parent? */
  found_self = octopus_path_find_child (found_parent, directory);

  /* Set output value of self if requested */
  if (self)
    {
      *self = found_self;
    }

  return found_self ? TRUE : FALSE;
}

gboolean
_octopus_path_tree_add (OctopusPath  *tree,
                        OctopusPath  *self,
                        GCancellable *cancellable,
                        GError      **error)
{
  GList *li;
  GList *next;
  OctopusPath *parent;

  octopus_debug ("Adding path '%s' to tree..",
                 self->private->directory_uri);

  /* If cancelled do nothing */
  if (g_cancellable_is_cancelled (cancellable))
    return FALSE;

  /* Try to find an already existing item. Even if not found,
   * parent will be set to the best possible parent */
  if (_octopus_path_tree_find (tree,
                               self->private->directory,
                               &parent,
                               NULL))
    {
      octopus_debug ("Cannot add path '%s' to tree: Already there",
                     self->private->directory_uri);
      g_set_error (error,
                   g_quark_from_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_EXISTS,
                   "Path '%s' is already in the tree",
                   self->private->directory_uri);
      return FALSE;
    }

  /* If parent exists, but its status is unstable, don't add it */
  if (parent->private->status == OCTOPUS_STATUS_INCONSISTENT)
    {
      octopus_debug ("Cannot add path '%s' to tree: Parent is inconsistent",
                     self->private->directory_uri);
      g_set_error (error,
                   g_quark_from_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_WRONG_STATUS,
                   "Parent path '%s' is in inconsistent state, "
                   "so cannot add new path '%s'",
                   parent->private->directory_uri,
                   self->private->directory_uri);
      return FALSE;
    }

  /* Check if this new item is actually parent of any already
   * existing item, and if so, move to the new parent */
  li = parent->private->children;
  while (li)
    {
      OctopusPath *li_path = OCTOPUS_PATH (li->data);

      next = g_list_next (li);
      if (g_file_has_prefix (li_path->private->directory,
                             self->private->directory))
        {
          /* Remove from the list of children in the parent,
           * and make it become a self contained list */
          parent->private->children = g_list_remove_link (parent->private->children,
                                                          li);
          /* Append to the list of children in the new path.
           * Note that using remove/concat we avoid some unneeded free/alloc */
          self->private->children = g_list_concat (li, self->private->children);
        }
      li = next;
    }

  /* Finally, add the new element in the parent */
  parent->private->children = g_list_prepend (parent->private->children,
                                              g_object_ref (self));

  /* Status of the path is always inherited from the parent when adding */
  self->private->status = parent->private->status;

  /* If disabled (as in OctopusCrawler!!!), just return */
  if (self->private->status == OCTOPUS_STATUS_DISABLED)
    return TRUE;

  if (self->private->action == OCTOPUS_ACTION_CRAWL ||
      self->private->action == OCTOPUS_ACTION_CRAWL_RECURSIVELY)
    {
      g_warn_if_reached ();
    }

  /* We need to enable the path.
   * Several checks should be done at this point:
   *   1. if the new path is recursively monitored:
   *     1.1 if the parent was also recursively monitored, MOVE monitors
   *          from parent to new path.
   *     1.2 if the parent was monitored (not recursively), ignored, or
   *          no parent at all, standard enable crawling the dir.
   *   2. if the new path is monitored (not recursively) or ignored:
   *     2.1 if the parent was recursively monitored, REMOVE monitors
   *          from the parent, AND standard enable.
   *     2.2 if the parent was monitored (not recursively), ignored, or
   *          no parent at all, standard enable crawling the dir.
   */

  /* 1.2, 2.2: standard enable */
  if (OCTOPUS_PATH_IS_TREE_ROOT (parent) ||
      parent->private->action != OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      octopus_debug ("Enabling the path (1.2, 2.2): "
                     "parent is root? '%s'; parent action: '%s'",
                     OCTOPUS_PATH_IS_TREE_ROOT (parent) ? "yes" : "no",
                     octopus_action_get_name (parent->private->action));
      return _octopus_path_enable (self, TRUE, cancellable, error);
    }

  /* 1.1: move monitors  */
  if (self->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      octopus_debug ("Enabling the path (1.1): parent action: '%s'",
                     octopus_action_get_name (parent->private->action));
      if (_octopus_path_enable (self, FALSE, cancellable, error))
        {
          octopus_path_move_monitors_recursively (parent,
                                                  self,
                                                  self->private->directory,
                                                  self->private->directory);
          return TRUE;
        }

      return FALSE;
    }

  /* 2.1: remove monitors from parent, and then standard enable */
  octopus_debug ("Enabling the path (2.1)");
  octopus_path_remove_monitors_recursively (parent, self->private->directory);
  return _octopus_path_enable (self, TRUE, cancellable, error);
}

gboolean
_octopus_path_tree_remove (OctopusPath   *tree,
                           GFile         *directory,
                           GError       **error)
{
  OctopusPath *self;
  OctopusPath *parent;

  octopus_file_debug (directory, "Removing path from tree..");

  /* Try to find an already existing item. Even if not found,
   * parent will be set to the best possible parent */
  if (!_octopus_path_tree_find (tree,
                                directory,
                                &parent,
                                &self))
    {
      gchar *uri;

      uri = g_file_get_uri (directory);
      octopus_debug ("Path '%s' not found configured in the monitor...", uri);
      g_set_error (error,
                   g_quark_from_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_NOT_FOUND,
                   "Path '%s' not found configured in the Monitor",
                   uri);
      g_free (uri);
      return FALSE;
    }

  /* If the item to be removed has any children, then move them to the
   * parent item */
  if (self->private->children)
    {
      parent->private->children = g_list_concat (parent->private->children,
                                                 self->private->children);
      self->private->children = NULL;
    }

  /* Once all children has been moved, we can now safely unlink the given
   * item from the tree */
  parent->private->children = g_list_remove (parent->private->children, self);

  /* If just crawling, return, no need to fix monitors */
  if (self->private->action == OCTOPUS_ACTION_CRAWL ||
      self->private->action == OCTOPUS_ACTION_CRAWL_RECURSIVELY)
    {
      return TRUE;
    }

  /* We need to disable the path.
   * Several checks should be done at this point:
   *   1. if the removed path was recursively monitored:
   *     1.1 if the parent was also recursively monitored, MOVE monitors
   *          from path to parent.
   *     1.2 if the parent was monitored (not recursively), ignored, or
   *          no parent at all; just remove path.
   *   2. if the removed path is monitored (not recursively) or ignored:
   *     2.1 if the parent was recursively monitored, need to RE-CRAWL the
   *          parent.
   *     2.2 if the parent was monitored (not recursively), ignored, or
   *          no parent at all; just remove path;
   */

  /* 1.2, 2.2: just remove */
  if (OCTOPUS_PATH_IS_TREE_ROOT (parent) ||
      parent->private->action != OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      g_object_unref (self);
      return TRUE;
    }

  /* 1.1: move monitors, then remove  */
  if (self->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      octopus_path_move_monitors_recursively (self,
                                              parent,
                                              directory,
                                              directory);
      g_object_unref (self);
      return TRUE;
    }

  /* 2.1: remove, but re-crawl parent */
  octopus_path_recrawl (parent, NULL, NULL);
  g_object_unref (self);
  return TRUE;
}

gboolean
_octopus_path_tree_expect_status (OctopusPath   *path,
                                  OctopusStatus  status)
{
  if (path->private->status != status)
    return FALSE;
  else if (!path->private->children)
    return TRUE;
  else
    {
      GList *li;

      for (li = path->private->children; li; li = g_list_next (li))
        {
          if (!_octopus_path_tree_expect_status (OCTOPUS_PATH (li->data),
                                                 status))
            return FALSE;
        }
      return TRUE;
    }
}

OctopusStatus
_octopus_path_tree_get_status (OctopusPath *tree)
{
  /* Recurses the tree getting the status. If the status of the subitems is not
   * equal to the status of the root item, INCONSISTENT. */
  if (_octopus_path_tree_expect_status (tree, tree->private->status))
    return tree->private->status;
  else
    return OCTOPUS_STATUS_INCONSISTENT;
}

OctopusPath *
_octopus_path_tree_new (void)
{
  OctopusPath *path;

  /* Create the new path object */
  path = g_object_new (OCTOPUS_TYPE_PATH, NULL);
  path->private->action = OCTOPUS_ACTION_IGNORE;
  path->private->status = OCTOPUS_STATUS_DISABLED;

  return path;
}

OctopusPath *
_octopus_path_new (GFile                       *directory,
                   OctopusAction                action,
                   OctopusPathNotificationFunc  func,
                   gpointer                     user_data)
{
  OctopusPath *path;

  octopus_debug ("New OctopusPath created, now adding to tree");

  /* Create the new path object */
  path = g_object_new (OCTOPUS_TYPE_PATH, NULL);
  if (directory)
    {
      path->private->directory = g_object_ref (directory);
      path->private->directory_uri = g_file_get_uri (path->private->directory);
    }

  /* Set notification stuff */
  path->private->func = func;
  path->private->user_data = user_data;

  /* Set requested action */
  path->private->action = action;

  /* Note that a fully valid reference is being returned,
   * which should be unref-ed */
  return path;
}

static gboolean
octopus_path_is_directory (GFile *file)
{
  return (g_file_query_file_type (file,
                                  G_FILE_QUERY_INFO_NONE,
                                  NULL) == G_FILE_TYPE_DIRECTORY ?
          TRUE : FALSE);
}

typedef struct {
  OctopusPath *path_source;
  OctopusPath *path_destination;
  GFile       *file_source;
  GFile       *file_destination;

  GList       *file_relative_path_list;
} MoveMonitorsForeach;

static gboolean
octopus_move_monitors_foreach (gpointer key,
                               gpointer value,
                               gpointer user_data)
{
  MoveMonitorsForeach *data = user_data;

  /* Insert new monitors in path_destination. */
  gchar *relative;

  relative = g_file_get_relative_path (data->file_source, G_FILE (key));
  if (relative)
    {
      if (data->path_destination->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
        {
          data->file_relative_path_list = g_list_prepend (data->file_relative_path_list,
                                                          relative);
        }
      else
        {
          g_free (relative);
        }
      return TRUE;
    }
  else if (g_file_equal (data->file_source, G_FILE (key)))
    {
      /* May be moving a monitor with the SAME path from one
       * OctopusPath to another one */
      data->file_relative_path_list = g_list_prepend (data->file_relative_path_list,
                                                      g_strdup (""));
      return TRUE;
    }

  return FALSE;
}

static void
octopus_path_move_monitors_recursively (OctopusPath *path_source,
                                        OctopusPath *path_destination,
                                        GFile       *file_source,
                                        GFile       *file_destination)
{
  MoveMonitorsForeach data;
  GList *li;

  data.path_source = path_source;
  data.path_destination = path_destination;
  data.file_source = file_source;
  data.file_destination = file_destination;
  data.file_relative_path_list = NULL;

  if ((path_source->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY) &&
      (octopus_path_is_directory (file_destination)))
    {
      /* Remove old monitors from path_source. */
      g_hash_table_foreach_remove (path_source->private->monitors,
                                   octopus_move_monitors_foreach,
                                   &data);
    }

  for (li = data.file_relative_path_list;
       li;
       li = g_list_next (li))
    {
      GFile *new_file;
      GError *inner_error = NULL;

      new_file = g_file_resolve_relative_path (file_destination,
                                               (gchar *)(li->data));
      if (new_file)
        {
          octopus_path_setup_monitor (path_destination,
                                      new_file,
                                      NULL,
                                      &inner_error);
        }
      else
        {
          gchar *uri;

          uri = g_file_get_uri (file_destination);
          g_set_error (&inner_error,
                       g_quark_from_static_string (OCTOPUS_ERROR),
                       OCTOPUS_ERROR_UNKNOWN,
                       "Couldn't resolve relative path during move (%s + %s)",
                       uri,
                       (gchar *)(li->data));

          g_free (uri);
        }

      if (inner_error)
        {
          OctopusEvent *event;

          /* Notify error on child */
          event = _octopus_event_error_new_from_error (new_file, inner_error);
          path_destination->private->func (path_destination,
                                           event,
                                           path_destination->private->user_data);
          g_object_unref (event);
        }

      g_clear_error (&inner_error);
      g_free (li->data);
    }
}

static void
octopus_path_create_monitors_recursively (OctopusPath *path,
                                          GFile       *file,
                                          gboolean     silent)
{
  if ((path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY) &&
      (octopus_path_is_directory (file)))
    {
      GError *inner_error = NULL;

      octopus_path_crawl (path,
                          file,
                          TRUE,
                          silent,
                          FALSE,
                          NULL,
                          &inner_error);
      if (inner_error)
        {
          if (!silent)
            {
              OctopusEvent *event;

              /* Notify error on child */
              event = _octopus_event_error_new_from_error (file, inner_error);
              path->private->func (path,
                                   event,
                                   path->private->user_data);
              g_object_unref (event);
            }
          g_clear_error (&inner_error);
        }
    }
}

typedef struct {
  OctopusPath *path;
  GFile       *directory;
} RemoveMonitorsForeach;

static gboolean
octopus_remove_monitors_foreach (gpointer key,
                                 gpointer value,
                                 gpointer user_data)
{
  RemoveMonitorsForeach *data = user_data;
  GFile *directory = G_FILE (key);

  if (g_file_has_prefix (directory, data->directory) ||
      g_file_equal (directory, data->directory))
    {
      return TRUE;
    }

  return FALSE;
}

static void
octopus_path_remove_monitors_recursively (OctopusPath *path,
                                          GFile       *file)
{
  if (path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      RemoveMonitorsForeach data;

      data.path = path;
      data.directory = file;

      /* is_directory won't work with an already deleted directory :-) */
      g_hash_table_foreach_remove (path->private->monitors,
                                   octopus_remove_monitors_foreach,
                                   &data);
    }
}

void
_octopus_path_list_children (OctopusPath  *path,
                             GSList      **list)
{
  GList *li;

  for (li = path->private->children;
       li;
       li = g_list_next (li))
    {
      OctopusPath *li_data = OCTOPUS_PATH (li->data);

      /* Add the child */
      *list = g_slist_prepend (*list, g_object_ref (li_data));
      /* And then traverse it */
      _octopus_path_list_children (li_data, list);
    }
}

static void
octopus_path_fix_paths_on_event (OctopusPath  *path,
                                 OctopusEvent *event)
{
  if (octopus_event_get_event_type (event) == OCTOPUS_EVENT_TYPE_CREATED)
    {
      GFile *source_file;

      source_file = octopus_event_get_source_file (event);
      octopus_path_create_monitors_recursively (path,
                                                source_file,
                                                FALSE);
      g_object_unref (source_file);
      return;
    }

  if (octopus_event_get_event_type (event) == OCTOPUS_EVENT_TYPE_DELETED)
    {
      GFile *source_file;

      source_file = octopus_event_get_source_file (event);
      octopus_path_remove_monitors_recursively (path,
                                                source_file);
      g_object_unref (source_file);
      return;
    }

  if (octopus_event_get_event_type (event) == OCTOPUS_EVENT_TYPE_MOVED)
    {
      OctopusPath *parent;
      GFile *source_file;
      GFile *destination_file;

      source_file = octopus_event_get_source_file (event);
      destination_file = octopus_event_moved_get_destination_file (OCTOPUS_EVENT_MOVED (event));

      /* Find inner most parent of dest */
      _octopus_path_tree_find (octopus_path_get_tree_root (path),
                               destination_file,
                               &parent,
                               NULL);

      /* Note: Parent node cannot be NULL when MOVE events */
      octopus_path_move_monitors_recursively (path,
                                              parent,
                                              source_file,
                                              destination_file);

      g_object_unref (source_file);
      g_object_unref (destination_file);
    }
}

static void
octopus_path_event_cb (GFileMonitor      *file_monitor,
                       GFile             *file,
                       GFile             *other_file,
                       GFileMonitorEvent  event_type,
                       gpointer	          user_data)
{
  OctopusPath *path = OCTOPUS_PATH (user_data);
  OctopusEvent *event = NULL;

  switch (event_type)
    {
    case G_FILE_MONITOR_EVENT_CHANGED:
    case G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
      event = _octopus_event_updated_new (file, TRUE, FALSE);
      break;
    case G_FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED:
      event = _octopus_event_updated_new (file, FALSE, TRUE);
      break;
    case G_FILE_MONITOR_EVENT_CREATED:
      event = _octopus_event_created_new (file);
      if (g_file_equal (file, path->private->directory))
          g_warn_if_reached ();
      break;
    case G_FILE_MONITOR_EVENT_DELETED:
      event = _octopus_event_deleted_new (file);
      if (g_file_equal (file, path->private->directory))
        {
          /* Oops, removed a directory which is exactly the
           * root directory of this OctopusPath... so
           * going into inconsistent state for this path. */
          path->private->status = OCTOPUS_STATUS_INCONSISTENT;
        }
      break;
    case G_FILE_MONITOR_EVENT_MOVED:
      event = _octopus_event_moved_new (file, other_file);
      if (g_file_equal (file, path->private->directory))
        {
          /* Oops, removed a directory which is exactly the
           * root directory of this OctopusPath... so
           * going into inconsistent state for this path. */
          path->private->status = OCTOPUS_STATUS_INCONSISTENT;
        }
      else if (g_file_equal (other_file, path->private->directory))
        g_warn_if_reached ();
      break;
    default:
      break;
    }

  /* Fix monitors on event */
  octopus_path_fix_paths_on_event (path, event);

  /* Notify event */
  if (event)
    {
      octopus_debug ("Emitting event in path '%s': %s",
                     path->private->directory_uri,
                     octopus_event_get_printable (event));
      path->private->func (path,
                           event,
                           path->private->user_data);
      g_object_unref (event);
    }
}

static void
octopus_path_disable_internal (OctopusPath  *path)
{
  if (G_LIKELY (path->private->monitors))
    {
      g_hash_table_destroy (path->private->monitors);
      path->private->monitors = NULL;
    }

  path->private->status = OCTOPUS_STATUS_DISABLED;
}

static gboolean
octopus_path_can_disable (OctopusPath  *path,
                          GError      **error)
{
  switch (path->private->status)
    {
    case OCTOPUS_STATUS_DISABLED:
      /* Already disabled! */
      return TRUE;
    case OCTOPUS_STATUS_NONE:
      /* Not a big deal, path was never enabled */
      path->private->status = OCTOPUS_STATUS_DISABLED;
      return TRUE;
    case OCTOPUS_STATUS_CRAWLING:
      /* Cannot disable while enabling operation is on-going! */
      g_set_error (error,
                   g_quark_from_static_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_WRONG_STATUS,
                   "Path at '%s' is currently being enabled",
                   path->private->directory_uri);
      return FALSE;
    case OCTOPUS_STATUS_ENABLED:
      /* Ok, so path is enabled, we can safely disable it */
      return TRUE;
    default:
      break;
    }

  /* Should never happen */
  g_warn_if_reached ();

  return FALSE;
}

gboolean
_octopus_path_disable (OctopusPath  *path,
                       GCancellable *cancellable,
                       GError      **error)
{
  GList *li;

  octopus_debug ("Disabling path '%s'...",
                 path->private->directory_uri);

  /* If cancelled do nothing */
  if (g_cancellable_is_cancelled (cancellable))
    return FALSE;

  /* Make sure we're in the proper status */
  if (!octopus_path_can_disable (path, error))
    {
      return FALSE;
    }

  /* Disable all children */
  li = path->private->children;
  while (li)
    {
      /* If cancelled do nothing */
      if (g_cancellable_is_cancelled (cancellable))
        return FALSE;

      if (!_octopus_path_disable (OCTOPUS_PATH (li->data),
                                  cancellable,
                                  error))
        return FALSE;

      li = g_list_next (li);
    }

  octopus_path_disable_internal (path);
  return TRUE;
}

static gboolean
octopus_path_can_enable (OctopusPath  *path,
                         GError      **error)
{
  switch (path->private->status)
    {
    case OCTOPUS_STATUS_CRAWLING:
    case OCTOPUS_STATUS_ENABLED:
      /* Already enabled! or being crawled */
      return TRUE;
    case OCTOPUS_STATUS_NONE:
    case OCTOPUS_STATUS_DISABLED:
      /* Ok, so path is disabled or uninitialized, so we can safely
       * enable it */
      return TRUE;
    default:
      /* Should never happen */
      g_warn_if_reached ();
      break;
    }

  return FALSE;
}

static gboolean
octopus_path_recrawl (OctopusPath   *path,
                      GCancellable  *cancellable,
                      GError       **error)
{
  gboolean recurse;

  /* Recrawling only allowed if already enabled */
  g_return_val_if_fail (path->private->status == OCTOPUS_STATUS_ENABLED, FALSE);
  g_return_val_if_fail (path->private->monitors, FALSE);

  /* Should recurse? */
  recurse = (path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY ?
             TRUE : FALSE);

  /* Start sync re-crawling */
  return octopus_path_crawl (path,
                             path->private->directory,
                             recurse,
                             FALSE,
                             TRUE,
                             cancellable,
                             error);
}

gboolean
_octopus_path_enable (OctopusPath   *path,
                      gboolean       crawl,
                      GCancellable  *cancellable,
                      GError       **error)
{
  GList *li;

  octopus_debug ("Enabling path '%s'...",
                 path->private->directory_uri);

  /* If cancelled do nothing */
  if (g_cancellable_is_cancelled (cancellable))
    return FALSE;

  /* Make sure we're in the proper status */
  if (!octopus_path_can_enable (path, error))
    return FALSE;

  /* Enable all children */
  li = path->private->children;
  while (li)
    {
      OctopusPath *li_data = OCTOPUS_PATH (li->data);

      octopus_debug ("Enabling child path '%s'...",
                     li_data->private->directory_uri);

      /* If cancelled do nothing */
      if (g_cancellable_is_cancelled (cancellable))
        return FALSE;

      if (!_octopus_path_enable (li_data,
                                 crawl,
                                 cancellable,
                                 error))
        return FALSE;

      li = g_list_next (li);
    }

  /* Nothing else to do here */
  if (path->private->action == OCTOPUS_ACTION_IGNORE)
    {
      path->private->status = OCTOPUS_STATUS_ENABLED;
      return TRUE;
    }

  /* Set as initializing */
  path->private->status = OCTOPUS_STATUS_CRAWLING;

  /* Create HT of monitors */
  if (path->private->action == OCTOPUS_ACTION_MONITOR ||
      path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY ||
      G_LIKELY (!path->private->monitors))
    {
      path->private->monitors = g_hash_table_new_full (g_file_hash,
                                                       (GEqualFunc) g_file_equal,
                                                       (GDestroyNotify) g_object_unref,
                                                       (GDestroyNotify) g_object_unref);
    }

  /* Only crawl if requested to do so */
  if (crawl)
    {
      gboolean recurse;

      /* Should recurse? */
      recurse = ((path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY ||
                  path->private->action == OCTOPUS_ACTION_CRAWL_RECURSIVELY) ?
                 TRUE : FALSE);

      octopus_debug ("Crawling path '%s'...",
                     path->private->directory_uri);

      /* Start sync crawling */
      if (!octopus_path_crawl (path,
                               path->private->directory,
                               recurse,
                               FALSE,
                               FALSE,
                               cancellable,
                               error))
        {
          octopus_path_disable_internal (path);

          /* If sync crawling fails, we must fully disable the monitor.
           * Note that it will only fail if the initial crawling of the
           * parent directory fails, not if some inner error is found in
           * any of the crawled directories. Well, will also fail if the
           * action was cancelled... */
          if (path->private->action == OCTOPUS_ACTION_MONITOR ||
              path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
            {
              path->private->status = OCTOPUS_STATUS_INCONSISTENT;

              octopus_debug ("Sync crawling failed, going into INCONSISTENT state"
                             " for path '%s'",
                             path->private->directory_uri);
            }
          return FALSE;
        }
    }

  /* Full crawling succeeded */
  path->private->status = OCTOPUS_STATUS_ENABLED;
  return TRUE;
}

static void
monitor_release_cb (gpointer data)
{
  gchar *uri = data;

  octopus_debug ("Monitor removed from directory '%s'", uri);
  _octopus_common_release_monitor ();
  g_free (uri);
}

static gboolean
octopus_path_setup_monitor (OctopusPath   *path,
                            GFile         *directory,
                            GCancellable  *cancellable,
                            GError       **error)
{
  GFileMonitor *file_monitor;
  gchar *uri;

  /* Before crawling this directory, check if the operation was
   * cancelled */
  if (g_cancellable_is_cancelled (cancellable))
    return FALSE;

  uri = g_file_get_uri (directory);

  /* If directory doesn't exist, don't setup monitor */
  if (!octopus_path_is_directory (directory))
    {
      g_set_error (error,
                   g_quark_from_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_NOT_FOUND,
                   "Cannot setup monitor in directory '%s' as it doesn't exist",
                   uri);
      g_free (uri);
      return FALSE;
    }

  /* If we reached the maximum number of allowed monitors, return */
  if (!_octopus_common_acquire_monitor ())
    {
      g_set_error (error,
                   g_quark_from_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_NOT_FOUND,
                   "Cannot setup monitor in directory '%s', "
                   "maximum number of monitors reached (%lu)",
                   uri,
                   octopus_common_get_max_monitors ());
      g_free (uri);
      return FALSE;
    }

  /* Create new monitor for the given directory */
  file_monitor = g_file_monitor_directory (directory,
                                           G_FILE_MONITOR_SEND_MOVED,
                                           NULL,
                                           error);
  if (!file_monitor)
    {
      _octopus_common_release_monitor ();
      g_free (uri);
      return FALSE;
    }

  octopus_debug ("Monitor set in directory '%s'", uri);

  /* Setup changed signal callback */
  g_signal_connect (file_monitor, "changed",
                    G_CALLBACK (octopus_path_event_cb),
                    path);

  /* Setup monitor release when GFileMonitor is destroyed */
  g_object_set_data_full (G_OBJECT (file_monitor),
                          "monitor-release",
                          uri,
                          monitor_release_cb);

  /* Add to the HT of monitors */
  g_hash_table_insert (path->private->monitors,
                       g_object_ref (directory),
                       file_monitor);

  return TRUE;
}

static gboolean
octopus_path_crawl (OctopusPath   *path,
                    GFile         *directory,
                    gboolean       recurse,
                    gboolean       silent,
                    gboolean       recrawl,
                    GCancellable  *cancellable,
                    GError       **error)
{
  GFileEnumerator *enumerator;
  gboolean already_existed = FALSE;

  /* If we are going to crawl a directory which is already one of the
   * child paths, then just return */
  if (octopus_path_find_child (path, directory))
    {
      octopus_file_debug (directory,
                          "Directory wont be crawled as already managed as child");
      return TRUE;
    }

  /* Monitoring needed in the path? */
  if (path->private->action == OCTOPUS_ACTION_MONITOR ||
      path->private->action == OCTOPUS_ACTION_MONITOR_RECURSIVELY)
    {
      /* If recrawling, check if we already had the monitor in this directory,
       * before adding it again */
      if (recrawl)
        {
          already_existed = (g_hash_table_lookup (path->private->monitors,
                                                  directory) != NULL ?
                             TRUE : FALSE);
        }

      if (!already_existed &&
          !octopus_path_setup_monitor (path,
                                       directory,
                                       cancellable,
                                       error))
        {
          return FALSE;
        }
    }

  /* Recurse the directory */
  enumerator = g_file_enumerate_children (directory,
                                          G_FILE_ATTRIBUTE_STANDARD_NAME "," G_FILE_ATTRIBUTE_STANDARD_TYPE,
                                          G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                          NULL,
                                          error);
  if (enumerator)
    {
      gboolean cancelled = FALSE;
      GFileInfo *child_info;
      GError *inner_error = NULL;

      while (!cancelled &&
             (child_info = g_file_enumerator_next_file (enumerator,
                                                        NULL,
                                                        &inner_error)) != NULL)
        {
          const gchar *child_name;
          gboolean is_dir;
          GFile *child;

          /* Get a GFile for the current file in the iteration */
          child_name = g_file_info_get_name (child_info);
          child = g_file_get_child (directory, child_name);
          is_dir = g_file_info_get_file_type (child_info) == G_FILE_TYPE_DIRECTORY;

          octopus_debug ("When crawling, child '%s' found... silent? %s; already? %s",
                         child_name,
                         silent ? "yes" : "no",
                         already_existed ? "yes" : "no");

          /* Notify found element */
          if (!silent && !already_existed)
            {
              OctopusEvent *event;

              event = _octopus_event_found_new (child, child_info);
              path->private->func (path,
                                   event,
                                   path->private->user_data);
              g_object_unref (event);
            }

          if (is_dir && recurse)
            {
              octopus_file_debug (child, "Crawling subpath...");

              /* Recurse! */
              octopus_path_crawl (path,
                                  child,
                                  recurse,
                                  silent,
                                  FALSE,
                                  cancellable,
                                  &inner_error);
              if (inner_error)
                {
                  if (!silent && !already_existed)
                    {
                      OctopusEvent *event;

                      /* Notify error on child */
                      event = _octopus_event_error_new_from_error (child, inner_error);
                      path->private->func (path,
                                           event,
                                           path->private->user_data);
                      g_object_unref (event);
                    }
                  g_clear_error (&inner_error);
                }
            }
          g_object_unref (child_info);
          g_object_unref (child);

          /* Check if someone cancelled the operation before keeping on
           * with the loop */
          cancelled = g_cancellable_is_cancelled (cancellable);
        }

      /* Dispose enumerator as no longer needed */
      g_object_unref (enumerator);

      /* Did we get an error while getting next child? */
      if (!child_info && inner_error)
        {
          /* If so, set error to be returned */
          g_propagate_error (error, inner_error);
          return FALSE;
        }

      /* Check if operation was cancelled during crawling */
      if (cancelled)
        {
          return FALSE;
        }

      /* Yey! */
      return TRUE;
    }

  /* Couldn't get a proper enumerator, so error in directory */
  return FALSE;
}

/**
 * octopus_path_get_directory:
 * @path: a #OctopusPath
 *
 * Returns the directory associated in the given path.
 *
 * Returns: a new reference to a #GFile. Call g_object_unref() on it when no
 * longer needed.
 */
GFile *
octopus_path_get_directory (const OctopusPath *path)
{
  g_return_val_if_fail (OCTOPUS_IS_PATH (path), NULL);
  return path->private->directory ? g_object_ref (path->private->directory) : NULL;
}

/**
 * octopus_path_get_directory_uri:
 * @path: a #OctopusPath
 *
 * Returns the URI of the directory associated in the given path.
 *
 * Returns: a constant string with the uri of @path, which should not be freed
 * by the caller.
 */
const gchar *
octopus_path_get_directory_uri (const OctopusPath *path)
{
  g_return_val_if_fail (OCTOPUS_IS_PATH (path), NULL);
  return path->private->directory_uri;
}

/**
 * octopus_path_get_action:
 * @path: a #OctopusPath
 *
 * Returns the action configured in @path.
 *
 * Returns: a #OctopusAction.
 */
OctopusAction
octopus_path_get_action (const OctopusPath *path)
{
  g_return_val_if_fail (OCTOPUS_IS_PATH (path), OCTOPUS_ACTION_NONE);
  return path->private->action;
}

/**
 * octopus_path_get_status:
 * @path: a #OctopusPath
 *
 * Returns the status of the given  @path.
 *
 * Returns: a #OctopusStatus.
 */
OctopusStatus
octopus_path_get_status (const OctopusPath *path)
{
  g_return_val_if_fail (OCTOPUS_IS_PATH (path), OCTOPUS_STATUS_NONE);
  return path->private->status;
}

/**
 * octopus_path_has_monitor:
 * @path: a #OctopusPath
 * @directory: a #GFile
 *
 * Checks if there is a #GFileMonitor for @directory in @path.
 *
 * Returns: #TRUE if @path holds a monitor, #FALSE otherwise.
 */
gboolean
octopus_path_has_monitor (const OctopusPath *path,
                          GFile             *directory)
{
  g_return_val_if_fail (OCTOPUS_IS_PATH (path), FALSE);
  g_return_val_if_fail (G_IS_FILE (directory), FALSE);
  return (path->private->monitors ?
          (g_hash_table_lookup (path->private->monitors, directory) ? TRUE : FALSE) :
          FALSE);
}
