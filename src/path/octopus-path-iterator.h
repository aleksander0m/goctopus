/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_PATH_ITERATOR_H__
#define __OCTOPUS_PATH_ITERATOR_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

/* Octopus headers */
#include <octopus-common.h>
#include <octopus-path.h>

/**
 * SECTION:octopus-path-iterator
 * @short_description: Iteration of paths
 * @title: OctopusPathIterator
 * @stability: Unstable
 * @include: octopus.h
 *
 * Support for iteration the list of paths retrieved from a
 * #OctopusMonitor or a #OctopusCrawler.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

/**
 * OctopusPathIterator:
 *
 * The Octopus Path Iterator object allows iterating over all #OctopusPath
 * objects configured in a given #OctopusMonitor.
 */
typedef struct _OctopusPathIterator OctopusPathIterator;

const OctopusPath *octopus_path_iterator_next    (OctopusPathIterator *iterator);
void               octopus_path_iterator_rewind  (OctopusPathIterator *iterator);
void               octopus_path_iterator_destroy (OctopusPathIterator *iterator);

/* END PUBLIC */

OctopusPathIterator *_octopus_path_iterator_new_from_tree (OctopusPath *tree);

G_END_DECLS

#endif /* __OCTOPUS_PATH_ITERATOR_H__ */
