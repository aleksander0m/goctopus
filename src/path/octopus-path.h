/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef __OCTOPUS_PATH_H__
#define __OCTOPUS_PATH_H__

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

/* Octopus headers */
#include <octopus-event.h>
#include <octopus-common.h>

/**
 * SECTION:octopus-path
 * @short_description: The Octopus Path interface
 * @title: OctopusPath
 * @stability: Unstable
 * @include: octopus.h
 *
 * A fine-grained interface to the #OctopusMonitor internals.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

#define OCTOPUS_TYPE_PATH            (octopus_path_get_type ())
#define OCTOPUS_PATH(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), OCTOPUS_TYPE_PATH, OctopusPath))
#define OCTOPUS_PATH_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OCTOPUS_TYPE_PATH, OctopusPathClass))
#define OCTOPUS_IS_PATH(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), OCTOPUS_TYPE_PATH))
#define OCTOPUS_IS_PATH_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OCTOPUS_TYPE_PATH))
#define OCTOPUS_PATH_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OCTOPUS_TYPE_PATH, OctopusPathClass))


typedef struct _OctopusPath         OctopusPath;
typedef struct _OctopusPathClass    OctopusPathClass;
typedef struct _OctopusPathPrivate  OctopusPathPrivate;

/**
 * OctopusPath:
 *
 * The <structname>OctopusPath</structname> provides information about the
 * configured paths in the #OctopusMonitor.
 */
struct _OctopusPath {
  /*< private >*/
  GObject parent;
  OctopusPathPrivate *private;
};

struct _OctopusPathClass {
  GObjectClass parent;
};

GType          octopus_path_get_type          (void);

GFile         *octopus_path_get_directory     (const OctopusPath *path);

const gchar   *octopus_path_get_directory_uri (const OctopusPath *path);

OctopusAction  octopus_path_get_action        (const OctopusPath *path);

OctopusStatus  octopus_path_get_status        (const OctopusPath *path);

gboolean       octopus_path_has_monitor       (const OctopusPath *path,
                                               GFile             *directory);

/* END PUBLIC */

typedef void (* OctopusPathNotificationFunc) (OctopusPath  *path,
                                              OctopusEvent *event,
                                              gpointer      user_data);

/* Path management */
OctopusPath *_octopus_path_new           (GFile                       *directory,
                                          OctopusAction                action,
                                          OctopusPathNotificationFunc  func,
                                          gpointer                     user_data);
gboolean     _octopus_path_enable        (OctopusPath   *path,
                                          gboolean       crawl,
                                          GCancellable  *cancellable,
                                          GError       **error);

gboolean     _octopus_path_disable       (OctopusPath   *path,
                                          GCancellable  *cancellable,
                                          GError       **error);

void          _octopus_path_list_children (OctopusPath  *path,
                                           GSList      **list);

/* Path tree management */
OctopusPath *  _octopus_path_tree_new        (void);
gboolean       _octopus_path_tree_add        (OctopusPath  *tree,
                                              OctopusPath  *self,
                                              GCancellable *cancellable,
                                              GError      **error);
gboolean       _octopus_path_tree_find       (OctopusPath    *tree,
                                              GFile          *directory,
                                              OctopusPath   **parent,
                                              OctopusPath   **self);
gboolean       _octopus_path_tree_remove     (OctopusPath    *tree,
                                              GFile          *directory,
                                              GError        **error);
OctopusStatus  _octopus_path_tree_get_status (OctopusPath *tree);

G_END_DECLS

#endif /* __OCTOPUS_PATH_H__ */
