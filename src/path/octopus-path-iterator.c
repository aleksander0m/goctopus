/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/* Octopus headers */
#include <octopus-path-iterator.h>

struct _OctopusPathIterator {
  GSList *paths;
  GSList *current;
};

OctopusPathIterator *
_octopus_path_iterator_new_from_tree (OctopusPath *tree)
{
  OctopusPathIterator *iterator;

  iterator = g_slice_new0 (OctopusPathIterator);
  _octopus_path_list_children (tree, &(iterator->paths));

  /* Set current pointing to the first element in the list */
  octopus_path_iterator_rewind (iterator);

  return iterator;
}

/**
 * octopus_path_iterator_next:
 * @iterator: a #OctopusPathIterator
 *
 * Iterates to the next available path, if any.
 *
 * Returns: a #OctopusPath which should not be unref-ed by the caller; or #NULL
 * if no more paths to iterate.
 */
const OctopusPath *
octopus_path_iterator_next (OctopusPathIterator *iterator)
{
  g_return_val_if_fail (iterator, NULL);
  if (iterator->current)
    {
      OctopusPath *current;

      current = OCTOPUS_PATH (iterator->current->data);
      /* Advance iterator */
      iterator->current = g_slist_next (iterator->current);
      return current;
    }
  return NULL;
}

/**
 * octopus_path_iterator_rewind:
 * @iterator: a #OctopusPathIterator
 *
 * Rewinds the iterator so that it points back to the first element in the list
 * to iterate.
 */
void
octopus_path_iterator_rewind (OctopusPathIterator *iterator)
{
  g_return_if_fail (iterator);
  iterator->current = iterator->paths;
}

/**
 * octopus_path_iterator_destroy:
 * @iterator: a #OctopusPathIterator
 *
 * Disposes @iterator. Once disposed, the #OctopusPath objects returned during
 * octopus_path_iterator_next() operations should no longer be used.
 */
void
octopus_path_iterator_destroy (OctopusPathIterator *iterator)
{
  g_return_if_fail (iterator);

  if (iterator->paths)
    {
      GSList *it;

      for (it = iterator->paths;
           it;
           it = g_slist_next (it))
        {
          g_object_unref (G_OBJECT (it->data));
        }
      g_slist_free (iterator->paths);
    }

  g_slice_free (OctopusPathIterator, iterator);
}
