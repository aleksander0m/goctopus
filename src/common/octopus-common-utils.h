/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef _OCTOPUS_COMMON_UTILS_H_
#define _OCTOPUS_COMMON_UTILS_H_

/* GLib headers */
#include <glib.h>

#include <config.h>

G_BEGIN_DECLS

#if ENABLE_INTERNAL_LOG
#  define octopus_debug(format, ...)            \
  g_debug ("%s:%d: " format,                    \
           __FILE__, __LINE__, ##__VA_ARGS__)
#  define octopus_message(format, ...)             \
  g_message ("%s:%d: " format,                     \
             __FILE__, __LINE__, ##__VA_ARGS__)
#  define octopus_warning(format, ...)             \
  g_warning ("%s:%d: " format,                     \
             __FILE__, __LINE__, ##__VA_ARGS__)
#  define octopus_critical(format, ...)           \
  g_critical ("%s:%d: " format,                   \
              __FILE__, __LINE__, ##__VA_ARGS__)

/* I'm lazy to get_uri() each time I want to report an error... */
# define octopus_file_critical(file, error, format, ...)                \
  do {                                                                  \
    gchar *uri = g_file_get_uri (file);                                 \
    octopus_critical ("%s [file: %s'] [error: %s]",                     \
                      format,                                           \
                      ##__VA_ARGS__,                                    \
                      uri,                                              \
                      error && *error ? (*error)->message : "none");    \
    g_free (uri);                                                       \
  } while (0)
# define octopus_file_debug(file, format, ...)              \
  do {                                                      \
    gchar *uri = g_file_get_uri (file);                     \
    octopus_debug ("%s [file: %s']",                        \
                   format,                                  \
                   ##__VA_ARGS__,                           \
                   uri);                                    \
    g_free (uri);                                           \
  } while (0)
#else
#  define octopus_debug(...)
#  define octopus_message(...)
#  define octopus_warning(...)
#  define octopus_critical(...)
#  define octopus_file_debug(...)
#  define octopus_file_critical(...)
#endif /* ENABLE_INTERNAL_LOG */

G_END_DECLS

#endif /* _OCTOPUS_COMMON_UTILS_H_ */
