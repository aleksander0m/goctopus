/*
 * Copyright (C) 2009,2010 Nokia <ivan.frade@nokia.com>
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/* This code is based on Gnome Tracker's libtracker-miner/tracker-monitor.c */

/* GLib headers */
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <string.h>
#include <stdlib.h>

#include "octopus-common.h"
#include "octopus-common-utils.h"

/* Guessing limits for non-inotify backends... */
#define MAX_FAM_MONITORS 400
#define MAX_FEN_MONITORS 8192
#define MAX_WIN32_MONITORS 8192

/* Percentage of total number of monitors used by Octopus in the inotify
 * backend */
#define MAX_INOTIFY_MONITORS_PERCENTAGE 80

static gulong
get_inotify_limit (void)
{
  GError *error = NULL;
  const gchar *filename = "/proc/sys/fs/inotify/max_user_watches";
  gchar *contents = NULL;
  gulong limit;

  if (!g_file_get_contents (filename, &contents, NULL, &error))
    {
      octopus_warning ("Couldn't get INotify monitor limit from:'%s', %s",
                       filename,
                       error ? error->message : "no error given");
      g_clear_error (&error);

      /* Setting limit to an arbitary limit */
      limit = 8192;
	}
  else
    {
      limit = atoi (contents);
      g_free (contents);
	}

	return limit;
}

/* Based on Gnome Tracker's `tracker_monitor_init ()' method. */
gulong
_octopus_limits_detect_monitor_backend (GError **error)
{
  GFile *file;
  GFileMonitor *monitor;
  const gchar  *name;
  gulong max_monitors = 0;

  /* For the first monitor we get the type and find out if we
   * are using inotify, FAM, polling, etc.
   */
  file = g_file_new_for_path (g_get_home_dir ());
  monitor = g_file_monitor_directory (file, G_FILE_MONITOR_NONE, NULL, NULL);

  /* We use the name because the type itself is actually
   * private and not available publically. Note this is
   * subject to change, but unlikely of course.
   */
  name = g_type_name (G_OBJECT_TYPE (monitor));
  if (!name)
    {
      g_set_error (error,
                   g_quark_from_static_string (OCTOPUS_ERROR),
                   OCTOPUS_ERROR_UNSUPPORTED,
                   "Monitor backend is unknown!");
    }
  else
    {
      /* Set limits based on backend... */
      if (strcmp (name, "GInotifyDirectoryMonitor") == 0)
        {
          /* Using inotify
           * Setting limit based on kernel
           * settings in /proc...
           */
          max_monitors = get_inotify_limit ();
          /* Apply percentage of this value */
          max_monitors *= (MAX_INOTIFY_MONITORS_PERCENTAGE/100.0);

          octopus_debug ("Monitor backend is Inotify (max: %lu)", max_monitors);
        }
      else if (strcmp (name, "GFamDirectoryMonitor") == 0)
        {
          /* Using Fam */
          octopus_debug ("Monitor backend is Fam (max: %lu)", max_monitors);
          /* Setting limit to an arbitary limit
           * based on testing
           */
          max_monitors = MAX_FAM_MONITORS;
        }
      else if (strcmp (name, "GFenDirectoryMonitor") == 0)
        {
          /* Using Fen, what is this? */
          octopus_debug ("Monitor backend is Fen (max: %lu)", max_monitors);

          max_monitors = MAX_FEN_MONITORS;
        }
      else if (strcmp (name, "GWin32DirectoryMonitor") == 0)
        {
          /* Using Windows */
          octopus_debug ("Monitor backend is Windows (max: %lu)", max_monitors);
          /* Guessing limit... */
          max_monitors = MAX_WIN32_MONITORS;
        }
      else
        {
          /* Unknown */
          g_set_error (error,
                       g_quark_from_static_string (OCTOPUS_ERROR),
                       OCTOPUS_ERROR_UNSUPPORTED,
                       "Monitor backend:'%s' is unknown!",
                       name);
        }
    }

  g_object_unref (monitor);
  g_object_unref (file);

  return max_monitors;
}
