/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include "octopus-common.h"
#include "octopus-common-utils.h"
#include "octopus-limits.h"

/* Octopus properly initialized? */
static gboolean initialized;

/* Maximum number of monitors supported by the backend */
static gulong max_monitors;

/* Currently used monitors */
static gulong n_monitors;

gboolean
_octopus_common_init (GError **error)
{
  if (!initialized)
    {
      /* Right now, the only initialization check to be done is actually checking
       * the monitor backend support */
      if ((max_monitors = _octopus_limits_detect_monitor_backend (error)) > 0)
        initialized = TRUE;
    }
  return initialized;
}

/**
 * octopus_common_get_max_monitors:
 *
 * Get the maximum number of monitors available. Note that this limit does not
 * exactly match with the real number of monitors available, as this is a shared
 * resource across the whole system.
 *
 * Returns: the maximum number of monitors available.
 */
gulong
octopus_common_get_max_monitors (void)
{
  return _octopus_common_init (NULL) ? max_monitors : 0;
}

/**
 * octopus_common_get_n_monitors:
 *
 * Get the number of currently used monitors.
 *
 * Returns: the number of monitors being used.
 */
gulong
octopus_common_get_n_monitors (void)
{
  return n_monitors;
}

gboolean
_octopus_common_acquire_monitor (void)
{
  if (n_monitors < max_monitors)
    {
      n_monitors++;
      if (n_monitors == max_monitors)
        {
          /* Warn the first time we reach to the maximum */
          octopus_critical ("Reached maximum number of monitors: %lu",
                            max_monitors);
        }
      return TRUE;
    }

  return FALSE;
}

void
_octopus_common_release_monitor (void)
{
  n_monitors--;
}
