/*
 * Copyright (C) 2010 Aleksander Morgado <aleksander@gnu.org>
 *
 * This file is part of The Octopus Library.
 *
 *   The Octopus Library is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   The Octopus Library is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with The Octopus Library.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

#ifndef _OCTOPUS_COMMON_H_
#define _OCTOPUS_COMMON_H_

/* GLib headers */
#include <glib.h>

/**
 * SECTION:octopus-common
 * @short_description: Common utilities in the Octopus Library
 * @title: OctopusCommon
 * @stability: Unstable
 * @include: octopus.h
 *
 * The Octopus Library provides a set of helper functionalities, including error
 * reporting, and action and status management.
 */

G_BEGIN_DECLS

/* BEGIN PUBLIC */

/**
 * OCTOPUS_ERROR:
 *
 * Error domain for Octopus methods. Errors in this domain will be from the
 * #OctopusError enumeration. See #GError for information on error domains.
 */
#define OCTOPUS_ERROR "OCTOPUS_ERROR"

/**
 * OctopusError:
 * @OCTOPUS_ERROR_UNKNOWN: Unknown error
 * @OCTOPUS_ERROR_EXISTS: The item already exists
 * @OCTOPUS_ERROR_NOT_FOUND: The item was not found
 * @OCTOPUS_ERROR_CANCELLED: The action was cancelled
 * @OCTOPUS_ERROR_WRONG_STATUS: Invalid operation due to wrong status
 * @OCTOPUS_ERROR_UNSUPPORTED: There is no support for this OS in Octopus
 *
 * Error codes returned by the Octopus Monitor
 */
typedef enum {
  OCTOPUS_ERROR_UNKNOWN = 0,
  OCTOPUS_ERROR_EXISTS,
  OCTOPUS_ERROR_NOT_FOUND,
  OCTOPUS_ERROR_CANCELLED,
  OCTOPUS_ERROR_WRONG_STATUS,
  OCTOPUS_ERROR_UNSUPPORTED,
} OctopusError;

/**
 * OctopusAction:
 * @OCTOPUS_ACTION_NONE: No action
 * @OCTOPUS_ACTION_IGNORE: The item and its children should be ignored.
 * @OCTOPUS_ACTION_CRAWL: The item should be crawled
 * @OCTOPUS_ACTION_CRAWL_RECURSIVELY: The item and its children should be
 *  crawled
 * @OCTOPUS_ACTION_MONITOR: The item should be crawled and monitored
 * @OCTOPUS_ACTION_MONITOR_RECURSIVELY: The item and its children should
 *  be crawled and monitored
 *
 * Actions configured in #OctopusCrawler, #OctopusMonitor and #OctopusPath
 * objects
 */
typedef enum {
  OCTOPUS_ACTION_NONE = 0,
  OCTOPUS_ACTION_IGNORE,
  OCTOPUS_ACTION_CRAWL,
  OCTOPUS_ACTION_CRAWL_RECURSIVELY,
  OCTOPUS_ACTION_MONITOR,
  OCTOPUS_ACTION_MONITOR_RECURSIVELY,
  /*< private >*/
  OCTOPUS_ACTION_LAST
} OctopusAction;

/**
 * OctopusStatus:
 * @OCTOPUS_STATUS_NONE: No status
 * @OCTOPUS_STATUS_ENABLED: Enabled
 * @OCTOPUS_STATUS_DISABLED: Disabled
 * @OCTOPUS_STATUS_INCONSISTENT: Inconsistent. A previous action to enable or
 * disable the object failed, and it is kept in an inconsistent state.
 * @OCTOPUS_STATUS_CRAWLING: The monitor is currently crawling a newly
 * enabled path.
 *
 * Status of #OctopusMonitor and #OctopusPath objects
 */
typedef enum {
  OCTOPUS_STATUS_NONE = 0,
  OCTOPUS_STATUS_ENABLED,
  OCTOPUS_STATUS_DISABLED,
  OCTOPUS_STATUS_INCONSISTENT,
  OCTOPUS_STATUS_CRAWLING,
  /*< private >*/
  OCTOPUS_STATUS_LAST
} OctopusStatus;

/**
 * OctopusEventType:
 * @OCTOPUS_EVENT_TYPE_UNKNOWN: Unknown event
 * @OCTOPUS_EVENT_TYPE_ERROR: Event is a #OctopusEventError
 * @OCTOPUS_EVENT_TYPE_FOUND: Event is a #OctopusEventFound
 * @OCTOPUS_EVENT_TYPE_CREATED: Event is a #OctopusEventCreated
 * @OCTOPUS_EVENT_TYPE_UPDATED: Event is a #OctopusEventUpdated
 * @OCTOPUS_EVENT_TYPE_DELETED: Event is a #OctopusEventDeleted
 * @OCTOPUS_EVENT_TYPE_MOVED: Event is a #OctopusEventMoved
 *
 * Type of event.
 */
typedef enum {
  OCTOPUS_EVENT_TYPE_UNKNOWN,
  OCTOPUS_EVENT_TYPE_ERROR,
  OCTOPUS_EVENT_TYPE_FOUND,
  OCTOPUS_EVENT_TYPE_CREATED,
  OCTOPUS_EVENT_TYPE_UPDATED,
  OCTOPUS_EVENT_TYPE_DELETED,
  OCTOPUS_EVENT_TYPE_MOVED,
  /*< private >*/
  OCTOPUS_EVENT_TYPE_LAST
} OctopusEventType;


gulong octopus_common_get_max_monitors (void);
gulong octopus_common_get_n_monitors (void);

/* END PUBLIC */

gboolean _octopus_common_init            (GError **error);
gboolean _octopus_common_acquire_monitor (void);
void     _octopus_common_release_monitor (void);

G_END_DECLS

#endif /* _OCTOPUS_COMMON_H_ */
