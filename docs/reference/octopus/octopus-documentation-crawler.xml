<?xml version='1.0' encoding="ISO-8859-1"?>

<part id="octopus-crawler">
  <title>The Crawler</title>
  <partintro>
    <para>
      The <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname>
      is the object providing crawling capabilities in the Octopus Library.
    </para>
  </partintro>

  <chapter id="octopus-crawler-create">
    <title>Creation</title>
    <para>
      You can create a new <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname> with
      the <function><link linkend="octopus-crawler-new">octopus_crawler_new()</link></function> method.
    </para>
  </chapter>

  <chapter id="octopus-crawler-add-remove--paths">
    <title>Adding paths</title>
    <para>
      New paths to crawl can be added in the <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname>
      with the <function><link linkend="octopus-crawler-add-path">octopus_crawler_add_path()</link></function> method.
      This method will NOT check if the given directory exists.
    </para>

    <sect2 id="octopus-crawler-add-paths-actions">
      <title>Actions</title>
      <para>
        When adding a new path to the crawler, you need to specify the <link linkend="OctopusAction">OctopusAction</link>
        which will be used for the given path.
        <itemizedlist>
          <listitem>
            <literal><link linkend="OCTOPUS-ACTION-CRAWL:CAPS">OCTOPUS_ACTION_CRAWL</link></literal>
	        <para>
	          The given path will be crawled, but not its subdirectories, if any.
	        </para>
          </listitem>
          <listitem>
            <literal><link linkend="OCTOPUS-ACTION-CRAWL-RECURSIVELY:CAPS">OCTOPUS_ACTION_CRAWL_RECURSIVELY</link></literal>
	        <para>
              The given path and all its subdirectories will be crawled.
	        </para>
          </listitem>
          <listitem>
            <literal><link linkend="OCTOPUS-ACTION-IGNORE:CAPS">OCTOPUS_ACTION_IGNORE</link></literal>
	        <para>
	          The given path will not be crawled. This action is used when you don't want to crawl some subdirectory of a
              recursively indexed directory.
	        </para>
          </listitem>
        </itemizedlist>
      </para>
    </sect2>
  </chapter>

  <chapter id="octopus-crawler-remove-paths">
    <title>Removing paths</title>
    <para>
      The paths that were added to the crawler can be removed with the
      <function><link linkend="octopus-crawler-remove-path">octopus_crawler_remove_path()</link></function> method.
    </para>
    <para>
      Once a path is removed, the internal tree of paths in the
      <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname> will be updated, and
      act depending on the new action to be done on the path, if any. For example, if <emphasis>Foo</emphasis>
      (configured to be ignored) is a subdirectory of <emphasis>Bar</emphasis> (configured to be recursively crawled),
      and <emphasis>Foo</emphasis> is then removed with
      <function><link linkend="octopus-crawler-remove-path">octopus_crawler_remove_path()</link></function>,
      then <emphasis>Foo</emphasis> will be recursively crawled, following the action configured in its
      parent directory <emphasis>Bar</emphasis>.
    </para>
    <para>
      This means that removing a path from the crawler doesn't imply that a directory is no longer crawled.
      It really depends on the actions configured (if any) in the parent directories of the one being removed.
    </para>
  </chapter>

  <chapter id="octopus-crawler-crawling">
    <title>Crawling</title>
    <para>
      When a path is added to an crawler, it will NOT automatically start crawling the directory. The crawling
      operation on all the configured paths will start when
      <function><link linkend="octopus-crawler-crawl">octopus_crawler_crawl()</link></function>
      is called. Once called, <structname><link linkend="OctopusEventFound-struct">OctopusEventFound</link></structname>
      events will be notified for each found directory or file.
    </para>
    <para>
      During the crawling operation, if the configured path doesn't exist or cannot be read a
      <structname><link linkend="OctopusEventError-struct">OctopusEventError</link></structname> event will be
      notified. The crawling operation will keep on with all additional paths that were added to the crawler.
    </para>
    <para>
      Once the crawling operation has finished,
      <function><link linkend="octopus-crawler-crawl">octopus_crawler_crawl()</link></function> can be directly
      called to start the crawling operation on the same configured directories.
    </para>
  </chapter>

  <chapter id="octopus-crawler-iterate-paths">
    <title>Iterating paths</title>

    <sect2 id="octopus-crawler-iterate-paths-path">
      <title>The Octopus Path</title>
      <para>
	    A <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname> will internally
	    store a <structname><link linkend="OctopusPath-struct">OctopusPath</link></structname> for each path
	    added with <function><link linkend="octopus-crawler-add-path">octopus_crawler_add_path()</link></function>.
      </para>
    </sect2>

    <sect2 id="octopus-crawler-iterate-paths-known">
      <title>Getting a known OctopusPath</title>
      <para>
	    You can use the <function><link linkend="octopus-crawler-get-path-at">octopus_crawler_get_path_at()</link></function>
	    method to retrieve the <structname><link linkend="OctopusPath-struct">OctopusPath</link></structname> for a given
	    existing path. Note that the specific path should have been already added to the
	    <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname>.
      </para>
    </sect2>

    <sect2 id="octopus-crawler-iterate-paths-all">
      <title>Iterating all available OctopusPaths</title>
      <para>
	    Sometimes, you may need to iterate the paths that were added in a given
	    <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname>.
	    Iteration over all currently configured paths of a crawler is done with the
	    <structname><link linkend="OctopusPathIterator">OctopusPathIterator</link></structname> object
	    returned by the <function><link linkend="octopus-crawler-get-paths">octopus_crawler_get_paths()</link></function>
	    method.
      </para>
      <para>
	    Once you have a proper <structname><link linkend="OctopusPathIterator">OctopusPathIterator</link></structname>,
	    you can use <function><link linkend="octopus-path-iterator-next">octopus_path_iterator_next()</link></function> to
	    get the next available <structname><link linkend="OctopusPath-struct">OctopusPath</link></structname>, and
	    <function><link linkend="octopus-path-iterator-rewind">octopus_path_iterator_rewind()</link></function> to move the
	    iterator back to the first element in the iteration.
      </para>
      <para>
	    Note that the returned <structname><link linkend="OctopusPath-struct">OctopusPath</link></structname> objects cannot
	    be modified by the caller, and they will be valid as long as the
	    <structname><link linkend="OctopusPathIterator">OctopusPathIterator</link></structname> object remains alive.
      </para>
      <para>
	    When no longer used, you should dispose the
	    <structname><link linkend="OctopusPathIterator">OctopusPathIterator</link></structname> object with
	    <function><link linkend="octopus-path-iterator-destroy">octopus_path_iterator_destroy()</link></function>.
      </para>
    </sect2>
  </chapter>

  <chapter id="octopus-crawler-events-retrieving">
    <title>Retrieving events</title>
    <para>
	  Any event happening during the crawling operation will be directoy notified by the
	  <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname>.
    </para>
    <para>
	  You will need to connect (<function><link linkend="g-signal-connect">g_signal_connect()</link></function>) to the
	  <link linkend="OctopusCrawler-event">"event"</link> signal in the
	  <structname><link linkend="OctopusCrawler">OctopusCrawler</link></structname> object in order to get
	  the notifications of the <structname><link linkend="OctopusEvent-struct">OctopusEvents</link></structname>.
    </para>
  </chapter>

  <chapter id="octopus-crawler-example">
    <title>Example</title>
    <para>
      The following program will create a <type><link linkend="OctopusCrawler">OctopusCrawler</link></type> object
      and add the path retrieved as input argument in it; then it will start crawling. For each file or directory
      found during crawling, a <type><link linkend="OctopusEventFound-struct">OctopusEventFound</link></type> will be issued,
      and in the callback configured we will print a message for each of them.

      <programlisting>
#include &lt;octopus.h&gt;

static void
event_cb (<type><link linkend="OctopusCrawler">OctopusCrawler</link></type> *crawler,
          <type><link linkend="OctopusEvent-struct">OctopusEvent</link></type>   *event,
          gpointer        user_data)
{
  g_print ("Received event: %s\n",
           <function><link linkend="octopus-event-get-printable">octopus_event_get_printable</link></function> (event));
}

int main (int argc, char **argv)
{
  <type><link linkend="OctopusCrawler">OctopusCrawler</link></type> *crawler;
  GFile *directory;
  GError *error = NULL;

  if (argc &lt; 2)
    return -1;

  /* Initialize GLib type system */
  g_type_init ();

  /* Create crawler */
  crawler = <function><link linkend="octopus-crawler-new">octopus_crawler_new</link></function> ();

  /* Setup the signal handler */
  g_signal_connect (crawler,
                    "event",
                    G_CALLBACK (event_cb),
                    NULL);

  /* Create a GFile with the input path */
  directory = g_file_new_for_commandline_arg (argv[1]);

  /* Add the path to be recursively crawled */
  if (!<function><link linkend="octopus-crawler-add-path">octopus_crawler_add_path</link></function> (crawler,
                                 directory,
                                 <literal><link linkend="OCTOPUS-ACTION-CRAWL-RECURSIVELY:CAPS">OCTOPUS_ACTION_CRAWL_RECURSIVELY</link></literal>
                                 &amp;error))
    {
      g_warning ("Error adding path synchronously: %s",
                 error ? error->message : "none");
      g_clear_error (&amp;error);
    }

  /* Start synchronous crawling! */
  if (!<function><link linkend="octopus-crawler-crawl">octopus_crawler_crawl</link></function> (crawler, NULL, &amp;error))
    {
      g_warning ("Crawling failed: %s",
                 error ? error->message : "none");
      g_clear_error (&amp;error);
    }

  g_print ("Finished!\n");

  g_object_unref (directory);
  g_object_unref (crawler);

  return 0;
}
      </programlisting>
    </para>

    <para>
      And here is an example of output of the previous program when passing '/opt/example' as input argument.
      <programlisting>
Received event: [ FOUND ] 'file:///opt/example/hello'
Received event: [ FOUND ] 'file:///opt/example/hello/world'
Received event: [ FOUND ] 'file:///opt/example/hello/world/hi.txt'
Received event: [ FOUND ] 'file:///opt/example/goodbye'
Received event: [ FOUND ] 'file:///opt/example/goodbye/cruel'
Received event: [ FOUND ] 'file:///opt/example/goodbye/cruel/world'
Received event: [ FOUND ] 'file:///opt/example/goodbye/cruel/world/cheers.txt'
Finished!
      </programlisting>
    </para>
  </chapter>

</part>
